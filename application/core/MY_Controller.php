<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MY_Controller [Controller]
 * Descricao
 * @copyright (c) year, Pablo Cezar Genesys Tech
 */
class MY_Controller extends CI_Controller {

    /**
     * <b>ConfigSysten</b>
     * Armazena valores de configurações do sistema.
     * @var type ARRAY
     */
    public $ConfigSysten;

    /**
     * <b>FormPost</b>
     * Armazena valores de array de uma requisição via post(submit).
     * @var type ARRAY
     */
    public $FormPost;

    /**
     * <b>SessionData</b>
     * Armazena valores de sessão do usuário.
     * @var type ARRAY
     */
    public $SessionData;

    /**
     * <b>Emails</b>
     * Armazena os emails recebidos via formulário de contato do site.
     * @var type ARRAY
     */
    public $Emails;

    /* Para visitantes Online */
    private $Cache;
    private $Date;

    public function __construct() {
        parent::__construct();
        $this->checkDbExists();
        $this->FormPost = $this->input->post();
        $this->ConfigSysten = $this->_getConfig();
        $this->SessionData = $this->session->userdata();
        $this->Emails = $this->_getNovosEmails();
    }

    /**
     * <b>getConfig<b>
     * Retorna Configurações do CMS
     * @return Array
     */
    private function _getConfig() {
        return $this->ConfiguracoesModel->getConfig()->row();
    }

    /**
     * <b>_getEmails<b>
     * Retorna Emaisl recebidos através do formulário de contato do site.
     * @return Array
     */
    private function _getNovosEmails() {
        return $this->EmailsModel->getNovosEmails()->result();
    }

    /**
     * <b>configAnalytics</b>
     * configuração de conta analytics, para receber métricas do site.
     * @param type $dimensions
     * @param type $dataIni
     * @param type $dataFim
     * @return ARRAY
     */
    public function configAnalytics($dimensions, $dataIni = 'Y-m-01', $dataFim = 'Y-m-d') {
        $chave = __DIR__ . DIRECTORY_SEPARATOR . 'genesys-tech-19a0014d1795.p12';
        if (!file_exists($chave)):
            return false;
        elseif ($this->ConfigSysten->config_report_id == ""):
            $chave = '';
        else:
            $ga = new gapi($this->ConfigSysten->config_client_email_report, $chave);

            $report_id = $this->ConfigSysten->config_report_id;
//            $dimensions = array('month', 'pageTitle');
            $metrics = array('pageviews', 'visits', 'users', 'newUsers', 'sessions', 'organicSearches', 'bounceRate');
            $sort_metric = NULL;
            $filter = "country == 'Brazil'";
            $start_date = date($dataIni);
            $end_date = date($dataFim);
            $start_index = NULL;
            $max_results = NULL;

            $ga->requestReportData($report_id, $dimensions, $metrics, $sort_metric, $filter, $start_date, $end_date, $start_index, $max_results);

            return $ga->getResults();
        endif;
    }

    /**
     * <b>checkSession</b>
     * Verifica a existencia da sessão do usuário, caso não exista sessão redireciona-o para a página de login.
     */
    public function checkSession() {
        $logado = $this->session->userdata('logado');
        if (!isset($logado) || $logado != true) {
            redirect('login');
        }
    }

    /**
     * <b>configSMTP</b>
     * Método responsavel por setar configurações de SMTP.
     */
    public function configSMTP() {
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = $this->ConfigSysten->config_smtp_host;
        $config['smtp_port'] = $this->ConfigSysten->config_smtp_porta;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = $this->ConfigSysten->config_smtp_usuario;
        $config['smtp_pass'] = $this->ConfigSysten->config_smtp_senha;
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to validate email or not      

        $this->email->initialize($config);
        return $this->load->library('email');
    }

    /**
     * Método de upload de arquivo.
     * @param String $pastaDestino
     * Pasta em que se destinará o arquivo.
     * @param String $arquivo
     * name do input.
     */
    public function upload($pastaDestino, $arquivo) {
        if (!is_dir($pastaDestino)):
            mkdir($pastaDestino, 0777);
        endif;

        $config['upload_path'] = "./{$pastaDestino}/";
        $config['allowed_types'] = 'gif|jpg|png|pdf|docx|doc|xlsx';
        $config['max_size'] = '1000';

        $this->load->library('upload', $config);
        
        $this->upload->do_upload($arquivo);
    }

    public function SEO($pagina) {
        $base_url = base_url();
        $canonical = $base_url . $this->uri->uri_string();

        $seo = "<meta name='robots' content='index, follow'/>\n";
        $seo .= "<link rel='canonical' href='{$canonical}'/>\n";
        $seo .= "<meta name='description' content='{$pagina->pagina_descricao}'/>\n";
        //Schema.org
        $seo .= "<meta itemprop='name' content='{$pagina->pagina_titulo}'/>\n";
        $seo .= "<meta itemprop='description' content='{$pagina->pagina_descricao}'/>\n";
        $seo .= "<meta itemprop='image' content='{$base_url}{$pagina->pagina_imagem_destacada}'/>\n";
        $seo .= "<meta itemprop='url' content='{$canonical}'/>\n";
        //Open Graphic
        $seo .= "<meta property='og:type' content='article'/>\n";
        $seo .= "<meta property='og:title' content='{$pagina->pagina_titulo}'/>\n";
        $seo .= "<meta property='og:site_name' content='{$this->ConfigSysten->config_nome_empresa}'/>\n";
        $seo .= "<meta property='og:url' content='{$canonical}'/>\n";
        $seo .= "<meta property='og:description' content='{$pagina->pagina_descricao}'/>\n";
        $seo .= "<meta property='og:locale' content='pt_BR'/>\n";
        $seo .= "<meta property='og:image' content='{$base_url}{$pagina->pagina_imagem_destacada}'/>\n";
        $seo .= "<meta property='article:author' content='{$this->ConfigSysten->config_facebook}'/>\n";
        $seo .= "<meta property='article:publisher' content='{$this->ConfigSysten->config_facebook}'/>\n";
        //Twitter
        $seo .= "<meta name='twitter:card' content='summary_large_image'/>\n";
        $seo .= "<meta name='twitter:site' content='{$canonical}'/>\n";
        $seo .= "<meta name='twitter:title' content='{$pagina->pagina_titulo}'/>\n";
        $seo .= "<meta name='twitter:description' content='{$pagina->pagina_descricao}'/>\n";
        if ($this->ConfigSysten->config_seo == "Ativo"):
            return $seo;
        else:
            return FALSE;
        endif;
    }

    /**
     * <b>Create Sitemap<b>
     * Cria o sitemap se não houver, caso exista atualiza.
     */
    public function create_sitemap() {
        $paginas = $this->PaginasModel->getPaginas()->result();
        $artigos = $this->PaginasModel->getArtigos()->result();

        $file = "sitemap.xml";
        if (file_exists($file)):
            unlink($file);
        endif;

        $xml = fopen("sitemap.xml", "a");

        $sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
$sitemap .= '
<?xml-stylesheet type="text/xsl" href="sitemap.xsl"?>';
$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

    $dateArr = explode(' ', date('Y-m-d H:i:s'));
    $lastmod = $dateArr[0] . 'T' . $dateArr[1] . '-03:00';
    $sitemap .= '<url>';
        $sitemap .= '<loc>' . base_url() . '</loc>';
        $sitemap .= '<lastmod>' . $lastmod . '</lastmod>';
        $sitemap .= '<changefreq>daily</changefreq>';
        $sitemap .= '<priority>1.0</priority>';
        $sitemap .= '</url>';
    foreach ($paginas as $pagina):
    $date = date('Y-m-d H:i:s', strtotime($pagina->pagina_data_alteracao));
    $dateArr = explode(' ', $date);
    $lastmod = $dateArr[0] . 'T' . $dateArr[1] . '-03:00';
    $sitemap .= '<url>';
        $sitemap .= '<loc>' . base_url($pagina->pagina_slug) . '</loc>';
        $sitemap .= '<lastmod>' . $lastmod . '</lastmod>';
        $sitemap .= '<changefreq>weekly</changefreq>';
        $sitemap .= '<priority>0.7</priority>';
        $sitemap .= '</url>';
    endforeach;

    foreach ($artigos as $artigo):
    $datePost = date('Y-m-d H:i:s', strtotime($artigo->pagina_data_alteracao));
    $dateArrPost = explode(' ', $datePost);
    $lastmodPost = $dateArrPost[0] . 'T' . $dateArrPost[1] . '-03:00';
    $sitemap .= '<url>';
        $sitemap .= '<loc>' . base_url($artigo->pagina_slug) . '</loc>';
        $sitemap .= '<lastmod>' . $lastmodPost . '</lastmod>';
        $sitemap .= '<changefreq>weekly</changefreq>';
        $sitemap .= '<priority>0.9</priority>';
        $sitemap .= '</url>';
    endforeach;

    $sitemap .= '</urlset>';

fwrite($xml, $sitemap);
fclose($xml);
}

public function site_map_ping() {
$siteMpPing = array();
$siteMpPing['google'] = "http://www.google.com/webmasters/tools/ping?sitemap=" . base_url() . 'sitemap.xml';
$siteMpPing['bing'] = "http://www.bing.com/webmaster/ping.aspx?siteMap=" . base_url() . 'sitemap.xml';

foreach ($siteMpPing as $siteUrl):
$ch = curl_init($siteUrl);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_exec($ch);
curl_close($ch);
endforeach;
}

//**************************************//
//********** Usuários Online **********//
//************************************//
public function CheckOnline($Cache = null) {
$this->Date = date('Y-m-d');
$this->Cache = ( (int) $Cache ? $Cache : 5 );

if (empty($_SESSION['useronline'])):
$this->setUsuario();
else:
$this->sessionUpdate();
endif;

$this->Date = null;
}

public function getOnlineVisits() {
$this->Date = date('Y-m-d');
$this->load->model('SiteViewsOnlineModel');
$userOnline = $this->SiteViewsOnlineModel->getUsersOnline($this->Date);
return $userOnline->result();
}

//cadastra usuário online na tabela
private function setUsuario() {
$_SESSION['useronline'] = [
"online_session" => session_id(),
"online_startview" => date('Y-m-d H:i:s'),
"online_endview" => date('Y-m-d H:i:s', strtotime("+{$this->Cache}minutes")),
"online_ip" => $this->input->server('REMOTE_ADDR'),
"online_url" => $this->input->server('REQUEST_URI'),
"online_agent" => $this->input->server('HTTP_USER_AGENT')
];
$this->CheckBrowser();

$sesOnline = $_SESSION['useronline'];

$this->load->model('SiteViewsOnlineModel');
$this->SiteViewsOnlineModel->cadUserOnline($sesOnline);
}

//Atualiza sessão do usuário!
private function sessionUpdate() {
$_SESSION['useronline']['online_endview'] = date('Y-m-d H:i:s', strtotime("+{$this->Cache}minutes"));
$_SESSION['useronline']['online_url'] = $this->input->server('REQUEST_URI');
$dados = array(
"online_endview" => $_SESSION['useronline']['online_endview'],
"online_url" => $_SESSION['useronline']['online_url']
);
$this->load->model('SiteViewsOnlineModel');
$update = $this->SiteViewsOnlineModel->updateUserOnline($dados, $_SESSION['useronline']['online_session']);
if ($update == 0):
$this->setUsuario();
endif;
}

//Identifica navegador do usuário!
private function CheckBrowser() {
$Browser = $_SESSION['useronline']['online_agent'];
if (strpos($Browser, 'Chrome')):
$_SESSION['useronline']['agent_name'] = 'Chrome';
elseif (strpos($Browser, 'Firefox')):
$_SESSION['useronline']['agent_name'] = 'Firefox';
elseif (strpos($Browser, 'MSIE') || strpos($Browser, 'Trident/')):
$_SESSION['useronline']['agent_name'] = 'IE';
else:
$_SESSION['useronline']['agent_name'] = 'Outros';
endif;
}

/**
* <b>getTemplates</b>
* Retorna os temas disponiveis para usar no site.
* @return Array
*/
public static function getTemplates() {
$path = "themes";
$themes = scandir($path);
$return = [];
$unset = ['errors', 'admin', 'manutencao', 'install'];
foreach ($themes as $theme):
if (!is_file($theme) && !is_dir($theme) && !in_array($theme, $unset)):
$return[] = $theme;
endif;
endforeach;
return $return;
}

/**
* <b>checkDbExists</b>
* Verifica se o banco de dados foi instalado. Caso não foi redimensiona para a página de instalação!
*/
private function checkDbExists() {
if (!$this->db->table_exists('configuracoes')):
redirect('install');
endif;
}

}