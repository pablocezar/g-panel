<?php

/**
 * <b>LinkPhone</b>
 * Cria link para ação de ligação em números de telefone!
 * @param type $Num = Número do telefone / celular;
 * @param type $Icon = Ícone do link, pode ser Null para não exibir icone;
 * @return string
 */
function LinkPhone($Num, $Icon) {
    $Icon= ($Icon = NULL ? '' : "{$Icon}");
    $linkNum = str_replace(['(', ')', '-', ' '], '', $Num);
    $return = "<a href=\"tel:+55".$linkNum."\" rel=\"nofollow\" title=\"Clique e Ligue!\">".$Icon.' '.$Num."</a>";
    return $return;
}

/**
 * <b>LinkEmail</b>
 * Cria link para envio de e-mail.
 * @param type $Email = Email válido;
 * @param type $Icon = Ícone do link, pode ser Null para não exibir icone;
 * @return string
 */
function LinkEmail($Email, $Icon = NULL) {
    $Icon= ($Icon = NULL ? '' : "{$Icon}");
    $return = "<a href=\"mailto:".$Email."\" rel=\"nofollow\" title=\"Envie um email para {$Email}\">".$Icon.' '.$Email."</a>";
    return $return;
}
