<?php

/**
 * <b>Thumbnail</b>
 * Gera a thumbnail de uma imagem, este helper é dependente do plugin timthumb.php. Sem este plugin na raiz do diretório e renomeado para tim.php
 * este helper não funcionará.
 * @param type $W = Largura;
 * @param type $H = Altura;
 * @param type $Zc = Zoom (valores de 1 á 3);
 * @param type $M = Marca D'água (TRUE/FALSE, é necessário existir a imagem "watermark.png" na pasta images do template.);
 * @param type $Imagem = Caminho da Imagem que será redimensionada.
 * @return string
 */
function Thumbnail($W, $H, $Imagem, $Zc = 1, $M = FALSE) {
    $get = get_instance();
    $theme = $get->db->get('configuracoes')->row();
    $MarcaDagua = ($M === TRUE) ? "&m=".base_url('themes/').$theme->config_tema_site.'/images/watermark.png'."&p=2" : '';
    $return = base_url() . 'tim.php?src=' . $Imagem . "&w={$W}&h={$H}&zc=$Zc{$MarcaDagua}";

    return $return;
}
