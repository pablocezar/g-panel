<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

    /**
     * <b>User</b>
     * Armazena dados do usuário logado.
     * @var type String
     */
    public $User;

    /**
     * <b>OnlineVisits</b>
     * Retorna o numero de visitantes online no site.
     * @var type String
     */
    public $OnlineVisits;

    /**
     * <b>Theme</b>
     * Armazena o nome do tema a ser carregado.
     * @var type String
     */
    private $Theme;

    /**
     * <b>PathTheme</b>
     * Armezena o caminho da pasta do tema.
     * @var type String
     */
    public $PathTheme;

    /**
     * <b>CountPages</b>
     * <b>CountPages</b>
     * Armezena o total de páginas / artigos .
     * @var type String
     */
    public $TotalPaginas;
    public $TotalArtigos;

    public function __construct() {
        parent::__construct();
        $this->checkSession();
        $this->load->helper('text');
        $this->load->library('pagination');
        $this->load->helper('imagem');

        $this->User = $this->UsuariosModel->getUsuarioId($this->SessionData['usuario_id'])->row();
        $this->OnlineVisits = $this->getOnlineVisits();
        $this->Theme = $this->_getTheme();
        $this->PathTheme = base_url() . 'themes/' . $this->Theme . '/';
        $this->TotalPaginas = count($this->PaginasModel->getPaginas()->result());
        $this->TotalArtigos = count($this->PaginasModel->getArtigos()->result());
    }

    public function index() {
        $this->load->file(__Dir__ . '/Gapi.class.php');
        $dados['view'] = "{$this->Theme}/modules/dashboard/dashboard";
        $dados['trafegoMensal'] = $this->configAnalytics('month');
        $dados['trafegoPaginas'] = $this->configAnalytics('pageTitle');
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();


        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ******************* getAjaxGA *********************
     * ***************************************************
     */
    public function getAjaxGA() {
        $this->load->file(__Dir__ . '/Gapi.class.php');
        $repleaceIni = str_replace('/', '-', $this->FormPost['dataIni']);
        $repleaceFim = str_replace('/', '-', $this->FormPost['dataFim']);

        $dataIni = date('Y-m-d', strtotime($repleaceIni));
        $dataFim = date('Y-m-d', strtotime($repleaceFim));

        $ga = $this->configAnalytics(['month', 'pageTitle'], $dataIni, $dataFim);
        $result = ['trafegos', 'paginas'];
        foreach ($ga as $linha):
            $visits[] = $linha->getvisits();
            $pageviews[] = $linha->getpageviews();
            $newUsers[] = $linha->getnewUsers();
            $organicSearches[] = $linha->getorganicSearches();

            $result['paginas'][] = ["getpageviews" => $linha->getpageviews(), "getpageTitle" => $linha->getpageTitle(), "getorganicSearches" => $linha->getorganicSearches(), "getbounceRate" => (int) $linha->getbounceRate()];
        endforeach;
        $result['trafegos']['visits'] = array_sum($visits);
        $result['trafegos']['pageviews'] = array_sum($pageviews);
        $result['trafegos']['newUsers'] = array_sum($newUsers);
        $result['trafegos']['organicSearches'] = array_sum($organicSearches);

        arsort($result['paginas']);

        $result['paginas'] = array_slice($result['paginas'], 0, 10);

        echo json_encode($result);
    }

    /**
     * ***************************************************
     * ******************** Paginas **********************
     * ***************************************************
     */
    public function paginas() {
        $dados['view'] = "{$this->Theme}/modules/paginas/paginas";
        $dados['paginas'] = $this->_configPaginacao('paginas');
        $dados['quantPaginas'] = count($dados['paginas']);
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function formPagina($id = NULL) {
        $dados['view'] = "{$this->Theme}/modules/paginas/form-pagina";
        $dados['todas_paginas'] = $this->PaginasModel->getPaginas()->result();
        $dados['todas_galerias'] = $this->GaleriasModel->getGalerias()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        if ($id != null):
            $dados['pagina'] = $this->PaginasModel->getPaginaId($id)->row();
//            $dados['ordenar_menu'] = $this->PaginasModel->menuPrincipal()->result();
//            $dados['pagina_mae'] = $this->PaginasModel->retornaPaginaMae($id)->row();
        endif;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ******************** Artigos **********************
     * ***************************************************
     */
    public function artigos() {
        $dados['view'] = "{$this->Theme}/modules/artigos/artigos";
        $dados['artigos'] = $this->_configPaginacao('paginas', 'artigo');
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function formArtigo($id = NULL) {
        $dados['view'] = "{$this->Theme}/modules/artigos/form-artigo";
        $dados['categorias'] = $this->CategoriasModel->getCategorias()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();
        $dados['todas_galerias'] = $this->GaleriasModel->getGalerias()->result();

        if ($id != null):
            $dados['artigo'] = $this->PaginasModel->getPaginaId($id)->row();
        endif;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function artigosAgendados($id = NULL) {
        $dados['view'] = "{$this->Theme}/modules/artigos/artigos-agendados";
        $dados['artigos'] = $this->_configPaginacao('paginas', 'artigo agendado');
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function categoriasArtigos($id = NULL) {
        $dados['view'] = "{$this->Theme}/modules/artigos/categorias-artigos";
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();
        if ($id != null):
            $dados['categoria'] = $this->CategoriasModel->getCategoriaId($id)->row();
        endif;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ******************** Usuarios *********************
     * ***************************************************
     */
    public function usuarios($id = '') {
        $dados['view'] = "{$this->Theme}/modules/usuarios/usuarios";
        $dados['usuarios'] = $this->UsuariosModel->getUsuarios()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();
        if ($id != ''):
            $dados['usuario'] = $this->UsuariosModel->getUsuarioId($id)->row();
        endif;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function configUsuario($id) {
        $dados['view'] = "{$this->Theme}/modules/configuracoes/configuracoes-usuario";
        $dados['usuario'] = $this->UsuariosModel->getUsuarioId($id)->row();
        $dados['configuracoes'] = $this->ConfigSysten;
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ***************** Configurações *******************
     * ***************************************************
     */
    public function configuracoes() {
        $dados['view'] = "{$this->Theme}/modules/configuracoes/configuracoes";
        $dados['usuarios'] = $this->UsuariosModel->getUsuarios()->result();
        $dados['configuracoes'] = $this->ConfigSysten;
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * *********************** Menus *********************
     * ***************************************************
     */
    public function menus() {
        $dados['view'] = "{$this->Theme}/modules/menus/menus";
        $dados['usuarios'] = $this->UsuariosModel->getUsuarios()->result();
        $dados['todas_paginas'] = $this->PaginasModel->getAllPages()->result();
        $dados['categorias'] = $this->CategoriasModel->getCategorias()->result();
        $dados['menu_principal'] = $this->MenusModel->getMenuPrincipal()->result();
        $dados['menu_rodape'] = $this->MenusModel->getMenuRodape()->result();
        $dados['configuracoes'] = $this->ConfigSysten;
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ********************** Emails *********************
     * ***************************************************
     */
    public function emails() {
        $dados['view'] = "{$this->Theme}/modules/emails/emails";
        $dados['todos_emails'] = $this->EmailsModel->getEmails()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ********************** Leads **********************
     * ***************************************************
     */
    public function popups($id = null) {
        $this->load->model("PopupsModel");
        $this->PopupsModel->checkDbExists();
        $dados['view'] = "{$this->Theme}/modules/popups/popups";
        $dados['todas_paginas'] = $this->PaginasModel->getPaginas()->result();
        $dados['todos_popups'] = $this->PopupsModel->getAll()->result();
        if (isset($id)):
            $dados['popup'] = $this->PopupsModel->get($id)->row();
        endif;
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ********************** Leads **********************
     * ***************************************************
     */
    public function leads() {
        $dados['view'] = "{$this->Theme}/modules/leads/leads";
        $dados['todos_leads'] = $this->LeadsModel->getLeads()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ******************* Accordions ********************
     * ***************************************************
     */
    public function accordions() {
        $this->load->model('AccordionsModel');
        $dados['view'] = "{$this->Theme}/modules/accordions/accordions";
        $dados['todos_accordion'] = $this->AccordionsModel->getAll()->result();
        $dados['todas_paginas'] = $this->PaginasModel->getPaginas()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function formAccordion($id = NULL) {
        $this->load->model('AccordionsModel');
        if ($id != NULL):
            $dados['accordion'] = $this->AccordionsModel->get($id)->row();
            $dados['accordion_lines'] = $this->AccordionsModel->getLines($id)->result();
        endif;
        $dados['view'] = "{$this->Theme}/modules/accordions/form-accordions";
        $dados['todos_accordion'] = $this->AccordionsModel->getAll()->result();
        $dados['todas_paginas'] = $this->PaginasModel->getPaginas()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ************* Regiões de Atendimento **************
     * ***************************************************
     */
    public function regioes_atendimento($id = null) {
        $dados['view'] = "{$this->Theme}/modules/regioes-atendimento/regioes-atendimento";

        if ($id != NULL):
            $dados['regiao'] = $this->RegioesAtendimentoModel->getRegiaoId($id)->row();
        endif;

        $dados['regioes'] = $this->RegioesAtendimentoModel->getRegioes()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ******************** Galerias *********************
     * ***************************************************
     */
    public function galerias() {
        $dados['view'] = "{$this->Theme}/modules/galerias/galerias";
        $dados['todas_galerias'] = $this->GaleriasModel->getGalerias()->result();
        $dados['imagens_galerias'] = $this->ImagensModel->getImagens()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function novaGaleria() {
        $dados['view'] = "{$this->Theme}/modules/galerias/nova-galeria";
        $dados['todas_galerias'] = $this->GaleriasModel->getGalerias()->result();
        $dados['imagens_galerias'] = $this->ImagensModel->getImagens()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function editarGaleria($id) {
        $dados['view'] = "{$this->Theme}/modules/galerias/editar-galeria";
        $dados['galeria'] = $this->GaleriasModel->getGaleriaId($id)->row();
        $dados['imagens_galeria'] = $this->ImagensModel->getImagemByGaleria($id)->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * ***************************************************
     * ******************** Banners **********************
     * ***************************************************
     */
    public function banners($id = null) {
        $dados['view'] = "{$this->Theme}/modules/banners/banners";
        $dados['todos_emails'] = $this->EmailsModel->getEmails()->result();
        $dados['proxPostagens'] = $this->PaginasModel->getProxPostagens()->result();
        $dados['banners'] = $this->BannersModel->getBanners()->result();

        if (isset($id)):
            $dados['banner'] = $this->BannersModel->getBannerId($id)->row();
        endif;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * <b>_getTheme</b>
     * Retorna Theme da Área Administrativa.
     * @return String
     */
    private function _getTheme() {
        $theme = 'admin';
        return $theme;
    }

    private function _configPaginacao($table, $tipo = 'pagina') {
        $base = ($tipo == "pagina") ? 'paginas' : 'artigos';
        $config['base_url'] = base_url('admin') . '/' . $base;

        //      Full Tag
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        //      Primeiro Link
        $config['first_link'] = 'Primeiro';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        //      Último Link
        $config['last_link'] = 'Último';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //      Próximo Link
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        //      Link Anterior
        $config['prev_link'] = '«';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        //      Páginas Correntes
        $config['cur_tag_open'] = "<li class='active'><span>";
        $config['cur_tag_close'] = "</span></li>";
        //      Página Ativa
        $config['num_tag_open'] = "<li>";
        $config['num_tag_close'] = "</li>";

        $quey = $this->_selectQuery($tipo);
        $config['total_rows'] = count($quey->result());
        $config['per_page'] = 8;
        $this->pagination->initialize($config);

        $quantidade = $config['per_page'];
        $inicio = $this->uri->segment(3);

        $dados = array(
            "links" => $this->pagination->create_links(),
            $table => $this->_selectQuery($tipo, $quantidade, $inicio)
        );
        return $dados;
    }

    private function _selectQuery($tipo, $quantidade = null, $inicio = null) {
        if ($tipo == "pagina"):
            $quey = $this->PaginasModel->getPaginas($quantidade, $inicio, $tipo);
        elseif ($tipo == "artigo"):
            $quey = $this->PaginasModel->getArtigos($quantidade, $inicio, $tipo);
        elseif ($tipo == "artigo agendado"):
            $quey = $this->PaginasModel->getArtigosAgendados($quantidade, $inicio);
        endif;
        return $quey;
    }

}
