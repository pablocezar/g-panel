<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Install extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->check();
    }

    public function index() {
        $this->load->view("install/index");
    }

    public function up() {
        $this->load->library('migration');

        $data = $this->input->post();

        $dados['usuario_nome'] = $this->input->post('nome');
        $dados['usuario_email'] = $this->input->post('email');
        $dados['usuario_senha'] = md5($this->input->post('senha'));

        $this->load->model("UsuariosModel");
        $this->UsuariosModel->updateUsuario(1, $dados);

        echo json_encode('Instalação realizada com sucesso!');
    }

    private function check() {
        if ($this->db->table_exists('configuracoes')):
            redirect(base_url());
        endif;
    }

}
