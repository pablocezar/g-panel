<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }
    
    public function artigosAgendados() {
        $artigos = $this->PaginasModel->getArtigosAgendados()->result();
//        $date = date('2017-03-14 03:00:00');
        $date = date('Y-m-d H:i:00');

        foreach ($artigos as $linha):
            if ($linha->pagina_postagem_programada == $date):
                $this->_publicaArtigo($linha->pagina_id);
            endif;
        endforeach;
    }

    private function _publicaArtigo($id) {
        $dados = array("pagina_postagem_programada" => NULL, "pagina_data_criacao" => date('Y-m-d H:i:s'));
        $query = $this->PaginasModel->updatePagina($id, $dados);
        if ($query):
            if ($this->ConfigSysten->config_seo == "Ativo"):
                $this->create_sitemap();
                $this->_siteMap_ping();
            endif;
        endif;
    }

    private function _siteMap_ping() {
        $gzip = gzopen('sitemap.xml.gz', 'w9');
        $gmap = file_get_contents('sitemap.xml');
        gzwrite($gzip, $gmap);
        gzclose($gzip);
        $this->site_map_ping();
    }

}
