<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    /**
     * <b>Theme</b>
     * Armazena o tema do site
     * @var type STRING
     */
    private $Theme;

    public function __construct() {
        parent::__construct();
        $this->Theme = $this->_getTheme();
    }

    public function index() {
        $view = "{$this->Theme}/modules/login/login";
        $dados['config'] = $this->ConfigSysten;
        $dados['theme'] = base_url() . 'themes/' . $this->Theme . '/';
        if (isset($this->FormPost['btn_logar'])):
            $this->_logar();
        endif;

        $this->load->view($view, $dados);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('admin');
    }
    
    public function ajaxLogar() {
        if (!$this->FormPost):
            redirect(base_url('admin'));
        endif;
        unset($this->FormPost['btn_logar']);
        $this->FormPost['usuario_senha'] = md5($this->input->post('usuario_senha'));

        $logar = $this->UsuariosModel->logar($this->FormPost);
//        sleep(2);
        if ($logar->num_rows() > 0):
            $usuario = $logar->row();
            $usuario_info = array(
                "usuario_id" => $usuario->usuario_id,
                "usuario_nome" => $usuario->usuario_nome,
                "usuario_nivel_acesso" => $usuario->usuario_nivel_acesso,
                "usuario_foto" => $usuario->usuario_foto,
                "logado" => true
            );
            $this->session->set_userdata($usuario_info);
            $name = explode(' ', $usuario_info['usuario_nome']);
            $msg = ['success', "Olá {$name[0]}, Seja Bem Vindo :)", true];
            echo json_encode($msg);
        else:
            $msg = ['danger', "Usuário ou Senha Inválidos!"];
            echo json_encode($msg);
        endif;
    }

    /**
     * <b>_logar<b>
     * Verifica a autenticidade das informações enviadas via post do formulário de login e realiza a autentificação no área admnistrativa do site.
     */
    private function _logar() {
        unset($this->FormPost['btn_logar']);
        $this->FormPost['usuario_senha'] = md5($this->input->post('usuario_senha'));

        $logar = $this->UsuariosModel->logar($this->FormPost);
        if ($logar->num_rows() > 0):
            $usuario = $logar->row();
            $usuario_info = array(
                "usuario_id" => $usuario->id,
                "usuario_nome" => $usuario->nome,
                "usuario_nivel_acesso" => $usuario->nivel_acesso,
                "usuario_foto" => $usuario->foto,
                "logado" => true
            );
            $this->session->set_userdata($usuario_info);
            redirect('admin');
        else:
            $this->session->set_flashdata("erro", "Usuário ou senha incorretos!");
            redirect('login');
        endif;
    }

    /**
     * <b>_getTheme</b>
     * Retorna Theme do Site.
     * @return String
     */
    private function _getTheme() {
//        $theme = $this->ConfigSysten->config_tema_admin;
        $theme = "admin";
        return $theme;
    }
    

}
