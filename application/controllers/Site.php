<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller {

    /**
     * <b>Theme</b>
     * Armazena o tema do site
     * @var type STRING
     */
    private $Theme;

    /**
     * <b>Pagina</b>
     * Armazena valores da Página
     * @var type ARRAY
     */
    public $Pagina;

    public function __construct() {
        parent::__construct();
        $this->CheckOnline();
        $this->Theme = $this->_getTheme();
        $this->load->helper('text');
        $this->Pagina = $this->_getPage();

        $this->load->helper('imagem');
        $this->load->helper('data');
        $this->load->helper('link');
    }

    public function index() {
        $dados['pagina'] = $this->Pagina;
        $dados['title'] = $this->Pagina->pagina_titulo;
        $dados['view'] = $this->_modelo_pagina();
        $dados['menu_principal'] = $this->_menu_principal();
        $dados['menu_rodape'] = $this->MenusModel->getMenuRodape()->result();
        $dados['theme'] = base_url('themes') . '/' . $this->Theme;
        $dados['paginas_destaque'] = $this->PaginasModel->getPaginasDestaque()->result();
        $dados['ultimos_artigos'] = $this->PaginasModel->getUltimosArtigos()->result();
        $dados['artigos'] = $this->PaginasModel->getArtigos()->result();
        $dados['artigos_destaque'] = $this->PaginasModel->getArtigosDestaque()->result();
        $dados['artigos_paginados'] = $this->_configPaginacao('paginas', 'artigo');
//        $dados['artigos_paginados'] = $this->_getArigosCategoria('artigos');
        $dados['categorias'] = $this->CategoriasModel->getCategorias()->result();
        $dados['categorias_destaque'] = $this->CategoriasModel->getCategoriaDestacadas()->result();
        $dados['regioes_atend'] = $this->RegioesAtendimentoModel->getRegioes()->result();
        $dados['bread_crumb'] = $this->_breadCrumps();
        $dados['metasSeo'] = $this->SEO($this->Pagina);
        $dados['banners'] = $this->BannersModel->getBanners()->result();
        $dados['galeria'] = $this->_galeriaFotos();
        $dados['accordion'] = $this->getAccordions();
        $dados['popup'] = $this->getPopup();
        $dados['config'] = $this->ConfigSysten;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function categoria() {
        $categoria = $this->uri->segment(2);
        if ($categoria == null):
            redirect('404');
        endif;

        $dados['pagina'] = $this->Pagina;
        $dados['categoria'] = $this->CategoriasModel->getCategoriaUrl($this->uri->segment(2))->row();
        if ($dados['categoria'] == null):
            redirect('404');
        endif;

        $dados['title'] = "Categoria {$dados['categoria']->categoria_titulo}";
        $dados['view'] = $this->_modelo_pagina();
        $dados['menu_principal'] = $this->_menu_principal();
        $dados['menu_rodape'] = $this->MenusModel->getMenuRodape()->result();
        $dados['theme'] = base_url('themes') . '/' . $this->Theme;
        $dados['artigos'] = $this->PaginasModel->getArtigos()->result();
        $dados['artigos_paginados'] = $this->_getArigosCategoria($this->uri->segment(2));
        $dados['categorias'] = $this->CategoriasModel->getCategorias()->result();
        $dados['bread_crumb'] = $this->_breadCrumps();
        $dados['regioes_atend'] = $this->RegioesAtendimentoModel->getRegioes()->result();
        $dados['metasSeo'] = $this->SEO($this->Pagina);
        $dados['config'] = $this->ConfigSysten;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    public function search() {

        if (!$this->FormPost) {
            redirect(base_url());
        }

        $dados['pagina'] = $this->Pagina;
        $dados['title'] = "Pesquisa";
        $dados['view'] = $this->_modelo_pagina();
        $dados['menu_principal'] = $this->_menu_principal();
        $dados['menu_rodape'] = $this->MenusModel->getMenuRodape()->result();
        $dados['theme'] = base_url('themes') . '/' . $this->Theme;
        $dados['metasSeo'] = $this->SEO($this->Pagina);
        $dados['search'] = $this->PaginasModel->search($this->FormPost['search'])->result();
        $dados['regioes_atend'] = $this->RegioesAtendimentoModel->getRegioes()->result();
        $dados['config'] = $this->ConfigSysten;

        $this->load->view("{$this->Theme}/index", $dados);
    }

    /**
     * <b>_getTheme</b>
     * Retorna Theme do Site.
     * @return String
     */
    private function _getTheme() {
        if ($this->ConfigSysten->config_manutencao == 0 || $this->session->userData('logado') == TRUE):
            $theme = $this->ConfigSysten->config_tema_site;
        else:
            $theme = 'manutencao';
        endif;
        return $theme;
    }

    /**
     * <b>_getPage</b>
     * Retorna a página, caso o resultado for NULL manda para a página 404.
     * @return ARRAY
     */
    private function _getPage() {
        $url = $this->uri->segment(1);
        if ($url == NULL):
            $result = (object) array(
                        "pagina_titulo" => $this->ConfigSysten->config_nome_empresa,
                        "pagina_descricao" => $this->ConfigSysten->config_descricao,
                        "pagina_imagem_destacada" => "themes/{$this->Theme}/images/social-img.png",
                        "pagina_id_pagina_mae" => 0,
                        "pagina_id_categoria" => 0
            );
            return $result;
        elseif ($url == "categoria"):
            $categoria = $this->CategoriasModel->getCategoriaUrl($this->uri->segment(2))->row();
            $result = (object) array(
                        "pagina_titulo" => $categoria->categoria_titulo,
                        "pagina_descricao" => $categoria->categoria_descricao,
                        "pagina_imagem_destacada" => base_url() . $categoria->categoria_imagem_destacada,
                        "pagina_id_pagina_mae" => 0,
                        "pagina_id_categoria" => 0,
                        "cat" => true
            );
        else:
            $pagina = $this->PaginasModel->getPaginaUrl($this->uri->segment(1))->row();

            if ($pagina != NULL):
                $result = $pagina;
            else:
                $result = (object) array(
                            "pagina_titulo" => $this->ConfigSysten->config_nome_empresa,
                            "pagina_descricao" => $this->ConfigSysten->config_descricao,
                            "pagina_imagem_destacada" => "themes/{$this->Theme}/images/social-img.png",
                            "pagina_id_pagina_mae" => 0,
                            "pagina_id_categoria" => 0,
                            "cat" => true
                );
            endif;
        endif;
        return $result;
    }

    private function _getArigosCategoria($urlCategoria) {
        $categoria = $this->CategoriasModel->getCategoriaUrl($urlCategoria)->row();
        if ($categoria != null):
            //Parametros de paginação
            $base_url = base_url('categoria') . '/' . $urlCategoria;
            $total_rows = $this->PaginasModel->getPaginaCategoriaId($categoria->categoria_id)->num_rows();
            $tabela = "paginas";
            $query = "getPaginaCategoria";

            $dados = $this->_paginacaoArtigosCat($base_url, $total_rows, $tabela, $categoria->categoria_id, $query);
        else:
            $dados = false;
        endif;
        return $dados;
    }

    /**
     * <b>_modelo_pagina</b>
     * Gerencia modelos de páginas de acordo com a url apresentada.
     * <b>Exemplo:</b><br>
     * url = "blog" (será carregada a view de modelo BLOG)<br>
     * url = "categoria" (será carregada a view de modelo CATEGORIA)<br>
     * e assim por diante.
     * Conforme houver necessidades de adicionar mais modelos de páginas poderão ser acrecentados em MY_Controller/modelo_pagina(). 
     */
    private function _modelo_pagina() {
        $url = $this->uri->segment(1);
        if ($url == null):
            $view = $this->ConfigSysten->config_tema_site . '/' . "home";
        elseif ($url == 'search'):
            $view = $this->ConfigSysten->config_tema_site . '/' . "search";
        elseif ($url == "categoria" && isset($this->Pagina->cat)):
            $view = $this->ConfigSysten->config_tema_site . '/' . "categoria";
        elseif (!isset($this->Pagina->pagina_tipo)):
            $view = $this->ConfigSysten->config_tema_site . '/' . "404";
        elseif ($url == $this->ConfigSysten->config_pag_lista_artigos):
            $view = $this->ConfigSysten->config_tema_site . '/' . "blog";
        elseif ($this->Pagina->pagina_tipo == 'artigo'):
            $view = $this->ConfigSysten->config_tema_site . '/' . "post";
        elseif ($url == "contato"):
            $view = $this->ConfigSysten->config_tema_site . '/' . "contato";
        else:
            $view = $this->ConfigSysten->config_tema_site . '/' . "pages";
        endif;

        return $view;
    }

    /**
     * <b>Menu Principal</b>
     * Retorna menu principal
     * 
     */
    public function _menu_principal() {
        $menu = $this->MenusModel->getMenuPrincipal()->result();

        $menu_principal = "";
        foreach ($menu as $linha):
            $sub_menu = $this->MenusModel->submenu($linha->menu_id)->result();
            $link = (!preg_match("/http/", $linha->menu_link)) ? base_url($linha->menu_link) : $linha->menu_link;
            $target = ($linha->menu_target_blank == 'on') ? "target=\"_blank\"" : '';
            $icone = ($linha->menu_icone != '') ? '<i class="' . $linha->menu_icone . '"></i>' : '';

            if (count($sub_menu) == 0):
                $menu_principal .= "<li><a title='" . $linha->menu_titulo . "' href='" . $link . "' {$target}>{$icone} {$linha->menu_titulo}</a></li>";
            else:
                $menu_principal .= "<li class='dropdown'><a href='" . $link . "' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false' >{$icone} {$linha->menu_titulo} <span class='caret'></span></a>";
                $menu_principal .= "<ul class='dropdown-menu'>";
                foreach ($sub_menu as $sub_linha):
                    $sublink = (!preg_match("/http/", $sub_linha->menu_link)) ? base_url($sub_linha->menu_link) : $sub_linha->menu_link;
                    $target = ($sub_linha->menu_target_blank == 'on') ? "target=\"_blank\"" : '';
                    $menu_principal .= "<li><a title='" . $sub_linha->menu_titulo . "' href='" . $sublink . "' {$target}>{$sub_linha->menu_titulo}</a></li>";
                endforeach;
                $menu_principal .= "</ul>";
                $menu_principal .= "</li>";
            endif;
        endforeach;
        return $menu_principal;
    }

    /**
     * <b>_configPaginacao</b>
     * Retorna dados de uma tabela paginados.
     * @param STRING $table
     * @param STRING $tipo
     * @return ARRAY
     */
    private function _configPaginacao($table, $tipo) {
        $this->load->library('pagination');
        $config['base_url'] = base_url($this->ConfigSysten->config_pag_lista_artigos);

        //      Full Tag
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        //      Primeiro Link
        $config['first_link'] = 'Primeiro';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        //      Último Link
        $config['last_link'] = 'Último';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //      Próximo Link
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        //      Link Anterior
        $config['prev_link'] = '«';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        //      Páginas Correntes
        $config['cur_tag_open'] = "<li class='active'><span>";
        $config['cur_tag_close'] = "</span></li>";
        //      Página Ativa
        $config['num_tag_open'] = "<li>";
        $config['num_tag_close'] = "</li>";

        $total = $this->PaginasModel->getArtigos($tipo)->result();
        $config['total_rows'] = count($total);
        $config['per_page'] = $this->ConfigSysten->config_num_pag_paginacao;
        $this->pagination->initialize($config);

        $quantidade = $config['per_page'];

        $inicio = $this->uri->segment(2);
        $dados = array(
            "links" => $this->pagination->create_links(),
            $table => $this->PaginasModel->getArtigos($quantidade, $inicio, $tipo),
        );

        return $dados;
    }

    /**
     * <b>_paginacaoArtigosCat</b>
     * Método de páginação de resultados de uma lista em banco de dados.
     * @param (string) $base_url base_url inicial da paginação
     * @param (int) $total_rows total de linhas no banco de dados a serem paginadas
     * @param (string) $tabela tabela que sera paginada
     * @param (string) $query query do model para paginação
     * @return (objeto)
     */
    private function _paginacaoArtigosCat($base_url, $total_rows, $tabela, $categoria, $query) {
        $this->load->library('pagination');

        $config['base_url'] = $base_url;
        $config['total_rows'] = $total_rows;
        $config['num_links'] = 4;
        $config['per_page'] = $this->ConfigSysten->config_num_pag_paginacao;
        //      Full Tag
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        //      Primeiro Link
        $config['first_link'] = 'Primeiro';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        //      Último Link
        $config['last_link'] = 'Último';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        //      Próximo Link
        $config['next_link'] = '»';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        //      Link Anterior
        $config['prev_link'] = '«';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        //      Páginas Correntes
        $config['cur_tag_open'] = "<li class='active'><span>";
        $config['cur_tag_close'] = "</span></li>";
        //      Página Ativa
        $config['num_tag_open'] = "<li>";
        $config['num_tag_close'] = "</li>";

        $this->pagination->initialize($config);

        $quantidade = $config['per_page'];

//        $inicio = ($this->uri->segment(3) == '') ? $this->uri->segment(2) : $this->uri->segment(3);
        $inicio = $this->uri->segment(3);
        $dados = array(
            "links" => $this->pagination->create_links(),
            $tabela => $this->PaginasModel->$query($categoria, $quantidade, $inicio)
        );


        return $dados;
    }

    private function _breadCrumps() {
        $base = base_url();
        
        $breadcrumb = '<ol class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

        if (isset($this->Pagina->pagina_id)):
            $breadcrumb .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
            $breadcrumb .= '<a itemprop="item" href="'.$base.'">
    <span itemprop="name">Home</span></a>';
            $breadcrumb .= '<meta itemprop="position" content="1" />';
            $breadcrumb .= '</li">';

            if ($this->Pagina->pagina_id_categoria != NULL):

                $breadcrumb .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
                $breadcrumb .= '<a itemprop="item" href="'.$base.'categoria/'.$this->Pagina->categoria_slug.'">
    <span itemprop="name">'.$this->Pagina->categoria_titulo.'</span></a>';
                $breadcrumb .= '<meta itemprop="position" content="2" />';
                $breadcrumb .= '</li">';
            endif;

            $breadcrumb .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
            $breadcrumb .= '<a itemprop="item" href="'.$base.$this->Pagina->pagina_slug.'">
    <span itemprop="name">'.$this->Pagina->pagina_titulo.'</span></a>';
            $breadcrumb .= '<meta itemprop="position" content="3" />';
            $breadcrumb .= '</li">';
            
        elseif ($this->uri->segment(1) == "categoria"):
            $breadcrumb .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
            $breadcrumb .= '<a itemprop="item" href="'.$base.'">
    <span itemprop="name">Home</span></a>';
            $breadcrumb .= '<meta itemprop="position" content="1" />';
            $breadcrumb .= '</li">';

            $breadcrumb .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
            $breadcrumb .= '<a itemprop="item" href="'.$base.'#/'.'">
    <span itemprop="name">Categorias</span></a>';
            $breadcrumb .= '<meta itemprop="position" content="2" />';
            $breadcrumb .= '</li">';

            $categoria = $this->CategoriasModel->getCategoriaUrl($this->uri->segment(2))->row();
            $breadcrumb .= '<li itemprop="itemListElement" itemscope
      itemtype="http://schema.org/ListItem">';
            $breadcrumb .= '<a itemprop="item" href="'.$base.'categoria/'.$categoria->categoria_slug.'">
    <span itemprop="name">'.$categoria->categoria_titulo.'</span></a>';
            $breadcrumb .= '<meta itemprop="position" content="3" />';
            $breadcrumb .= '</li">';
        endif;
        $breadcrumb .= '</ol">';
        return $breadcrumb;

    }

    private function _galeriaFotos() {
        if (isset($this->Pagina->pagina_id_galeria)):
            $imagens = $this->ImagensModel->getImagemByGaleria($this->Pagina->pagina_id_galeria)->result();
            $thumbnail = "";
            foreach ($imagens as $img):
                $thumbnail .= '<div class="col-xs-6 col-md-3 galeria">';
                $thumbnail .= '<a title="' . $img->imagem_descricao . '" href="' . base_url('medias/uploads/' . $img->imagem_imagem) . '" rel="gallery" class="thumbnail fancybox">';
                $thumbnail .= '<img src="' . Thumbnail(300, 200, 'medias/uploads/' . $img->imagem_imagem) . '" alt="' . $img->imagem_titulo . '" title="' . $img->imagem_titulo . '">';
                $thumbnail .= '</a>';
                $thumbnail .= '</div>';
            endforeach;
            return $thumbnail;
        else:
            return false;
        endif;
    }

    /**
     * <b>getAccordions</b>
     * Retorna accordion.
     * @return string
     */
    private function getAccordions() {
        $this->load->model("AccordionsModel");
        $this->Pagina->pagina_id = (!isset($this->Pagina->pagina_id) || $this->Pagina->pagina_id == NULL) ? 0 : $this->Pagina->pagina_id;
        $query = $this->AccordionsModel->getByPageId($this->Pagina->pagina_id);
        $content = '';
        if (count($query) > 0):
            foreach ($query as $linha):
                $content .= '<div class="js-acordion">';
                $content .= '<div class="js-acordion-title">' . $linha->accordion_titulo . '</div>';
                $content .= '<div class="js-acordion-content">' . $linha->accordion_conteudo . '</div>';
                $content .= '</div>';
            endforeach;
        endif;
        
        return $content;
    }
    
    private function getPopup() {
        $page = $this->Pagina;
        $this->load->model("PopupsModel");
        $popup = $this->PopupsModel->getByPagina($page->pagina_id)->row();
        return $popup;
    }

}
