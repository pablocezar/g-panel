<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Leads extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
    }

    /**
     * <b>formEmail</b>
     * Método parra envio de emails através do um formulário via post.
     */
    public function formEmail() {
        $ConfigSysten = $this->ConfigSysten;
        $this->configSMTP();

        $this->email->from($this->FormPost["email"], $this->FormPost["nome"]);
        $this->email->to($ConfigSysten->email);
        $this->email->subject($this->FormPost["assunto"]);
        $this->email->attach($this->_uploadAnexo());
        $this->email->message(''
                . '<b>Como Encontrei a  Empresa ' . $ConfigSysten->nome_empresa . ': ' . '</b>' . $this->FormPost["como_nos_achou"] . '<br>'
                . '<b>Email:</b>' . $this->FormPost["email"] . '<br>'
                . '<b>Telefone:</b>' . $this->FormPost["telefone"] . '<br>'
                . nl2br($this->FormPost["mensagem"]));

        $this->email->send();
    }
    
    public function deletaLead($id) {
        $query = $this->LeadsModel->deleteLead($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Lead excluido com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }
    
}
