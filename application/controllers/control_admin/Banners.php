<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
    }
    
    public function cadBanner() {
        $dados = $this->_trataDados();
        $query = $this->BannersModel->cadBanner($dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Banner Cadastrado com Sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }
    
    public function editBanner($id) {
        $dados = $this->_trataDados();
        $query = $this->BannersModel->editaBanner($id, $dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Informações Alteradas com Sucesso");
            redirect(base_url('admin/banners'));
        else:
            echo 'erro';
        endif;
    }
    
    public function deletaBanner($id) {
        $query = $this->BannersModel->deleteBanner($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Banner excluido com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }
    
    private function _trataDados() {
        $base_url = base_url();
        $dados = $this->FormPost;
        $dados['banner_imagem'] = str_replace($base_url, '', $this->FormPost['banner_imagem']);
        return $dados;
    }

}
