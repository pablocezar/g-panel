<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categorias extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper("text");
        $this->_criaSlug();
    }

    public function index() {
        
    }

    public function ajaxCheckCategoria($url) {
        $query = $this->CategoriasModel->getCategoriaUrl($url);
        if ($query->num_rows() > 0):
            $result = array('urlDuplicada' => true,);
        else:
            $result = array('success' => true,);
        endif;


        echo json_encode($result);
    }
    
    public function ajaxReturnCategorias() {
        $query = $this->CategoriasModel->getCategorias()->result();
        echo json_encode($query);
    }

    public function cadastraCategoria() {
        $this->_imagemDestacada();
        $dados = $this->FormPost;
        
        if($dados['categoria_cor'] == ''):
            $dados['categoria_cor'] = '#000';
        endif;
        
        $query = $this->CategoriasModel->cadCategoria($dados);
        return $query;
    }

    public function editaCategoria($id) {
        $this->_imagemDestacada();
        $dados = $this->FormPost;

        $query = $this->CategoriasModel->updateCategoria($id, $dados);
        return $query;
    }

    public function deletaCategoria($id) {
        $query = $this->CategoriasModel->deleteCategoria($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Categoria excluida com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    private function _criaSlug() {
        if ($this->input->post("categoria_slug") == ''):
            $titulo = $this->input->post("categoria_titulo");
            $slug = url_title(convert_accented_characters($titulo), 'dash', TRUE);
            $this->FormPost['categoria_slug'] = $slug;
        else:
            $slug = url_title(convert_accented_characters($this->input->post("categoria_slug")), 'dash', TRUE);
            $this->FormPost['categoria_slug'] = $slug;
        endif;
    }
    
    private function _imagemDestacada() {
        $img = str_replace(base_url(), '', $this->input->post("categoria_imagem_destacada"));
        $this->FormPost['categoria_imagem_destacada'] = $img;
    }

}
