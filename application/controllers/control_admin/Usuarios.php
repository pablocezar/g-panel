<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->_encriptSenha();
        $this->load->helper('text');
    }

    public function index() {
        
    }

    public function ajaxCheckUsuario($url) {
        $query = $this->UsuariosModel->getUsuarioUrl($url);
        if ($query->num_rows() > 0):
            $result = array('urlDuplicada' => true,);
        else:
            $result = array('success' => true,);
        endif;


        echo json_encode($result);
    }

    public function ajaxReturnUsuarios() {
        $query = $this->UsuariosModel->getUsuarios()->result();
        echo json_encode($query);
    }

    public function cadastraUsuario() {
        $dados = $this->FormPost;
        $dados['usuario_foto'] = 'themes/admin/assets/images/user-default.png';

        $query = $this->UsuariosModel->cadUsuario($dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Usuário cadastrado com sucesso!");
            redirect($this->input->server('HTTP_REFERER'));
        else:
            echo 'erro';
        endif;
    }

    public function editaUsuario($id) {
        $dados = $this->FormPost;

        if (isset($dados['usuario_foto'])):
            $data = $dados['usuario_foto'];

            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            $data = base64_decode($data);

            $nameImage = url_title(convert_accented_characters($dados['usuario_nome']), 'dash', TRUE);
            ;
            $image = $nameImage . '.jpg';

            file_put_contents('themes/admin/uploads' . '/' . $image, $data);
            $dados['usuario_foto'] = 'themes/admin/uploads' . '/' . $image;
        endif;

        $query = $this->UsuariosModel->updateUsuario($id, $dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Informações salvas com sucesso!");
            redirect($this->input->server('HTTP_REFERER'));
        else:
            echo 'erro';
        endif;
    }

    public function deletaUsuario($id) {
        $query = $this->UsuariosModel->deleteUsuario($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Usuario excluida com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    private function _encriptSenha() {
        if ($this->input->post('usuario_senha') == ''):
            unset($this->FormPost['usuario_senha']);
        else:
            $senha = md5($this->input->post('usuario_senha'));
            $this->FormPost['usuario_senha'] = $senha;
        endif;
    }

}
