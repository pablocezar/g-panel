<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function editaConfiguracoes() {
        if (isset($this->FormPost['config_logo'])):
            $this->FormPost['config_logo'] = str_replace(base_url(), '', $this->FormPost['config_logo']);
        endif;
        $this->_configSEO();
        $dados = $this->FormPost;
        $query = $this->ConfiguracoesModel->EditaConfig($dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Informações Salvas com Sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    private function _configSEO() {
        $dados = $this->FormPost;
        if (isset($dados['config_seo']) && $dados['config_seo'] == "Ativo"):
            $this->create_robotsTXT();
            $this->create_sitemap();
            $this->_siteMap_ping();
        endif;
    }

    private function _siteMap_ping() {
        $gzip = gzopen('sitemap.xml.gz', 'w9');
        $gmap = file_get_contents('sitemap.xml');
        gzwrite($gzip, $gmap);
        gzclose($gzip);
        $this->site_map_ping();
    }

    private function create_robotsTXT() {
        $file = "robots.txt";
        if (file_exists($file)):
            unlink($file);
        endif;

        $xml = fopen("robots.txt", "a");

        $content = "User-agent: *\r\n";
        $content .= "Disallow: /admin/\r\n";
        $content .= "Sitemap: ". base_url()."sitemap.xml";

        fwrite($xml, $content);
        fclose($xml);
    }

}
