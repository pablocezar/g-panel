<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accordions extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper("text");
        $this->load->model("AccordionsModel");
    }

    public function index() {
        
    }

    public function cad() {
        $this->getIdPaginas();
        $query = $this->AccordionsModel->cad($this->FormPost);
        if ($query):
            $redirect = $this->input->server("HTTP_REFERER");
            $this->session->set_flashdata('sucesso', "Accordion Criado com sucesso!");
            redirect($redirect);
        else:
            echo 'erro ao cadastrar';
        endif;
    }

    public function edit($id) {
        $this->getIdPaginas();
        $query = $this->AccordionsModel->edit($id, $this->FormPost);
        if ($query):
            $redirect = $this->input->server("HTTP_REFERER");
            $this->session->set_flashdata('sucesso', "Accordion Atualizado com sucesso!");
            redirect($redirect);
        else:
            echo 'erro ao atualizar';
        endif;
    }

    public function order() {
        $itens_menu = $this->input->post("item");

        $display_order = 0;
        foreach ($itens_menu as $id => $value):
            $data = ["accordion_ordem" => $display_order];
            $this->AccordionsModel->orderLines($id, $data);
            $display_order++;
        endforeach;

        echo json_encode(true);
    }

    public function del($id) {
        //Primeiro Deleta todas as linhas do accordion passado via $id.
        $this->delLinesAccId($id);
        //Depois deleta o Accordion
        $query = $this->AccordionsModel->del($id);

        if ($query):
            $redirect = $this->input->server("HTTP_REFERER");
            $this->session->set_flashdata('sucesso', "Accordion Deletado com sucesso!");
            redirect($redirect);
        else:
            echo 'erro ao atualizar';
        endif;
    }

    private function getIdPaginas() {
        if (isset($this->FormPost['accordion_paginas_id']) && is_array($this->FormPost['accordion_paginas_id'])):
            $this->FormPost['accordion_paginas_id'] = implode(',', $this->FormPost['accordion_paginas_id']);
        endif;
    }

    public function getAjaxByID($id) {
        $query = $this->AccordionsModel->get($id)->row();
        echo json_encode($query);
    }

    /**
     * <b>delLinesAccId</b>
     * Deleta todas as linhas do accordion passado via $id.
     * @param type $id
     */
    private function delLinesAccId($id) {
        $this->AccordionsModel->delLinesAccId($id);
    }

}
