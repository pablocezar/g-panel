<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Galerias extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function ajaxPathGaleria($pasta = NULL) {
        $path = "medias/uploads/{$pasta}";
        $diretorio = scandir($path);
        foreach ($diretorio as $file):
            if (!is_dir($file)):
                $img[] = base_url($path) . "/{$file}";
            endif;
        endforeach;
        echo json_encode($img);
    }

    public function criarGaleria() {
        $dados = $this->FormPost;
        unset($dados['imagem']);
        $query = $this->GaleriasModel->cadGaleria($dados);
        $this->_cadImagensGaleria($query);
        if ($query):
            $this->session->set_flashdata('sucesso', "Galeria criada com sucesso!");
            redirect(base_url('admin/galerias'));
        else:
            echo 'erro';
        endif;
    }

    public function deletarGaleria($id) {
        $this->ImagensModel->deleteImagem($id);
        $query = $this->GaleriasModel->deleteGaleria($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Galeria excluida com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    public function editarGaleria($id) {
        $dados = array("galeria_titulo" => $this->FormPost['galeria_titulo']);
        $this->_editImagensGaleria($id);

        $query = $this->GaleriasModel->updateGaleria($id, $dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Galeria atualizada com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    public function RemoveImagemGaleria($id) {
        $query = $this->ImagensModel->deleteImagem($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Imagem removida com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    /**
     * <b>cadImagensGaleria</b>
     * Cadastra as imagens no banco de com o id da galeria criada. 
     * @param type $query
     */
    private function _cadImagensGaleria($query) {
        $dados = $this->FormPost;
        $id = $this->db->insert_id($query);
        $imagens = $dados['imagem'];
        foreach ($imagens as $linha):
            $linha['imagem_id_galeria'] = $id;
            $this->ImagensModel->cadImagem($linha);
        endforeach;
    }

    private function _editImagensGaleria($id) {
        $dados = $this->FormPost;

        foreach ($dados['imagem'] as $linha):
            if (!isset($linha['imagem_id'])):
                $linha['imagem_id_galeria'] = $id;
                $newImage = $linha;
                $this->ImagensModel->cadImagem($newImage);
            endif;
        endforeach;

        if (isset($linha['imagem_id'])):
            $imagens = $dados['imagem'];
            foreach ($imagens as $linha):
                $this->ImagensModel->updateImagem($linha['imagem_id'], $linha);
            endforeach;
        endif;
    }

}
