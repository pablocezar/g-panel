<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->_formEmail();
    }

    /**
     * <b>FormatEmail</b>
     * Estiliza a Mensagem de Email que o cliente receberá o contato via formulário do site.
     * @param type $nome
     * @param type $email
     * @param type $tel
     * @param type $como_nos_achou
     * @param type $assunto
     * @param type $mensagem
     * @return string
     */
    private function FormatEmail($nome, $email, $tel, $como_nos_achou, $assunto, $mensagem) {
        $content = ""
                . "<table width='800px' style='font-family: Arial'>"
                . "<thead>"
                . "<tr>"
                . "<th colspan='3' style='border:1px solid; width: 100%; background:#1ab394; color:#fff; padding: 10px;'>Olá " . $this->ConfigSysten->config_nome_empresa . "</th>"
                . "</tr>"
                . "</thead>"
                . "<tbody>"
                . "<tr>"
                . "<td colspan='2' style='background: #fafafa; padding:10px; text-align: center'><b>{$nome}</b> entrou em contato via fomulário do site. Segue abaixo as informações:</td>"
                . "</tr>"
                . "<tr>"
                . "<td><br><b>Nome:</b>{$nome}</td>"
                . "</tr>"
                . "<tr>"
                . "<td><b>Email:</b> {$email}</td>"
                . "</tr>"
                . "<tr>"
                . "<td><b>Telefone:</b> {$tel}</td>"
                . "</tr>"
                . "<tr>"
                . "<td><b>Como achou a " . $this->ConfigSysten->config_nome_empresa . ":</b> {$como_nos_achou}</td>"
                . "</tr>"
                . "<tr>"
                . "<td><b>Assunto:</b> {$assunto}</td>"
                . "</tr>"
                . "<tr>"
                . "<td><br><b>Mensagem:</b><p>{$mensagem}</p></td>"
                . "</tr>"
                . "</tbody>"
                . "</tfooter>"
                . "<tr>"
                . "<td style='padding-top:30px'>"
                . "<small>Desenvolvido por Genesys Tech.<br>www.genesystech.com.br<br><b style='color:#1ab394'>Obrigado por ser nosso parceiro!<b></small>"
                . "</td>"
                . "</tr>"
                . "</tfooter>"
                . "</table>";
        return $content;
    }

    /**
     * <b>formEmail</b>
     * Método parra envio de emails através do um formulário via post.
     */
    public function formEmail() {
        
        $mensagem = $this->FormatEmail($this->FormPost['nome'], $this->FormPost['email'], $this->FormPost['telefone'], $this->FormPost['como_nos_achou'], $this->FormPost['assunto'], $this->FormPost['mensagem']);
        
        $ConfigSysten = $this->ConfigSysten;
        $this->configSMTP();

        $this->email->from($this->FormPost["email"], $this->FormPost["nome"]);
        $this->email->to($ConfigSysten->config_email);
        $this->email->subject($this->FormPost["assunto"]);
        $this->email->attach($this->_uploadAnexo());
        $this->email->message($mensagem);

        $this->email->send();
    }

    public function deletaEmail($id) {
        $query = $this->EmailsModel->deleteEmail($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Email excluido com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    public function getEmailJson($id) {
        $email = $this->EmailsModel->getEmailId($id)->row();
        echo json_encode($email);
    }

    public function marcaComoLido($id) {
        $dados = array("aberto" => 1);
        $email = $this->EmailsModel->editaEmail($id, $dados);
    }

    /**
     * <b>_salvaEmail</b>
     * Salva o email antes do envio, serve para disponibilizar os emails recebidos no painel.
     */
    private function _salvaEmail($dados) {
        $dados['data'] = date('Y-m-d H:i:s');
        $this->EmailsModel->cadEmail($dados);
    }

    /**
     * <b>_uploadAnexo</b>
     * realiza o upload do arquivo em anexo na mensagem de email.
     */
    private function _uploadAnexo() {
        $this->upload('emails_anexo', "anexo");
        $dados_upload = $this->upload->data();
        $anexo = $dados_upload['file_path'] . $dados_upload['file_name'];

        $dados = $this->FormPost;
        if (isset($dados['g-recaptcha-response'])):
            unset($dados['g-recaptcha-response']);
        endif;
        unset($dados['formEmail']);
        $dados['anexo'] = $dados_upload['file_name'];
        $this->_salvaEmail($dados);
        return $anexo;
    }

}
