<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Popups extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("PopupsModel");
    }

    public function index() {
        
    }

    public function cad() {
        $this->setIdPaginas();
        $this->setImage();
        $query = $this->PopupsModel->cad($this->FormPost);
        if ($query):
            $this->session->set_flashdata('sucesso', "Popup criado com sucesso!");
            redirect(base_url('admin/popups'));
        else:
            echo 'erro ao criar popup';
        endif;
    }
    
    public function update($id) {
        $this->setIdPaginas();
        $this->setImage();
        $query = $this->PopupsModel->update($id, $this->FormPost);
        
        if ($query):
            $this->session->set_flashdata('sucesso', "Popup atualizado com sucesso!");
            redirect($this->input->server("HTTP_REFERER"));
        else:
            echo 'erro ao atualizar popup';
        endif;
    }

    public function del($id) {
        $query = $this->PopupsModel->del($id);
        
        if ($query):
            $this->session->set_flashdata('sucesso', "Popup deletado com sucesso!");
            redirect(base_url('admin/popups'));
        else:
            echo 'erro ao deletar popup';
        endif;
    }

    /*     * *********************************** */
    /*     * ******** Private Methods ********* */
    /*     * ********************************* */

    /**
     * <b>setIdPaginas</b>
     */
    private function setIdPaginas() {
        $this->FormPost['popup_id_paginas'] = implode(',', $this->FormPost['popup_id_paginas']);
    }

    private function setImage() {
        $this->FormPost['popup_imagem_destacada'] = str_replace(base_url(), '', $this->FormPost['popup_imagem_destacada']);
    }

}
