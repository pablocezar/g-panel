<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RegioesAtendimento extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function cad() {
        $this->FormPost['regiao_status'] = 'ativo';
        $query = $this->RegioesAtendimentoModel->cadRegiao($this->FormPost);
        if ($query):
            $this->session->set_flashdata('sucesso', "Região Cadastrada com Sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }
    
    
    public function edit($id) {
        $query = $this->RegioesAtendimentoModel->editaRegiao($id, $this->FormPost);
        if ($query):
            $this->session->set_flashdata('sucesso', "Informações Salvas com Sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }
    
    
    public function del($id) {
        $query = $this->RegioesAtendimentoModel->deleteRegiao($id);
        if ($query):
            $this->session->set_flashdata('sucesso', "Região Excluida com Sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }
}
