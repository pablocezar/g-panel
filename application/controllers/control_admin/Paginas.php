<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Paginas extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper("text");
        $this->_criaSlug();
        $this->_imagemDestacada();
    }

    public function index() {
        
    }

    public function ajaxCheckPagina($url) {
        $query = $this->PaginasModel->getPaginaUrl($url);
        if ($query->num_rows() > 0):
            $result = array('urlDuplicada' => true,);
        else:
            $result = array('success' => true,);
        endif;


        echo json_encode($result);
    }
    
    public function ajaxGetArtigo() {
        $search = $this->FormPost['titulo'];
        $query = $this->PaginasModel->AjaxAtigosByTitle($search);
        if ($query->num_rows() > 0):
            $result = array('urlDuplicada' => true,);
        else:
            $result = array('success' => true,);
        endif;

        echo json_encode($query->result());
    }

    public function cadastraPagina() {
        if ($this->FormPost['pagina_tipo'] == 'artigo'):
            $this->_convertDataPost();
            $this->_arrayCategorias();
            $redirect = base_url('admin/artigos');
        else:
            $redirect = base_url('admin/paginas');
        endif;

        $dados = $this->FormPost;
        $dados['pagina_data_criacao'] = date('Y-m-d H:i:s');

        $query = $this->PaginasModel->cadPagina($dados);
        if ($query):
            if ($this->ConfigSysten->config_seo == "Ativo"):
                $this->create_sitemap();
                $this->_siteMap_ping();
            endif;
            $this->session->set_flashdata('sucesso', "Página Criada com sucesso!");
            redirect($redirect);
        else:
            echo 'erro ao cadastrar página';
        endif;
    }

    public function editaPagina($id) {
        if ($this->FormPost['pagina_tipo'] == 'artigo'):
            $this->_convertDataPost();
            $this->_arrayCategorias();
        endif;
        
        $dados = $this->FormPost;
        unset($dados['pagina_slug']);
        if (!isset($dados['pagina_destaque_home'])):
            $dados['pagina_destaque_home'] = 'off';
        endif;

        $query = $this->PaginasModel->updatePagina($id, $dados);
        if ($query):
            $this->session->set_flashdata('sucesso', "Informações Salvas com Sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    public function ordenaPagina() {
        $ids = $this->input->post("item");
        $display_order = 1;

        foreach ($ids as $id):
            $this->PaginasModel->ordenaPagina($id, $display_order);
            $display_order++;
        endforeach;
    }

    public function deletaPagina($id) {
        $query = $this->PaginasModel->deletePagina($id);
        if ($query):
            if ($this->ConfigSysten->config_seo == "Ativo"):
                $this->create_sitemap();
                $this->_siteMap_ping();
            endif;
            $this->session->set_flashdata('sucesso', "Página excluida com sucesso!");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            echo 'erro';
        endif;
    }

    private function _criaSlug() {
        if ($this->input->post("pagina_slug") == ''):
            $titulo = $this->input->post("titulo");
            $slug = url_title(convert_accented_characters($titulo), 'dash', TRUE);
            $this->FormPost['pagina_slug'] = $slug;
        else:
            $slug = url_title(convert_accented_characters($this->input->post("pagina_slug")), 'dash', TRUE);
            $this->FormPost['pagina_slug'] = $slug;
        endif;
    }

    private function _imagemDestacada() {
        $img = str_replace(base_url(), '', $this->input->post("pagina_imagem_destacada"));
        $this->FormPost['pagina_imagem_destacada'] = $img;
    }

    private function _convertDataPost() {
        $data = $this->input->post('pagina_postagem_programada');
        if ($data != ""):
            $repleace = str_replace('/', '-', $data);
            $convertData = date('Y-m-d H:i:00', strtotime($repleace));
            $this->FormPost['pagina_postagem_programada'] = $convertData;
            if ($this->FormPost['pagina_postagem_programada'] <= date('Y-m-d H:i:00')):
                $this->FormPost['pagina_postagem_programada'] = NULL;
            endif;
        else:
            $this->FormPost['pagina_postagem_programada'] = NULL;
        endif;
    }

    private function _arrayCategorias() {
        $id_categoria = $this->input->post('pagina_id_categoria');

        $id_categoria = str_replace('"', '', json_encode($id_categoria));
        $id_categoria = str_replace('[', '', $id_categoria);
        $id_categoria = str_replace(']', '', $id_categoria);

        $this->FormPost['pagina_id_categoria'] = $id_categoria;
    }

    private function _siteMap_ping() {
        $gzip = gzopen('sitemap.xml.gz', 'w9');
        $gmap = file_get_contents('sitemap.xml');
        gzwrite($gzip, $gmap);
        gzclose($gzip);
        $this->site_map_ping();
    }

}
