<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Menus extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
    }

    public function cad() {
        $link_inerno = str_replace(base_url(), '', $this->input->post("menu_link"));
        $base_url = base_url();
        $this->FormPost['menu_link'] = (preg_quote("/$base_url/", $this->input->post("menu_link"))) ? $link_inerno : $this->FormPost['menu_link'];
        $query = $this->MenusModel->cadMenu($this->FormPost);
        if ($query):
            $this->session->set_flashdata('sucesso', "Link adicionado com sucesso");
            redirect($_SERVER['HTTP_REFERER']);
        else:
            $this->session->set_flashdata('erro', "Erro ao adicionar link");
            redirect($_SERVER['HTTP_REFERER']);
        endif;
    }

    public function ordenaMenu() {
        $itens_menu = $this->input->post("item");

        $display_order = 0;
        foreach ($itens_menu as $id => $value):
            if ($value != NULL):
                $data = ["menu_link_pai" => $value, "menu_ordem" => $display_order];
                $this->MenusModel->ordenaMenu($id, $data);
            else:
                $data = ["menu_link_pai" => $value, "menu_ordem" => $display_order];
                $this->MenusModel->ordenaMenu($id, $data);
            endif;
            $display_order++;
        endforeach;

        echo json_encode(true);
    }
    
    public function removeItemMenu($id) {
        $this->MenusModel->removeItemMenu($id);
        echo json_encode(true);
    }

    /**
     * <b>formEmail</b>
     * Método parra envio de emails através do um formulário via post.
     */
}
