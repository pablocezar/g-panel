<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_database extends CI_Migration {

    public function up() {
        $this->CreateAllTables();
        $this->setDefault();
    }

    public function down() {
        $this->dbforge->drop_table('usuarios');
        $this->dbforge->drop_table('paginas');
    }
    
    private function CreateAllTables() {
        $this->createUsuarios();
        $this->createPaginas();
        $this->createBanners();
        $this->createCategorias();
        $this->createConfiguracoes();
        $this->createEmails();
        $this->createGalerias();
        $this->createImgens();
        $this->createLeads();
        $this->createMenusSite();
        $this->createRegioesAtendimento();
        $this->createSiteViewsOnline();
    }

    /**
     * <b>setDefault</b>
     * Seta valores padrão das configurações para inicio de cms, também é criado o usuário inicial.
     * @return BOLEAN
     */
    private function setDefault() {
        $usuarios = array(
            "usuario_nome" => "Genesys Tech User",
            "usuario_email" => "contato@genesystech.com.br",
            "usuario_foto" => "themes/admin/assets/images/user-default.png",
            "usuario_nivel_acesso" => 3,
            "usuario_senha" => md5('123456')
        );
        
        $config = array(
            "config_tema_site" => 'default',
            "config_tema_admin" => 'admin',
            "config_num_pag_paginacao" => 5,
            "config_seo" => "Inativo",
            "config_manutencao" => 1
        );
        
        $this->db->where("config_id", 1)->insert("configuracoes", $config);
        $this->db->insert("usuarios", $usuarios);
    }

    private function createUsuarios() {
        $this->dbforge->add_field(array(
            'usuario_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'usuario_nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'usuario_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'usuario_foto' => array(
                'type' => 'LONGTEXT',
            ),
            'usuario_nivel_acesso' => array(
                'type' => 'INT',
                'constraint' => '3',
            ),
            'usuario_tipo_menu' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
            ),
            'usuario_senha' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'usuario_data_cadastro' => array(
                'type' => 'timestamp',
            ),
        ));
        $this->dbforge->add_key('usuario_id', TRUE);
        $this->dbforge->create_table('usuarios');
    }

    private function createPaginas() {
        $this->dbforge->add_field(array(
            'pagina_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'pagina_tipo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'pagina_postagem_programada' => array(
                'type' => 'DATETIME',
                'NULL' => TRUE,
            ),
            'pagina_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'pagina_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '300',
            ),
            'pagina_destaque_home' => array(
                'type' => 'VARCHAR',
                'constraint' => '4',
            ),
            'pagina_imagem_destacada' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'pagina_descricao' => array(
                'type' => 'VARCHAR',
                'constraint' => '230',
            ),
            'pagina_conteudo' => array(
                'type' => 'LONGTEXT',
            ),
            'pagina_id_galeria' => array(
                'type' => 'int',
                'constraint' => '3',
            ),
            'pagina_id_categoria' => array(
                'type' => 'varbinary',
                'constraint' => '255',
            ),
            'pagina_data_criacao' => array(
                'type' => 'timestamp',
            ),
            'pagina_data_alteracao' => array(
                'type' => 'datetime',
            ),
        ));
        $this->dbforge->add_key('pagina_id', TRUE);
        $this->dbforge->create_table('paginas');
    }

    private function createBanners() {
        $this->dbforge->add_field(array(
            'banner_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'banner_imagem' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'banner_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'banner_link' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'banner_descricao' => array(
                'type' => 'LONGTEXT',
            ),
            'banner_ordem' => array(
                'type' => 'INT',
                'constraint' => '5',
            ),
            'banner_data_cadastro' => array(
                'type' => 'TIMESTAMP',
            ),
        ));
        
        $this->dbforge->add_key('banner_id', TRUE);
        $this->dbforge->create_table('banners');
    }
    
    private function createCategorias() {
        $this->dbforge->add_field(array(
            'categoria_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'categoria_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'categoria_imagem_destacada' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'categoria_descricao' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'categoria_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'categoria_cor' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'categoria_destaque' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'categoria_data_criacao' => array(
                'type' => 'TIMESTAMP',
            ),
        ));
        
        $this->dbforge->add_key('categoria_id', TRUE);
        $this->dbforge->create_table('categorias');
    }
    
    private function createConfiguracoes() {
        $this->dbforge->add_field(array(
            'config_id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'config_logo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_nome_empresa' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_telefone' => array(
                'type' => 'VARCHAR',
                'constraint' => '18',
            ),
            'config_telefone_2' => array(
                'type' => 'VARCHAR',
                'constraint' => '18',
            ),
            'config_telefone_3' => array(
                'type' => 'VARCHAR',
                'constraint' => '18',
            ),
            'config_cep' => array(
                'type' => 'VARCHAR',
                'constraint' => '15',
            ),
            'config_logradouro' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_numero' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
            ),
            'config_localidade' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_uf' => array(
                'type' => 'VARCHAR',
                'constraint' => '3',
            ),
            'config_complemento' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'config_email_2' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'config_horario_atendimento' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_facebook' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_twitter' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_instagram' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_youtube' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_google_plus' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_linkedin' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_tema_admin' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_tema_site' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_pag_lista_artigos' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_manutencao' => array(
                'type' => 'VARCHAR',
                'constraint' => '5',
            ),
            'config_descricao' => array(
                'type' => 'VARCHAR',
                'constraint' => '160',
            ),
            'config_smtp_host' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_smtp_porta' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_smtp_usuario' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_smtp_senha' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_cod_analytics' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_report_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_client_email_report' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_chave_analytics' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_cod_mapa_google' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE,
            ),
            'config_seo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'config_num_pag_paginacao' => array(
                'type' => 'INT',
                'constraint' => '8',
            ),
        ));
        
        $this->dbforge->add_key('config_id', TRUE);
        $this->dbforge->create_table('configuracoes');
    }
    
    private function createEmails() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'telefone' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'como_nos_achou' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'assunto' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'mensagem' => array(
                'type' => 'LONGTEXT',
            ),
            'anexo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'aberto' => array(
                'type' => 'INT',
                'constraint' => '1',
            ),
            'data' => array(
                'type' => 'DATETIME'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('emails');
    }

    private function createGalerias() {
        $this->dbforge->add_field(array(
            'galeria_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'galeria_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'galeria_data_criacao' => array(
                'type' => 'timestamp',
            ),
        ));
        $this->dbforge->add_key('galeria_id', TRUE);
        $this->dbforge->create_table('galerias');
    }
    
    private function createImgens() {
        $this->dbforge->add_field(array(
            'imagem_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'imagem_id_galeria' => array(
                'type' => 'INT',
                'constraint' => 80,
            ),
            'imagem_imagem' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'imagem_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'imagem_link' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'imagem_descricao' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'imagem_data_criacao' => array(
                'type' => 'timestamp',
            ),
        ));
        $this->dbforge->add_key('imagem_id', TRUE);
        $this->dbforge->create_table('imagens');
    }
    
    private function createLeads() {
        $this->dbforge->add_field(array(
            'lead_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'lead_nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'lead_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'lead_data_cadastro' => array(
                'type' => 'timestamp',
            ),
        ));
        $this->dbforge->add_key('lead_id', TRUE);
        $this->dbforge->create_table('leads');
    }
    
    private function createMenusSite() {
        $this->dbforge->add_field(array(
            'menu_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'menu_nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'menu_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'menu_link' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'menu_link_pai' => array(
                'type' => 'INT',
                'constraint' => '80',
            ),
            'menu_target_blank' => array(
                'type' => 'VARCHAR',
                'constraint' => '4',
            ),
            'menu_ordem' => array(
                'type' => 'INT',
                'constraint' => '80',
            ),
            'menu_icone' => array(
                'type' => 'VARCHAR',
                'constraint' => '80',
            ),
            'menu_data_cadastro' => array(
                'type' => 'timestamp',
            ),
        ));
        $this->dbforge->add_key('menu_id', TRUE);
        $this->dbforge->create_table('menus_site');
    }
    
    private function createRegioesAtendimento() {
        $this->dbforge->add_field(array(
            'regiao_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'regiao_nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'regiao_localidades' => array(
                'type' => 'LONGTEXT',
            ),
            'regiao_status' => array(
                'type' => 'VARCHAR',
                'constraint' => '7',
            ),
            'regiao_data_cadastro' => array(
                'type' => 'timestamp',
            ),
        ));
        $this->dbforge->add_key('regiao_id', TRUE);
        $this->dbforge->create_table('regioes_atendimento');
    }
    
    private function createSiteViewsOnline() {
        $this->dbforge->add_field(array(
            'online_id' => array(
                'type' => 'INT',
                'constraint' => 80,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'online_session' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'online_startview' => array(
                'type' => 'TIMESTAMP',
            ),
            'online_endview' => array(
                'type' => 'TIMESTAMP',
            ),
            'online_ip' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'online_url' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'online_agent' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
            'agent_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
        ));
        $this->dbforge->add_key('online_id', TRUE);
        $this->dbforge->create_table('siteviews_online');
    }
}
