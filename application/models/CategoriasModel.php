<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class CategoriasModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "categorias";
    }

    public function getCategorias($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->order_by("categoria_titulo", "ASC");
        return $this->db->get($this->Table);
    }
    
    public function getCategoriaId($id) {
        $query = $this->db->where('categoria_id', $id)->get($this->Table);
        return $query;
    }

    public function getCategoriaUrl($url) {
        $query = $this->db->where('categoria_slug', $url)->get($this->Table);
        return $query;
    }
    
    public function getCategoriaDestacadas() {
        $query = $this->db->where('categoria_destaque', "Sim")->get($this->Table);
        return $query;
    }
    
    public function cadCategoria($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function updateCategoria($id, $dados) {
        $query = $this->db->where('categoria_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    public function deleteCategoria($id) {
        $query = $this->db->where('categoria_id', $id)->delete($this->Table);
        return $query;
    }
    
    
}
