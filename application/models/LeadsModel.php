<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [Model]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class LeadsModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "leads";
    }

    public function getLeads($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->order_by("lead_data_cadastro", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getNovosLeads($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->where('lead_aberto', 0);
        $this->db->order_by("lead_data", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getLeadId($id) {
        $query = $this->db->where('lead_id', $id)->get($this->Table);
        return $query;
    }

    public function cadLead($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function deleteLead($id) {
        $query = $this->db->where('lead_id', $id)->delete($this->Table);
        return $query;
    }
    
    public function editaLead($id, $dados) {
        $query = $this->db->where('lead_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    
}
