<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class BannersModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "banners";
    }

    public function getBanners() {
        $this->db->order_by("banner_data_cadastro", "DESC");
        return $this->db->get($this->Table);
    }
    
    
    public function getBannerId($id) {
        $query = $this->db->where('banner_id', $id)->get($this->Table);
        return $query;
    }

    public function cadBanner($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function deleteBanner($id) {
        $query = $this->db->where('banner_id', $id)->delete($this->Table);
        return $query;
    }
    
    public function editaBanner($id, $dados) {
        $query = $this->db->where('banner_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    
}
