<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Galerias_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar Genesys tech
 */
class GaleriasModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "galerias";
    }

    public function getGalerias($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->order_by("galeria_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getGaleriaId($id) {
        $query = $this->db->where('galeria_id', $id)->get($this->Table);
        return $query;
    }

    public function getGaleriaUrl($url) {
        $query = $this->db->where('galeria_slug', $url)->get($this->Table);
        return $query;
    }
    
    public function cadGaleria($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function updateGaleria($id, $dados) {
        $query = $this->db->where('galeria_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    public function deleteGaleria($id) {
        $query = $this->db->where('galeria_id', $id)->delete($this->Table);
        return $query;
    }
    
    
}
