<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class ConfiguracoesModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "configuracoes";
    }

    public function getConfig() {
        return $this->db->get($this->Table);
    }
    
    public function EditaConfig($dados) {
        return $this->db->update($this->Table, $dados);
    }
    
}
