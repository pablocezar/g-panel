<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MenusModel [Model]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class MenusModel extends CI_Model {

    private $Table;

    public function __construct() {
        parent::__construct();
        $this->Table = "menus_site";
    }

    public function cadMenu($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }

    public function getMenuPrincipal() {
        $this->Truncate();
        $this->db->where("menu_nome", 'principal');
        $this->db->where("menu_link_pai", 0);
        $this->db->order_by("menu_ordem", "ASC");
        return $this->db->get($this->Table);
    }
    
    public function getMenuRodape() {
//        $this->Truncate();
        $this->db->where("menu_nome", 'rodapé');
        $this->db->where("menu_link_pai", 0);
        $this->db->order_by("menu_ordem", "ASC");
        return $this->db->get($this->Table);
    }

    function submenu($id) {
        $query = $this->db->where("menu_link_pai", $id)->order_by("menu_ordem", "ASC")->get($this->Table);
        return $query;
    }

    public function ordenaMenu($id, $data) {
        $this->db->where("menu_id", $id);
        $this->db->update($this->Table, $data);
    }

    public function removeItemMenu($id) {
        $query = $this->db->where('menu_id', $id)->delete($this->Table);
        return $query;
    }

    public function Truncate() {
        $this->db->where("menu_nome", 'principal');
        $this->db->where("menu_link_pai", 0);
        $this->db->order_by("menu_ordem", "ASC");
        $result = $this->db->get($this->Table)->result();
        if (count($result) == 0):
            $this->db->truncate($this->Table);
        endif;
    }

}
