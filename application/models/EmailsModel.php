<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class EmailsModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "emails";
    }

    public function getEmails() {
        $this->db->order_by("data", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getNovosEmails() {
        $this->db->where('aberto', 0);
        $this->db->order_by("data", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getEmailId($id) {
        $query = $this->db->where('id', $id)->get($this->Table);
        return $query;
    }

    public function cadEmail($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function deleteEmail($id) {
        $query = $this->db->where('id', $id)->delete($this->Table);
        return $query;
    }
    
    public function editaEmail($id, $dados) {
        $query = $this->db->where('id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    
}
