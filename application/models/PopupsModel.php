<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [Model]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class PopupsModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "popups";
        $this->checkDbExists();
    }
    
    public function cad($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function getAll() {
        $query = $this->db->order_by('popup_id', "ASC")->get($this->Table);
        return $query;
    }
    
    public function get($id) {
        $query = $this->db->where('popup_id', $id)->get($this->Table);
        return $query;
    }
    
    public function getByPagina($pagina_id) {
        $this->db->where("popup_status !=", 0);
        $query = $this->db->where("FIND_IN_SET($pagina_id, popup_id_paginas) !=", 0)->get($this->Table);
        return $query;
    }
    
    public function update($id, $dados) {
        $query = $this->db->where('popup_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    public function del($id) {
        $query = $this->db->where('popup_id', $id)->delete($this->Table);
        return $query;
    }

    
    
    
    /**************************************/
    /********** Private Methods **********/
    /************************************/
    
    /**
     * <b>checkTableExists</b>
     * Vefica se a tabela existe, caso não exista então a cria.
     */
    public function checkDbExists() {
        if (!$this->db->table_exists($this->Table)):
            $this->load->dbforge();
            $this->dbforge->add_field(array(
                'popup_id' => array(
                    'type' => 'INT',
                    'constraint' => 80,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'popup_id_paginas' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'default' => NULL,
                ),
                'popup_imagem_destacada' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'default' => NULL,
                ),
                'popup_titulo' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'default' => NULL,
                ),
                'popup_conteudo' => array(
                    'type' => 'LONGTEXT',
                    'default' => NULL,
                ),
                'popup_link' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'default' => NULL,
                ),
                'popup_status' => array(
                    'type' => 'INT',
                    'constraint' => 80,
                    'unsigned' => TRUE,
                ),
            ));
            $this->dbforge->add_key('popup_id', TRUE);
            $this->dbforge->create_table($this->Table);
        endif;
        
    }
    
}
