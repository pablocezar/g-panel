<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * SiteViewsOnlineModel [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class SiteViewsOnlineModel extends CI_Model {

    private $Table;

    public function __construct() {
        parent::__construct();
        $this->Table = "gns_siteviews_online";
    }

    public function getUsersOnline($date) {
        $this->delUserOnline();
        $query = $this->db->get($this->Table);
        return $query;
    }

    public function cadUserOnline($dados) {
        $this->delUserOnline();
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }

    public function updateUserOnline($dados, $session) {
        $query = $this->db->where('online_session', $session)->update($this->Table, $dados);
        if ($query):
            return $this->db->affected_rows();
        else:
            return $this->db->affected_rows();
        endif;
    }

    public function delUserOnline() {
        $expire = date('Y-m-d H:i:s', strtotime('-5 minutes'));

        $query = $this->db->where("online_endview <", $expire)->delete($this->Table);
        if ($query):
            return true;
        else:
            return false;
        endif;
    }

}
