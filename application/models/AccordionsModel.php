<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [Model]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class AccordionsModel extends CI_Model {

    private $Table;

    public function __construct() {
        parent::__construct();
        $this->checkTableExists();
        $this->Table = "accordions";
    }

    public function getAll() {
        $this->db->where('accordion_id_pai', NULL);
        $this->db->order_by("accordion_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }

    public function get($id) {
        $query = $this->db->where('accordion_id', $id)->get($this->Table);
        return $query;
    }

    public function getByPageId($id) {
        $this->db->select('accordion_id');
        $this->db->where('accordion_status', 1);
        $AccId = $this->db->where('accordion_paginas_id', $id)->get($this->Table)->result();
        if (count($AccId) == 1):
            $query = $this->getLines($AccId[0]->accordion_id)->result();
            return $query;
        elseif (count($AccId) == 0 || count($AccId) > 1):
            return [];
        else:
            $query = $this->getLines($id)->result();
            return $query;
        endif;
    }

    public function getLines($id) {
        $this->db->order_by('accordion_ordem', 'ASC');
        $query = $this->db->where('accordion_id_pai', $id)->get($this->Table);
        return $query;
    }

    public function orderLines($id, $data) {
        $this->db->where("accordion_id", $id);
        $query = $this->db->update($this->Table, $data);
        return $query;
    }

    public function cad($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }

    public function del($id) {
        $query = $this->db->where('accordion_id', $id)->delete($this->Table);
        return $query;
    }

    public function delLinesAccId($id) {
        $query = $this->db->where('accordion_id_pai', $id)->delete($this->Table);
        return $query;
    }

    public function edit($id, $dados) {
        $query = $this->db->where('accordion_id', $id)->update($this->Table, $dados);
        return $query;
    }

    /**
     * <b>checkTableExists</b>
     * Verifica se existe a tabela accordions caso não exista é criada para utilização!
     */
    private function checkTableExists() {
        if (!$this->db->table_exists('accordions')):
            $this->load->dbforge();
            $this->dbforge->add_field(array(
                'accordion_id' => array(
                    'type' => 'INT',
                    'constraint' => 80,
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ),
                'accordion_id_pai' => array(
                    'type' => 'INT',
                    'constraint' => 80,
                    'default' => NULL,
                ),
                'accordion_titulo' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'default' => NULL,
                ),
                'accordion_conteudo' => array(
                    'type' => 'LONGTEXT',
                    'default' => NULL,
                ),
                'accordion_paginas_id' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'default' => NULL,
                ),
                'accordion_status' => array(
                    'type' => 'INT',
                    'constraint' => 1,
                    'default' => NULL,
                ),
                'accordion_ordem' => array(
                    'type' => 'INT',
                    'constraint' => 80,
                    'default' => NULL,
                ),
                'accordion_data_alteracao' => array(
                    'type' => 'datetime',
                ),
                'accordion_data_criacao' => array(
                    'type' => 'timestamp'
                ),
            ));
            $this->dbforge->add_key('accordion_id', TRUE);
            $this->dbforge->create_table('accordions');
        endif;
    }

}
