<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class UsuariosModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "usuarios";
    }
    
    function logar($dados) {
        return $this->db->get_where($this->Table, $dados);
    }

    public function getUsuarios($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->order_by("usuario_data_cadastro", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getUsuarioId($id) {
        $query = $this->db->where('usuario_id', $id)->get($this->Table);
        return $query;
    }

    public function getUsuarioUrl($url) {
        $query = $this->db->where('slug', $url)->get($this->Table);
        return $query;
    }
    
    public function cadUsuario($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function updateUsuario($id, $dados) {
        $query = $this->db->where('usuario_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    public function deleteUsuario($id) {
        $query = $this->db->where('usuario_id', $id)->delete($this->Table);
        return $query;
    }
    
    
}
