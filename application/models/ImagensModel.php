<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Imagens_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar Genesys tech
 */
class ImagensModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "imagens";
    }

    public function getImagens($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->order_by("imagem_id", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getImagemByGaleria($id_galeria) {
        $query = $this->db->where('imagem_id_galeria', $id_galeria)->get($this->Table);
        return $query;
    }
    
     public function getImagemById($id) {
        $query = $this->db->where('imagem_id', $id)->get($this->Table);
        return $query;
    }

    public function getImagemUrl($url) {
        $query = $this->db->where('pagina_slug', $url)->get($this->Table);
        return $query;
    }
    
    public function cadImagem($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function updateImagem($id, $dados) {
        unset($dados['imagem_id']);
        $query = $this->db->where('imagem_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    public function deleteImagem($id) {
        $query = $this->db->where('imagem_id', $id)->delete($this->Table);
        return $query;
    }
    
    
}
