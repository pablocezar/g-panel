<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Configuracoes_model [TIPO]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class PaginasModel extends CI_Model {

    private $Table;

    public function __construct() {
        parent::__construct();
        $this->Table = "paginas";
    }
    
    public function getAllPages(){
        return $this->db->get($this->Table);
    }

    public function getPaginas($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->where('pagina_tipo', "pagina");
        $this->db->order_by("pagina_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function search($dados) {
        $this->db->like('pagina_titulo', $dados);
        return $this->db->get($this->Table);
    }

    public function getPaginasDestaque($tipo = 'pagina') {
//        $this->db->order_by("ordem_menu", "ASC");
        $this->db->where('pagina_tipo', $tipo);
        $this->db->where('pagina_destaque_home', 'on');
        return $this->db->get($this->Table);
    }
    
    public function getArtigos($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->or_where('pagina_postagem_programada', NULL);
        $this->db->where('pagina_tipo', "artigo");
        $this->db->order_by("pagina_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getUltimosArtigos() {
        $this->db->limit(12);
        $this->db->or_where('pagina_postagem_programada', NULL);
        $where = "FIND_IN_SET('2', pagina_id_categoria)";  
        $this->db->where($where);
        $this->db->where('pagina_tipo', "artigo");
        $this->db->order_by("pagina_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getArtigosDestaque($tipo = 'artigo') {
        $this->db->where('pagina_tipo', $tipo);
        $this->db->where('pagina_destaque_home', 'on');
        $this->db->order_by("pagina_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getArtigosAgendados() {
        $this->db->or_where('pagina_postagem_programada !=', NULL);
        $this->db->where('pagina_tipo', "artigo");
        $this->db->order_by("pagina_postagem_programada", "ASC");
        return $this->db->get($this->Table);
    }
    
    public function getProxPostagens($tipo = 'artigo') {
        $this->db->where('pagina_tipo', $tipo);
        $this->db->where('pagina_postagem_programada >=', date('Y-m-d 03:00:00'));
        $this->db->where('pagina_postagem_programada <=', date('Y-m-d 03:00:00', strtotime('+3 days')));
        $this->db->order_by("pagina_data_criacao", "DESC");
        return $this->db->get($this->Table);
    }

    public function getPaginaId($id) {
        $query = $this->db->where('pagina_id', $id)->get($this->Table);
        return $query;
    }

    public function getPaginaUrl($url) {
        $this->join_categorias();
        $query = $this->db->where("gns_paginas.pagina_slug", $url)->get($this->Table);
        return $query;
    }
    
    public function AjaxAtigosByTitle($search) {
        $this->db->where("pagina_tipo", "artigo");
        $query = $this->db->like("pagina_titulo", $search)->get($this->Table);
        return $query;
    }
    
    public function getPaginaCategoria($categoria, $quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) {
            $this->db->limit($quantidade, $inicio);
        }
        $where = "FIND_IN_SET('".$categoria."', pagina_id_categoria) AND pagina_postagem_programada = " . NULL;  
        $query = $this->db->order_by("paginas.pagina_data_criacao", "DESC")->where($where)->get($this->Table);
        return $query;
    }
    
    public function getPaginaCategoriaId($id_categoria) {
        $where = "FIND_IN_SET('".$id_categoria."', pagina_id_categoria)";  
        $this->db->where($where);
        $query = $this->db->get($this->Table);
        return $query;
    }

    public function cadPagina($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }

    public function updatePagina($id, $dados) {
        $query = $this->db->where('pagina_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    function ordenaPagina($id, $display_order) {
        $this->db->where("pagina_id", $id);
        $this->db->set("pagina_ordem_menu", $display_order);
        $this->db->update($this->Table);
    }
    
    public function retornaPaginaMae($id) {
        $pagina_filho = $this->db->where("pagina_id", $id)->get($this->Table)->row();
        $pagina_mae = $pagina_filho->id_pagina_mae;
        $query = $this->db->where("pagina_id", $pagina_mae)->get($this->Table);
        return $query;
    }

    public function deletePagina($id) {
        $query = $this->db->where('pagina_id', $id)->delete($this->Table);
        return $query;
    }

    /*     * ****************************************************** */
    /*     * ****************** Private Methods ****************** */
    /*     * **************************************************** */
    private function join_categorias() {
        return $this->db->join('categorias', 'categorias.categoria_id = paginas.pagina_id_categoria', 'left');
    }
    
}
