<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * RegioesAtendimentoModel [Model]
 * Descricao
 * @copyright (c) year, Pablo Cezar PBL Desenvolvimento Web
 */
class RegioesAtendimentoModel extends CI_Model{
    
    private $Table;
    
    public function __construct() {
        parent::__construct();
        $this->Table = "regioes_atendimento";
    }

    public function getRegioes($quantidade = 0, $inicio = 0) {
        if ($quantidade > 0) :
            $this->db->limit($quantidade, $inicio);
        endif;
        $this->db->order_by("regiao_data_cadastro", "DESC");
        return $this->db->get($this->Table);
    }
    
    public function getRegiaoId($id) {
        $query = $this->db->where('regiao_id', $id)->get($this->Table);
        return $query;
    }

    public function cadRegiao($dados) {
        $query = $this->db->insert($this->Table, $dados);
        return $query;
    }
    
    public function deleteRegiao($id) {
        $query = $this->db->where('regiao_id', $id)->delete($this->Table);
        return $query;
    }
    
    public function editaRegiao($id, $dados) {
        $query = $this->db->where('regiao_id', $id)->update($this->Table, $dados);
        return $query;
    }
    
    
}
