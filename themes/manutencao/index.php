<!DOCTYPE html>

<html lang="pt-br" prefix="og: http://ogp.me/ns/article#"> 

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Manutenção</title>

        <?= $metasSeo ?>



        <!-- Bootstrap -->

        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

        <link href="<?= $theme ?>/style.css" rel="stylesheet">



        <!-- Font Awesome -->

        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">



        <!-- Favicon -->

        <link rel="shortcut icon" href="<?= $theme ?>/images/favicon.png"/>



        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

        <!--[if lt IE 9]>

          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <![endif]-->

    </head>

    <body>

	

        <div class="bg">

			<div class="all">

				<i class="fa fa-gears pulse"></i>

				<h1>Site em manutenção...</h1>

                                <p>O site encontra-se em manutenção, por favor retorne mais tarde!</p>

                                <h3><i class="fa fa-envelope"></i> <?= $config->config_email ?></h3>

                                <h3><i class="fa fa-phone"></i> <?= $config->config_telefone ?></h3>

                                <p><img width="150px" src="<?= $theme.'/images/logo-genesys.png' ?>" alt="Genesys Tech" title="Genesys Tech"/></p>
                                <p class="ass-rodape"><small>Por: <a href="https://www.genesystech.com.br" target="_blank" title="Criação e Otimização de Sites">Genesys Tech</a></small></p>

			</div>

        </div>





        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="<?= $theme ?>/script.js"></script>

    </body>

</html>