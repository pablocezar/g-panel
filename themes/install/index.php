<!doctype html>
<html lang="pt-br">

<head>
    <title>G Panel Setup</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <style>
        h1 {
            color: #555;
            font-size: 1.3em;
            text-align: center;
            padding: 20px 0;
        }

        a {
            color: #fff;
        }

        a:hover {
            color: #fff;
        }

        button {
            cursor: pointer
        }

        .bg_left {
            position: fixed;
            width: 50%;
            height: 100%;
            left: 0;
            background: #fafafa;
            padding-top: 50px;
        }

        .bg_right {
            position: fixed;
            width: 50%;
            height: 100%;
            right: 0;
            background-color: #18A689;
            background-image: url(themes/install/bg_rigth.png);
            background-repeat: no-repeat;
            background-position: left center;
            background-size: cover;
            padding-top: 50px;
            color: #fff
        }

        .bg_right h1 {
            color: #fff;
        }

        .logo {
            text-align: center
        }

        .bottom {
            position: absolute;
            bottom: 0;
            left: 20%;
            right: 20%;
        }

        .success {
            text-align: center;
            color: #666666;
            display: none
        }

        @media(max-width: 768px) {
            .bg_left {
                width: 100%;
            }

            .bg_right {
                display: none;
            }
        }
    </style>
</head>

<body>
    <div class="bg_left">
        <div class="container-fluid">
            <div class="container">
                <div class="logo">
                    <img src="<?= base_url('themes/') ?>admin/assets/images/logo-g-panel.png" title="G Panel" alt="G Panel" />
                </div>
                <h1>Bem vindo a tela de instalação do painel administrativo G Panel!</h1>
                <p>Após editado o arquivos arquivos de configurações e banco de dados, escolha o e-mail e senha e clique no botão de Setup!</p>
                <form action="#" id="form_install">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" class="form-control" id="nome" placeholder="nome" autofocus required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email" placeholder="name@example.com" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="senha">Senha</label>
                        <input type="password" name="senha" class="form-control" id="senha" placeholder="senha" required>
                    </div>
                    <div class="form-group">
                        <label for="confirme_senha">Confirme a Senha</label>
                        <input type="password" name="confirme_senha" class="form-control" id="confirme_senha" placeholder="confirme a senha" required>
                    </div>
                    <button type="submit" class="btn btn-info btn-block" id="btn_setup"><i class="fa fa-database"></i> Setup</button>
                </form>
                <p class='success'><b>Por favor pressione f5...</b></p>
            </div>
        </div>
    </div>
    <div class="bg_right">
        <div class="container-fluid">
            <div class="container">
                <h1 class="empresa">Genesys Tech</h1>
                <p class="text-center bottom">
                    <b>Por:</b> Pablo Cezar<br>
                    <a href="https://www.genesystech.com.br" title="Genesys Tech" target="_blank">www.genesystech.com.br</a>
                </p>
            </div>
        </div>
    </div>
    <script>
        var base_url = '<?= base_url() ?>';
    </script>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="<?= base_url('themes/admin/') ?>ajax.js"></script>

    <script>
        $(function() {
            $("#form_install").submit(function() {

                if ($('#confirme_senha').val() !== $('#senha').val()) {
                    alert('A senha não confere!');
                    return false;
                }

                var data = $(this).serialize();
                $.ajax({
                    url: "install/up",
                    type: 'Post',
                    dataType: 'HTML',
                    data: data,
                    beforeSend: function() {
                        $("#btn_setup").removeClass('btn-info').addClass('btn-success');
                        $("#btn_setup").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Instalando...");
                    },
                    success: function(res) {
                        $("#respostaSetup").html(res);
                        $(".success").fadeIn();
                        $("#btn_setup").attr('disabled', 'disabled');
                        $("#btn_setup").html("<i class='fa fa-check'></i> " + 'Instalação realizada com sucesso!');
                    },
                    error: function() {
                        alert("erro");
                    }
                });

                return false;
                $.ajax({
                    url: "install/up",
                    type: 'GET',
                    beforeSend: function() {
                        $("#btn_setup").removeClass('btn-info').addClass('btn-success');
                        $("#btn_setup").html("<i class='fa fa-refresh fa-spin fa-fw'></i> Instalando...");
                    },
                    success: function(res) {
                        $("#respostaSetup").html(res);
                        $(".success").fadeIn();
                        $("#btn_setup").attr('disabled', 'disabled');
                        $("#btn_setup").html("<i class='fa fa-check'></i> Instalação realizada com sucesso!");
                    },
                    error: function() {}
                });
                return false;
            });
        });
    </script>

</body>

</html>