<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>
<!-- TinyMCE -->
<script src="<?= $this->PathTheme ?>plugins/tinymce/tinymce.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-tynimce.js"></script>

<!-- nestedSortable -->
<script src="<?= $this->PathTheme ?>plugins/nestedSortable/jquery.mjs.nestedSortable.js"></script>

<script src="<?= $this->PathTheme ?>js/accordion.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-align-justify"></i> <span id="active_menu">Accordions</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Accordions</strong></li>
        </ol>
    </div>
    <div class="col-lg-5">
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url('admin/formAccordion') ?>" class="btn btn-primary pull-right" title="Criar Novo Accordion"><i class="fa fa-pencil-square-o"></i> Novo Accordion</a>
        <a href="<?= base_url('admin/accordions') ?>" class="btn btn-info pull-right" title="Listar Accordions" style="margin-right: 5px"><i class="fa fa-list"></i> Listar Accordions</a>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Novo Accordion</h5>
            </div>
            <div class="ibox-content">
                <form class="row" id="form_pagina" action="<?= (!isset($accordion)) ? base_url('control_admin/accordions/cad') : base_url('control_admin/accordions/edit') . '/' . $accordion->accordion_id ?>" method="post">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Título</label>
                            <input type="text" value="<?= isset($accordion) ? $accordion->accordion_titulo : '' ?>" id="accordion_titulo" name="accordion_titulo" class="form-control j_accordion_titulo" placeholder="Titulo do Accordion" required/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Exibir em:</label>
                            <?php
                            if (isset($accordion)):
                                $arrAccPag = explode(',', $accordion->accordion_paginas_id);
                            endif;
                            ?>
                            <select name="accordion_paginas_id[]" class="select2" required>
                                <option value="0" <?= (isset($accordion) && in_array(0, $arrAccPag) ? 'selected' : '') ?>>Home Page</option>
                                <?php foreach ($todas_paginas as $linha): ?>
                                    <option value="<?= $linha->pagina_id ?>" <?= (isset($accordion) && in_array($linha->pagina_id, $arrAccPag) ? 'selected' : '') ?>><?= $linha->pagina_titulo ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Ativo/Inativo:</label>
                            <select name="accordion_status" class="form-control" required>
                                <option value="1" <?= (isset($accordion) && $accordion->accordion_status == 1) ? 'selected' : '' ?>>Ativo</option>
                                <option value="0" <?= (isset($accordion) && $accordion->accordion_status == 0) ? 'selected' : '' ?>>Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label style="color: #ffff">.</label>
                        <div class="form-group">
                            <?php if (!isset($accordion)): ?>
                                <input type="hidden" name="accordion_data_criacao" value="<?= date('Y-m-d H:i:s') ?>" required/>
                                <button id="btnCheck" type="submit" class="btn btn-primary">Salvar</button>
                            <?php else: ?>
                                <button id="btn_edit_pagina" type="submit" class="btn btn-primary">Atualizar</button>
                            <?php endif ?>
                        </div>
                    </div>
                </form>



                <div class="row">
                    <div class="col-md-8">

                        <button class="btn btn-primary pull-right btn-sm" data-toggle="modal" data-target=".modal-novo-accordion" title="Adicionar Linha"><i class="fa fa-plus"></i> Adicionar Linha</button>

                        <style>
                            .sortableAcc li{background: none; padding: 0; border: 0}
                        </style>
                        <ol class="sortable sortableAcc">
                            <?php if (isset($accordion)): ?>
                                <?php foreach ($accordion_lines as $linha): ?>
                                    <li id="item_<?= $linha->accordion_id ?>">
                                        <div class="ibox">
                                            <div class="ibox-title">
                                                <h5><?= $linha->accordion_titulo ?></h5>
                                                <div class="ibox-tools">
                                                    <a class="collapse-link">
                                                        <i class="fa fa-chevron-up"></i>
                                                    </a>
<!--                                                    <a onclick="getAjaxAccID('<?= $linha->accordion_id ?>')" data-toggle="modal" data-target=".modal-editar-accordion" class="dropdown-toggle" href="#" title="Editar">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>-->
                                                    <a onclick="getAjaxAccID('<?= $linha->accordion_id ?>')" class="dropdown-toggle" href="#" title="Editar">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                    <a onclick="modalExcluirAccordions('<?= $linha->accordion_id ?>')" title="Deletar">
                                                        <i class="fa fa-times"></i>
                                                    </a>

                                                    <a class="control" title="Mover">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                        <i class="fa fa-ellipsis-v"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="ibox-content" style="display: none;">
                                                <?= $linha->accordion_conteudo ?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ol>



                    </div>
                </div>


            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade modal-novo-accordion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-align-justify"></i> Nova Linha</h4>
            </div>
            <form id="form_pagina" action="<?= base_url('control_admin/accordions/cad') ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Título</label>
                        <?php if (isset($accordion)): ?>
                            <input type="hidden" value="<?= $accordion->accordion_id ?>" id="accordion_id_pai" name="accordion_id_pai" required/>
                            <input type="hidden" value="1" name="accordion_status" required/>
                        <?php else: ?>
                            <input type="hidden" value="<?= date('Y-m-d H:i:s') ?>" name="accordion_data_criacao" required/>
                        <?php endif; ?>
                        <input type="text" value="" id="accordion_titulo" name="accordion_titulo" class="form-control" placeholder="Titulo da Linha" required/>
                    </div>
                    <div class="form-group">
                        <div class="box box-default">
                            <div class="box-header">
                            </div>
                            <div class="box-body pad">
                                <textarea class="form-control" id="editable" name="accordion_conteudo" rows="10" cols="80"><?= isset($accordion) ? $accordion->accordion_conteudo : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnCheck" type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade modal-editar-accordion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-align-justify"></i> Editar Linha</h4>
            </div>
            <form id="form_edit_accordion_line" action="" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Título</label>
                        <?php if (isset($accordion)): ?>
                            <input type="hidden" value="<?= $accordion->accordion_id ?>" id="accordion_id_pai" name="accordion_id_pai" required/>
                            <input type="hidden" value="1" name="accordion_status" required/>
                        <?php else: ?>
                            <input type="hidden" value="<?= date('Y-m-d H:i:s') ?>" name="accordion_data_criacao" required/>
                        <?php endif; ?>
                        <input type="text" value="" name="accordion_titulo" class="form-control j_accordion_titulo" placeholder="Titulo da Linha" required/>
                    </div>
                    <div class="form-group">
                        <div class="box box-default">
                            <div class="box-header">
                            </div>
                            <div class="box-body pad">
                                <textarea class="form-control j_accordion_conteudo" id="editable" name="accordion_conteudo" rows="10" cols="80"><?= isset($accordion) ? $accordion->accordion_conteudo : '' ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnCheck" type="submit" class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>