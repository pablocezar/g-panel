<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>

<script src="<?= $this->PathTheme ?>js/accordion.js"></script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-align-justify"></i> <span id="active_menu">Accordions</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Accordions</strong></li>
        </ol>
    </div>
    <div class="col-lg-5">
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url('admin/formAccordion') ?>" class="btn btn-primary pull-right" title="Criar Novo Accordion"><i class="fa fa-pencil-square-o"></i> Novo Accordion</a>
        <a href="<?= base_url('admin/accordions') ?>" class="btn btn-info pull-right" title="Listar Accordions" style="margin-right: 5px"><i class="fa fa-list"></i> Listar Accordions</a>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Gerenciar Accordions</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <table id="datatable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Nome</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($todos_accordion as $linha):?>
                                <tr>
                                    <td><?= $linha->accordion_titulo ?></td>
                                    <td>
                                    <?php if($linha->accordion_status == 1): ?>
                                        ativo
                                    <?php else: ?>
                                        inativo
                                    <?php endif; ?>
                                    </td>
                                    <td>
                                        <a href="<?= base_url('admin/formAccordion/').$linha->accordion_id?>" title="Editar" class="btn btn-info"><span class="glyphicon glyphicon-pencil"></span></a>
                                        <a onclick="modalExcluirAccordions('<?= $linha->accordion_id ?>')" href="#" title="Deletar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Nome</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>