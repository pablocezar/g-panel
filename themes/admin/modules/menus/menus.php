<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>

<!-- icheck -->
<link href="<?= $this->PathTheme ?>plugins/iCheck/custom.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/iCheck/icheck.min.js"></script>

<!-- nestedSortable -->
<script src="<?= $this->PathTheme ?>plugins/nestedSortable/jquery.mjs.nestedSortable.js"></script>

<script src="<?= $this->PathTheme ?>js/menus.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-list-alt"></i> <span id="active_menu">Menus</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Menus</strong></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="container col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Gerenciar Menus do Site</h5>
            </div>
            <div class="ibox-content">
                <div class="col-md-4">
                    <h4>Adicionar Links</h4>
                    <div class="col-md-4">
                        <div class="radio i-checks"><label> <input type="radio" id="radio_pagina" checked="" value="option1" name="a"> <i class="fa fa-file-o"></i> Página</label></div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio i-checks"><label> <input type="radio" id="radio_cat" value="option2" name="a"> <i class="fa fa-tags"></i> Categoria </label></div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio i-checks"><label> <input type="radio" id="radio_link" value="option3" name="a"> <i class="fa fa-link"></i> Link </label></div>
                    </div>
                    <div class="col-lg-12 row">
                        <hr>
                    </div>
                    <form id="link_pagina" action="<?= base_url() . '/control_admin/menus/cad' ?>" class="" method="post">
                        <input name="menu_nome" value="principal" type="hidden"/>
                        <input name="menu_titulo" id="txt_titulo_pagina" type="hidden"/>

                        <div class="form-group">
                            <select name="menu_nome" id="menu_nome" class="form-control" required>
                                <option value="">Selecione o menu</option>
                                <option value="principal">Principal</option>
                                <option value="rodapé">Rodapé</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <select name="menu_link" id="select_pagina" class="select2" style="width: 100%">
                                <option value="">Selecione a Página</option>
                                <?php foreach ($todas_paginas as $linha): ?>
                                    <option value="<?= base_url() . $linha->pagina_slug ?>" data-titulo="<?= $linha->pagina_titulo ?>"><?= $linha->pagina_titulo ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <input placeholder="Ícone de menu" name="menu_icone" class="form-control" style="width: 100%" type="text"/>
                        </div>

                        <div class="form-group row" style="border: none">
                            <div class="checkbox i-checks"><label> <input type="checkbox" name="menu_target_blank"> <i class="fa fa-share"></i> Abrir em nova janela </label></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Inserir</button>
                    </form>


                    <form id="link_categoria" action="<?= base_url() . '/control_admin/menus/cad' ?>" class="" method="post">
                        <input name="menu_nome" value="principal" type="hidden"/>
                        <input name="menu_titulo" id="txt_titulo_cat" type="hidden"/>
                        
                        <div class="form-group">
                            <select name="menu_nome" id="menu_nome" class="form-control" required>
                                <option value="">Selecione o menu</option>
                                <option value="principal">Principal</option>
                                <option value="rodapé">Rodapé</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <select name="menu_link" id="select_cat" class="select2" style="width: 100%">
                                <option value="">Selecione a Categoria</option>
                                <?php foreach ($categorias as $linha): ?>
                                    <option value="<?= base_url('categoria') . '/' . $linha->categoria_slug ?>"><?= $linha->categoria_titulo ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <input placeholder="Ícone de menu" name="menu_icone" class="form-control" style="width: 100%" type="text"/>
                        </div>
                        <div class="form-group row" style="border: none">
                            <div class="checkbox i-checks"><label> <input type="checkbox" name="menu_target_blank"> <i class="fa fa-share"></i> Abrir em nova janela </label></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Inserir</button>
                    </form>


                    <form id="link_url" action="<?= base_url() . '/control_admin/menus/cad' ?>" class="" method="post">
                        <input name="menu_nome" value="principal" type="hidden"/>
                        
                        <div class="form-group">
                            <select name="menu_nome" id="menu_nome" class="form-control" required>
                                <option value="">Selecione o menu</option>
                                <option value="principal">Principal</option>
                                <option value="rodapé">Rodapé</option>
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <input placeholder="Título do Link" name="menu_titulo" class="form-control" style="width: 100%" type="text"/>
                        </div>

                        <div class="form-group">
                            <input placeholder="Insira aqui o link" name="menu_link" class="form-control" style="width: 100%" type="text"/>
                        </div>
                        <div class="form-group">
                            <input placeholder="Ícone de menu" name="menu_icone" class="form-control" style="width: 100%" type="text"/>
                        </div>
                        <div class="form-group row" style="border: none">
                            <div class="checkbox i-checks"><label> <input type="checkbox" name="menu_target_blank"> <i class="fa fa-share"></i> Abrir em nova janela </label></div>
                        </div>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Inserir</button>
                    </form>

                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Menu Principal</h4>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-sm pull-right" id="btn_edit_menu_principal"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                    <ol class="sortable sortableP" id="menu_sortable">
                        <?php foreach ($menu_principal as $linha): ?>
                            <?php if ($linha->menu_link_pai == 0): ?>
                                <li id="item_<?= $linha->menu_id ?>">
                                    <div>
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                        <?= $linha->menu_titulo; ?>
                                        <a href="<?= base_url() . 'control_admin/menus/removeItemMenu/' . $linha->menu_id ?>" class="pull-right btnRemoveItemMenu" title="Remover item de menu"><i class="fa fa-trash"></i></a>
                                    </div>
                                    <ol>
                                        <?php
                                        $submenu = $this->MenusModel->submenu($linha->menu_id)->result();
                                        ?>
                                        <?php foreach ($submenu as $linha): ?>
                                            <li id="item_<?= $linha->menu_id ?>">
                                                <div>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <?= $linha->menu_titulo; ?>
                                                    <a href="<?= base_url() . 'control_admin/menus/removeItemMenu/' . $linha->menu_id ?>" class="pull-right btnRemoveItemMenu" title="Remover item de menu"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ol>
                                </li>
                            <?php else: ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ol>
                </div>
                
                
                
                
                
                
                
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Menu Rodapé</h4>
                        </div>
                        <div class="col-md-6">
                            <button type="submit" class="btn btn-primary btn-sm pull-right" id="btn_edit_menu_rodape"><i class="fa fa-save"></i> Salvar</button>
                        </div>
                    </div>
                    <ol class="sortable sortableR" id="menu_sortable_rodape">
                        <?php foreach ($menu_rodape as $linha): ?>
                            <?php if ($linha->menu_link_pai == 0): ?>
                                <li id="item_<?= $linha->menu_id ?>">
                                    <div>
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                        <?= $linha->menu_titulo; ?>
                                        <a href="<?= base_url() . 'control_admin/menus/removeItemMenu/' . $linha->menu_id ?>" class="pull-right btnRemoveItemMenu" title="Remover item de menu"><i class="fa fa-trash"></i></a>
                                    </div>
                                    <ol>
                                        <?php
                                        $submenu = $this->MenusModel->submenu($linha->menu_id)->result();
                                        ?>
                                        <?php foreach ($submenu as $linha): ?>
                                            <li id="item_<?= $linha->menu_id ?>">
                                                <div>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <?= $linha->menu_titulo; ?>
                                                    <a href="<?= base_url() . 'control_admin/menus/removeItemMenu/' . $linha->menu_id ?>" class="pull-right btnRemoveItemMenu" title="Remover item de menu"><i class="fa fa-trash"></i></a>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ol>
                                </li>
                            <?php else: ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ol>
                </div>
                
                
                
                
                
                
                
                <div class="clearfix"></div>
            </div>
        </div>
    </div><!-- container -->
</div><!-- row -->