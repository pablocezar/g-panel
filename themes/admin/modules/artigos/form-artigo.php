<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>
<!-- TinyMCE -->
<script src="<?= $this->PathTheme ?>plugins/tinymce/tinymce.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-tynimce.js"></script>

<!-- Datepicker -->
<link href="<?= $this->PathTheme ?>plugins/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/datetimepicker/jquery.datetimepicker.full.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-datepicker.js"></script>

<script src="<?= $this->PathTheme ?>js/artigos.js"></script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-pencil"></i> <span id="active_menu"><?= (isset($artigo) ? 'Editar Artigo' : 'Novo Artigo') ?></span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li><a href="<?= base_url('admin/paginas') ?>" title="Páginas">Artigos</a></li>
            <li class="active"><strong><?= (isset($artigo) ? 'Editar Artigo' : 'Novo Artigo') ?></strong></li>
        </ol>
    </div>
    <div class="col-lg-5">
        <div style="padding-top: 40px"></div>
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <?php if (isset($artigo)): ?>
            <a href="<?= base_url('admin/formArtigo') ?>" class="btn btn-primary pull-right" title="Criar Novo Artigo"><i class="fa fa-pencil"></i> Novo Artigo</a>
        <?php endif; ?>
        <a href="<?= base_url('admin/artigos') ?>" class="btn btn-info pull-right" title="Listar Artigos" style="margin-right: 5px"><i class="fa fa-list"></i> Listar Artigos</a>
    </div>
</div>

<?php if (!$categorias): ?>
    <div class="alert alert-success" role="alert" style="background: #21B9BB; color: #fff; border-radius: 0">
        <b><i class="fa fa-exclamation-circle"></i> Opaa!</b> 
        Para criar seus artigos é necessário primeiramente criar suas categorias para organiza-los para os leitores do seu site! <a style="padding: 5px; background: #fff; color: #21B9BB; border-radius: 3px; margin-left: 10px;" href="<?= base_url('admin/categoriasArtigos')?>" title="Criar Categorias">Criar Categorias</a>
    </div>
<div class="row"></div>
<?php else: ?>

<div class="row">
    <form id="form_pagina" action="<?= (!isset($artigo)) ? base_url('control_admin/paginas/cadastraPagina') : base_url('control_admin/paginas/editaPagina') . '/' . $artigo->pagina_id ?>" method="post">
        <div class="col-lg-9">
            <div class="form-group">
                <input type="text" id="pagina_tipo" value="artigo" name="pagina_tipo" class="hidden"/>
                <label>Título</label>
                <input type="text" value="<?= isset($artigo) ? $artigo->pagina_titulo : '' ?>" id="pagina_titulo" name="pagina_titulo" class="form-control j_pagina_titulo" placeholder="Titulo do Artigo" required/>
                <span style="color:#ff0000; font-weight: bold; display: none" id="msg_pg_duplicada">Atenção esta página já existe!</span>
            </div>
            <div class="form-group">
                <label for="descricao">Sub Título / Descrição <small>(SEO)</small></label>
                <textarea name="pagina_descricao" id="pagina_descricao" class="form-control" rows="2" cols="80" placeholder="Resuma o contéudo abordado neste artigo" required><?= isset($artigo) ? $artigo->pagina_descricao : '' ?></textarea>
            </div>
            <div class="form-group">
                <div class="box box-default">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <textarea id="editable" name="pagina_conteudo" rows="10" cols="80"><?= isset($artigo) ? $artigo->pagina_conteudo : '' ?></textarea>
                    </div>
                </div><!-- /.box -->
            </div>
            <?php if (isset($artigo)): ?>
                <p class="info-artigos">Artigo criado em <?= date('d/m/Y H:i', strtotime($artigo->pagina_data_criacao)) ?> e atualizado pela última vez em <?= date('d/m/Y H:i', strtotime($artigo->pagina_data_alteracao)) ?></p>
            <?php endif; ?>

        </div><!-- /col-lg-9 -->

        <div class="col-lg-3">
            <div class="form-group">
                <label>Link</label>
                <input type="text" value="<?= isset($artigo) ? $artigo->pagina_slug : '' ?>" name="pagina_slug" id="checkSlug" class="form-control j_pagina_slug" placeholder="Link" <?= (isset($artigo) ? 'disabled' : '') ?>/>
            </div>
            <div class="selecionar_imagem_pagina">
                <div class="form-group">
                    <label>Imagem Destacada</label>
                    <a href="#" data-toggle="modal" data-target="#file_manager">
                        <img id="preview" src="<?= isset($artigo) && $artigo->pagina_imagem_destacada != '' ? base_url() . 'tim.php?src=' . $artigo->pagina_imagem_destacada . '&w=360&h=200' : $this->PathTheme . 'assets/images/img-padrao.jpg' ?>" style="max-height:290px" class="img-responsive">
                        <input id="imagem" value="<?= isset($artigo) ? $artigo->pagina_imagem_destacada : '' ?>" name="pagina_imagem_destacada" type="hidden">
                    </a>
                </div>
            </div>
            <div class="form-group">
                <label>Selecione uma Galeria</label>
                <select name="pagina_id_galeria" id="id_galeria" class="form-control">
                    <option value="0">(Sem Galeria)</option>
                    <?php foreach ($todas_galerias as $galeria): ?>
                        <option value="<?= $galeria->galeria_id ?>" <?= (isset($artigo) && $artigo->pagina_id_galeria == $galeria->galeria_id) ? 'selected' : '' ?>><?= $galeria->galeria_titulo ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Categorias</label>
                <select name="pagina_id_categoria[]" class="select2" multiple="multiple" required>
                    <?php $id_categoria = explode(',', $artigo->pagina_id_categoria); ?>
                    <?php foreach ($categorias as $categoria): ?>
                        <option value="<?= $categoria->categoria_id ?>" <?= (in_array($categoria->categoria_id, $id_categoria)) ? 'selected' : '' ?>><?= $categoria->categoria_titulo ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>
                    <input name="pagina_destaque_home" id="pagina_destaque_home" type="checkbox" <?= (isset($artigo) && $artigo->pagina_destaque_home == 'on') ? 'checked' : '' ?>>
                    Artigo Destaque
                </label>
            </div>
            <?php if (!isset($artigo) || (isset($artigo) && $artigo->pagina_postagem_programada != NULL)): ?>
                <div class="form-group" id="data_1">
                    <label>Agendar Postagem</label>
                    <div class="input-group date" id="datePostArtigo">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input id="datetimepicker" type="text" class="form-control" value="<?= (isset($artigo) && $artigo->pagina_postagem_programada != NULL) ? date('d/m/Y H:i:s', strtotime($artigo->pagina_postagem_programada)) : '' ?>" name="pagina_postagem_programada"/>
                    </div>
                </div>
            <?php endif; ?>
            <div class="form-group">
                <input type="hidden" name="pagina_data_alteracao" value="<?= date('Y-m-d H:i:s') ?>"/>
                <?php if (!isset($artigo)): ?>
                    <button id="btnCheck" type="submit" class="btn btn-primary">Salvar</button>
                <?php else: ?>
                    <button id="btn_edit_pagina" type="submit" class="btn btn-primary">Atualizar</button>
                <?php endif ?>
            </div>
        </div><!-- /col-lg-3 -->
    </form>
</div>
<?php endif; ?>