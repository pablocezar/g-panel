<!-- Colorpicker -->
<link href="<?= $this->PathTheme ?>assets/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<script src="<?= $this->PathTheme ?>js/artigos.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-tags"></i> <span id="active_menu">Categorias</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li><a href="<?= base_url('admin/paginas') ?>" title="Páginas">Artigos</a></li>
            <li class="active"><strong>Categorias</strong></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-5">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><?= (isset($categoria) ? 'Editar Categoria' : 'Criar Categorias') ?></h5>
                <?php if(isset($categoria)):?>
                <div class="pull-right" style="margin-top:-7px;">
                    <a href="<?= base_url('admin').'/'.'categoriasArtigos' ?>" class="btn btn-sm btn-primary" title="Criar Categoria"><i class="fa fa-plus"></i> Criar Nova</a>
                </div>
                <?php endif;?>
            </div>
            <div class="ibox-content">
                <p>
                    Crie e edite categorias e organize o conteúdo do site, você pode também declarar cores para cada categoria criada.
                </p>
                <form id="form_categorias" data-ajax="<?= (isset($categoria) ? 'editAjax' : 'cadAjax') ?>" action="<?= (isset($categoria) ? base_url('control_admin/categorias/editaCategoria/') . $categoria->categoria_id : base_url('control_admin/categorias/cadastraCategoria')) ?>" method="post">

                    <div class="form-group">
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#file_manager">
                            <span class="glyphicon glyphicon-picture"></span> Imagem destacada
                        </a>
                    </div>


                    <div class="form-group">
                        <img id="preview" src="<?= isset($categoria) && $categoria->categoria_imagem_destacada != '' ? base_url() . $categoria->categoria_imagem_destacada : $this->PathTheme . 'assets/images/img-padrao.jpg' ?>" style="max-height:290px" class="img-responsive">
                        <input id="imagem" value="<?= isset($categoria) ? $categoria->categoria_imagem_destacada : '' ?>" name="categoria_imagem_destacada" type="hidden">
                    </div>

                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label>Selecione a cor</label>
                            <input name="categoria_cor" value="<?= (isset($categoria)) ? $categoria->categoria_cor : '' ?>" type="text" class="form-control j_color_picker" value="" />
                        </div>
                        <div class="form-group col-xs-6">
                            <label>Amostra</label>
                            <div style="background-color: <?= (isset($categoria)) ? $categoria->categoria_cor : 'rgb(24,166,137)' ?>; width: 100%; height: 35px;" id="catColor_amostra"></div>
                        </div>
                    </div><!-- row -->

                    <div class="form-group">
                        <label>Titulo da Categoria</label>
                        <input name="categoria_titulo" value="<?= (isset($categoria)) ? $categoria->categoria_titulo : '' ?>" type="text" class="form-control j_categoria_titulo" id="categoria_titulo" placeholder="Titulo"/>
                        <input type="hidden" value="<?= (isset($categoria)) ? $categoria->categoria_id : '' ?>" id="id_cat"/>
                        <input type="hidden" id="checkSlug"/>
                        <span style="color:#ff0000; font-weight: bold; display: none" id="msg_pg_duplicada">Atenção esta categoria já existe!</span>
                    </div>

                    <div class="form-group">
                        <label>Descrição</label>
                        <textarea name="categoria_descricao" placeholder="Descrição" class="form-control"><?= (isset($categoria)) ? $categoria->categoria_descricao : '' ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label>Destacar na Home</label>
                        <select class="form-control" name="categoria_destaque">
                            <option value="Não" <?= (isset($categoria) && $categoria->categoria_destaque == 'Não') ? 'selected' : '' ?>>Não</option>
                            <option value="Sim" <?= (isset($categoria) && $categoria->categoria_destaque == 'Sim') ? 'selected' : '' ?>>Sim</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" id="btnCheck" class="btn btn-primary">Salvar</button>
                        <img src="<?= $this->PathTheme ?>/assets/images/ajax_loader.gif" alt="img-ajax" class="img-ajax-loader"/>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- col-md-5 -->

    <div class="col-md-7">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Categorias Cadastradas</h5>
            </div>
            <div class="ibox-content">

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Cor</th>
                            <th>Imagem</th>
                            <th>Titulo</th>
                            <th>Decrição</th>
                            <th>Açoes</th>
                        </tr>
                    </thead>
                    <tbody id="resultAjaxCategorias">
                        <!-- Alimentado via Ajax ( getResultsAjaxCategorias() ) -->
                    </tbody>
                </table>

            </div>
        </div>
    </div><!-- col-md-7 -->

</div><!-- row -->