<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-clock-o"></i> <span id="active_menu">Artigos Agendados</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Todos Artigos</strong></li>
        </ol>
    </div>
    <div class="col-lg-9">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url() ?>admin/formArtigo" title="Novo Artigo" class="btn btn-primary pull-right"><i class="fa fa-pencil"></i> Novo Artigo</a>
    </div>
</div>

<div class="row">
    <?php foreach ($artigos['paginas']->result() as $linha): ?>
        <?php if ($linha->pagina_postagem_programada == NULL): ?>
            <?php unset($linha); ?>
        <?php else: ?>
            <div class="col-md-3 col-sm-6">
                <div class="ibox-content text-center" style="min-height: 300px">
                    <div class="m-b-sm">
                        <img class="img-dest-pagina" alt="image" src="<?= ($linha->pagina_imagem_destacada == '') ? $this->PathTheme . 'assets/images/img-padrao.jpg' : base_url('') . $linha->pagina_imagem_destacada ?>">
                    </div>
                    <p class="font-bold"><?= $linha->pagina_titulo ?></p>
                    <p class="font-bold"><i class="fa fa-clock-o"></i> <?= date('d/m/Y H:i:s', strtotime($linha->pagina_postagem_programada))?></p>
                    <div class="text-center">
                        <a href="<?= base_url() . $linha->pagina_slug ?>" target="_blank" class="btn btn-xs btn-primary">
                            <i class="fa fa-share"></i>
                            Ver
                        </a>
                        <a href="<?= base_url() . 'admin/formArtigo/' . $linha->pagina_id ?>" class="btn btn-xs btn-warning">
                            <i class="fa fa-pencil"></i>
                            Editar
                        </a>
                        <a onclick="modalExcluirPagina(<?= $linha->pagina_id ?>)" class="btn btn-xs btn-danger">
                            <i class="fa fa-trash"></i>
                            Excluir
                        </a>
                    </div>
                </div>
                <div style="padding: 10px 0"></div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
</div><!-- /row -->
<br>
<br>
<br>