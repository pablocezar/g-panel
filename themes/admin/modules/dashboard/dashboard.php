<!-- Datepicker -->
<link href="<?= $this->PathTheme ?>plugins/datetimepicker/jquery.datetimepicker.min.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/datetimepicker/jquery.datetimepicker.full.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-datepicker.js"></script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-home"></i> <span id="active_menu">Dashboard</span></h2>
        <ol class="breadcrumb">
            <li class="active"><strong>Dashboard</strong></li>
        </ol>
    </div>
</div>

<?php if ($this->ConfigSysten->config_seo == "Inativo"): ?>
    <div class="container-fluid row">
        <div class="alert alert-warning">
            <h3>Atenção este site não esta configurado para ser rastreado corretamente pelos motores de busca!</h3>
            Para configurar vá em <b><i class="fa fa-geas"></i> Configurações</b>  e em <b>Configurações Web Site</b> na opçao <b>Sitemap e Meta Tags (SEO)</b> selecione <b>Ativar</b>!
        </div>
    </div>
<?php endif; ?>

<?php if ($trafegoMensal == false): ?>
    <div class="container-fluid row">
        <div class="alert alert-danger">
            Não foi possivel localizar a chave <b>(.p12)</b> da conta do Analytics. Sem essa chave não é possivel realizar relatórios.
        </div>
    </div>
<?php else: ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="form-group" id="data_5">
                <form class="form-inline" action="<?= base_url('admin/getAjaxGA') ?>" id="filtro_analytics" method="post">
                    <div class="input-daterange input-group form-group" id="datepicker">
                        <input type="text" class="input-sm form-control datepickerDashboard" name="dataIni" value="<?= date('01/m/Y') ?>"/>
                        <span class="input-group-addon">até</span>
                        <input type="text" class="input-sm form-control datepickerDashboard" id="dataFim" name="dataFim" value="<?= date('d/m/Y') ?>" />
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-refresh"></i> Atualizar</button>
                        <img src="<?= $this->PathTheme ?>/assets/images/ajax_loader.gif" alt="img-ajax" class="img-ajax-loader"/>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
            <?php foreach ($trafegoMensal as $linha): ?>
                <div class="col-md-3">
                    <div class="widget style1 navy-bg">
                        <div class="row vertical-align">
                            <div class="col-xs-3">
                                <i class="fa fa-users fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="font-bold" id="getVisits"><?= $linha->getVisits() ?></h2>
                                Visitas
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget style1 yellow-bg">
                        <div class="row vertical-align">
                            <div class="col-xs-3">
                                <i class="fa fa-eye fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="font-bold" id="getPageviews"><?= $linha->getPageviews() ?></h2>
                                PageViews
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget style1 blue-bg">
                        <div class="row vertical-align">
                            <div class="col-xs-3">
                                <i class="fa fa-user-plus fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="font-bold" id="getnewUsers"><?= $linha->getnewUsers() ?></h2>
                                Novos Usuários
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget style1 lazur-bg">
                        <div class="row vertical-align">
                            <div class="col-xs-3">
                                <i class="fa fa-globe fa-3x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <h2 class="font-bold" id="getorganicSearches"><?= $linha->getorganicSearches() ?></h2>
                                Tráfegos Orgânicos
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
    </div><!-- row -->

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Páginas mais visitadas</h5>
                </div>
                <div class="ibox-content table-responsive">
                    <table class="table table-hover no-margins">
                        <thead>
                            <tr>
                                <th>Página</th>
                                <th>Vizualizações</th>
                                <th>Visitas Orgânicas</th>
                                <th>Tax. Rejeição</th>
                            </tr>
                        </thead>
                        <tbody id="j_ajax_result_pages">
                            <?php arsort($trafegoPaginas) ?>
                            <?php $array = array_slice($trafegoPaginas, 0, 10) ?>
                            <?php foreach ($array as $linha): ?>
                                <tr>
                                    <td><?= $linha->getpageTitle() ?></td>
                                    <td><?= $linha->getpageviews() ?></td>
                                    <td><?= $linha->getorganicSearches() ?></td>
                                    <td class="text-navy">
                                        <i class="fa fa-level-<?= ( $linha->getbounceRate() <= 50) ? 'up' : 'down' ?>"></i> 
                                        <?= (int) $linha->getbounceRate() ?>%
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
    <br>
    <br>
    <br>
    <div style="clear: both"></div>