<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>

<script src="<?= $this->PathTheme ?>js/regioes-atendimento.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-map-marker"></i> <span id="active_menu">Regiões de Atendimento</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Regiões de Atendimento</strong></li>
        </ol>
    </div>
</div>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5><?= (!isset($regiao) ? 'Cadastrar Regiões': 'Editar Região')?> de Atendimento</h5>
    </div>
    <div class="ibox-content">
        <div class="col-md-6">
            <form action="<?= base_url() . 'control_admin/regioesAtendimento/' . (!isset($regiao) ? 'cad' : 'edit/'.$regiao->regiao_id) ?>" method="post">
                <div class="form-group">
                    <label for="regiao_nome">Nome da Região</label>
                    <input class="form-control" value="<?= (isset($regiao) ? $regiao->regiao_nome : '') ?>" name="regiao_nome" id="regiao_nome" placeholder="Nome da Região" type="text"/>
                </div>
                <div class="form-group">
                    <label for="regiao_localidades">Localidades</label>
                    <input class="select2" value="<?= (isset($regiao) ? $regiao->regiao_localidades : '') ?>" name="regiao_localidades" id="regiao_localidades" placeholder="Localidades" type="text" style="width: 100%"/>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div><!-- col-md-6 -->
        <div class="col-md-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Região</th>
                        <th>Status</th>
                        <th>Opções</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($regioes as $linha): ?>
                        <tr>
                            <td><?= $linha->regiao_nome ?></td>
                            <td><?= $linha->regiao_status ?></td>
                            <td>
                                <a href="<?= base_url().'admin/regioes_atendimento'.'/'.$linha->regiao_id ?>" data-toggle="tooltip" title="Editar" class="btn btn-warning btn-sm"><span class="fa fa-pencil"></span></a>
                                <a onclick="modalExcluirRegioesAtend('<?= $linha->regiao_id ?>')" data-toggle="tooltip" href="#" title="Deletar" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div><!-- col-md-6 -->
        <div style="clear: both"></div>
    </div>
</div>