
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Painel Admin G Panel - <?= $config->config_nome_empresa ?></title>

        <link rel="shortcut icon" href="<?= $theme ?>assets/images/favicon.png"/>
        <link href="<?= $theme ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= $theme ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="<?= $theme ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?= $theme ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= $theme ?>plugins/notificacao/pblSimpleNotification.css" rel="stylesheet">
        <link href="<?= $theme ?>pbl.css" rel="stylesheet">

    </head>

    <body class="gray-bg bg_login">

        <div class="middle-box text-center loginscreen  animated fadeInDown">
            <div>
                <div>

                    <div style="padding: 55px 0"></div>
                    <?php
                    $logo = ($config->config_logo != '') ? $config->config_logo : $theme.'assets/images/logo-g-panel.png';
                    ?>
                    <center class="logo-name">
                        <img class="img-responsive" src="<?= $logo ?>" style="max-height: 100px" alt="G Panel" title="G Panel">
                    </center>

                </div>

                <?php if ($this->session->flashdata("erro")): ?>
                    <div class="alert alert-danger alert-dismissible alert_login" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Erro:</strong> <?= $this->session->flashdata("erro") ?>
                    </div>
                <?php endif; ?>

                <form class="m-t formLogin" role="form" id="formLogin" action="<?= base_url('login') . '/' . 'ajaxLogar' ?>" method="post">
                    <div class="form-group">
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="fa fa-user"></i></div>
                            <input type="email" name="usuario_email" class="form-control" placeholder="email" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group mb-2 mb-sm-0">
                            <div class="input-group-addon"><i class="fa fa-lock"></i></div>
                            <input type="password" name="usuario_senha" class="form-control" placeholder="senha" required="">
                        </div>
                    </div>
                    <button type="submit" name="btn_logar" class="btn btn-primary block full-width m-b btn_logar"><i class="fa fa-sign-in"></i> Login</button>
                    <a href="<?= base_url() ?>" title="Voltar ao site <?= $config->config_nome_empresa ?>"><small><i class="fa fa-mail-reply"></i> Voltar ao site <?= $config->config_nome_empresa ?>.</small></a>
                </form>
                
                <p class="m-t"> <small style="color: #fff">&copy; 2015 - <?= date('Y') ?>. Desenvolvido por <a href="https://www.genesystech.com.br" title="Genesys Tech" target="_blank">Genesys Tech</a></small> </p>
                <p class="m-t"> <small style="color: #fff">Painel Administrativo G-Panel, uso exclusivo para clientes da Genesys Tech.</small> </p>
            </div>
        </div>
        <script>
            var base_url = '<?= base_url() ?>';
        </script>
        <!-- Mainly scripts -->
        <script src="<?= $theme ?>assets/js/jquery-2.1.1.js"></script>
        <script src="<?= $theme ?>assets/js/bootstrap.min.js"></script>
        <script src="<?= $theme ?>plugins/notificacao/pblSimpleNotification.js"></script>
        <script src="<?= $theme ?>js/login.js"></script>
    </body>

</html>
