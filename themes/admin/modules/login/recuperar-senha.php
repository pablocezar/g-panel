<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Painel Admin <?= $info_site->config_titulo ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?= base_url() ?>themes/admin/dist/css/AdminLTE.min.css">
        
        <!--Custom CSS-->
        <link rel="stylesheet" href="<?= base_url() ?>themes/admin/pbl.css">
        
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?= base_url() ?>themes/admin/dist/img/fav-icon.png">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page background-login">

        <!-- Automatic element centering -->
        <div class="lockscreen-wrapper">
            <div class="lockscreen-logo">
                <a title="recuperar senha" href="#"><?= $titulo ?></a>
            </div>
            <?php if ($this->session->flashdata("erro")): ?>
            <div class="lockscreen-name">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Erro:</strong> <?= $this->session->flashdata("erro") ?>
                </div>
            </div>
            <br>
            <?php endif; ?>
            <?php if ($this->session->flashdata("sucesso")): ?>
            <div class="lockscreen-name">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Ok:</strong> <?= $this->session->flashdata("sucesso") ?>
                </div>
            </div>
            <br>
            <?php endif; ?>
            <!-- START LOCK SCREEN ITEM -->
            <div class="lockscreen-item">
                <!-- lockscreen image -->
                <div class="lockscreen-image">
                    <img src="<?= base_url().$info_site->config_logo ?>" alt="User Image">
                </div>
                <!-- /.lockscreen-image -->

                <!-- lockscreen credentials (contains the form) -->
                <form id="form_recuperar_senha"  action="<?= base_url('login/redefinir_senha') ?>" method="post" class="lockscreen-credentials">
                    <div class="input-group">
                        <input type="email" name="usuario_email" id="usuario_email" class="form-control" placeholder="Email">
                        <div class="input-group-btn">
                            <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
                        </div>
                    </div>
                </form><!-- /.lockscreen credentials -->

            </div><!-- /.lockscreen-item -->
            <div class="help-block text-center" style="color:#fff">
                Informe o seu email e você recebará uma nova senha!
            </div>
            <div class="lockscreen-footer text-center">
                Copyright &copy; 2016-<?= date('Y'); ?> <b> - <a href="http://www.genesystech.com.br" target="_blank">Genesys Tech</a></b><br>
                Todos os Direitos Reservados
            </div>
        </div><!-- /.center -->


        <!-- jQuery 2.1.4 -->
        <script src="<?= base_url() ?>assets/bootstrap/js/jquery.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
