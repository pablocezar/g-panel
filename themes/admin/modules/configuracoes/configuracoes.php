<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-gears"></i> <span id="active_menu">Configurações</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Configurações</strong></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="<?= ($this->User->usuario_nivel_acesso >= 3) ? 'col-md-8' : 'col-md-12' ?>">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5><i class="fa fa-gears"></i> Configurações Gerais <small class="m-l-sm">Configurações gerais do site e CMS</small></h5>
            </div>
            <div class="ibox-content">
                <div class="panel blank-panel">

                    <div class="panel-heading">
                        <div class="panel-options">

                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#Empresa"><i class="fa fa-building-o"></i> Info. da Empresa</a></li>
                                <li class=""><a data-toggle="tab" href="#RedesSociais"><i class="fa fa-laptop"></i> Redes Sociais</a></li>
                                <?php if ($this->User->usuario_nivel_acesso >= 3): ?>
                                    <li class=""><a data-toggle="tab" href="#CodAnalytics"><i class="fa fa-area-chart"></i> Cód. Analytics</a></li>
                                <?php endif ?>
                                <li class=""><a data-toggle="tab" href="#MapaGoogle"><i class="fa fa-map-marker"></i> Mapa Google</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="tab-content">
                            <div id="Empresa" class="tab-pane active">
                                <form class="row" action="<?= base_url('control_admin') . '/configuracoes/editaConfiguracoes' ?>" method="post">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Logo</label><br>
                                            <a href="#" data-toggle="modal" data-target="#file_manager" style="display: inline-block">
                                                <img id="preview" src="<?= $configuracoes->config_logo != '' ? Thumbnail(360, 200, $configuracoes->config_logo, 2) : $this->PathTheme . 'assets/images/img-padrao.jpg' ?>" style="max-height:120px" class="img-responsive">
                                                <input id="imagem" value="<?= $configuracoes->config_logo ?>" name="config_logo" type="hidden">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nome_empresa">Nome da Empresa</label>
                                            <input name="config_nome_empresa" value="<?= $configuracoes->config_nome_empresa ?>" id="nome_empresa" class="form-control" placeholder="Nome da Empresa" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone">Telefone</label>
                                            <input name="config_telefone" value="<?= $configuracoes->config_telefone ?>"  id="telefone" class="form-control" placeholder="(00) 0000-0000" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="telefone_2">Telefone 2</label>
                                            <input name="config_telefone_2" value="<?= $configuracoes->config_telefone_2 ?>" id="telefone_2" class="form-control" placeholder="(00) 0000-0000" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input name="config_email" value="<?= $configuracoes->config_email ?>" id="email" class="form-control" placeholder="email" type="email"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="email_2">Email 2</label>
                                            <input name="config_email_2" value="<?= $configuracoes->config_email_2 ?>" id="email_2" class="form-control" placeholder="email 2" type="email"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="horario_atendimento">Horário de Atendimento</label>
                                            <input name="config_horario_atendimento" value="<?= $configuracoes->config_horario_atendimento ?>" id="horario_atendimento" class="form-control" placeholder="Horário de Atendimento" type="text"/>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="cep">CEP</label>
                                            <input name="config_cep" value="<?= $configuracoes->config_cep ?>" id="config_cep" class="form-control" placeholder="CEP" type="text"/>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-xs-8">
                                                <label for="logradouro">Rua / Logradouro</label>
                                                <input name="config_logradouro" value="<?= $configuracoes->config_logradouro ?>" id="config_logradouro" class="form-control" placeholder="Rua / Logradouro" type="text"/>
                                            </div>
                                            <div class="form-group col-xs-4">
                                                <label for="numero">Número</label>
                                                <input name="config_numero" value="<?= $configuracoes->config_numero ?>" id="numero" class="form-control" placeholder="N°" type="text"/>
                                            </div>
                                        </div><!-- row -->
                                        <div class="form-group">
                                            <label for="complemento">Complemento</label>
                                            <input name="config_complemento" value="<?= $configuracoes->config_complemento ?>" id="config_complemento" class="form-control" placeholder="Complemento" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="localidade">Localidade / Cidade</label>
                                            <input name="config_localidade" value="<?= $configuracoes->config_localidade ?>" id="config_localidade" class="form-control" placeholder="Localidade / Cidade" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="uf">UF</label>
                                            <input name="config_uf" value="<?= $configuracoes->config_uf ?>" id="config_uf" class="form-control" placeholder="UF" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="descricao">Descrição</label>
                                            <textarea name="config_descricao" rows="4" id="descricao" class="form-control" placeholder="Descrição da Empresa"><?= $configuracoes->config_descricao ?></textarea>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div><!-- col-lg-12 -->

                                </form>
                            </div><!-- #Website -->

                            <div id="RedesSociais" class="tab-pane">
                                <form class="row" action="<?= base_url('control_admin') . '/configuracoes/editaConfiguracoes' ?>" method="post">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook</label>
                                            <input name="config_facebook" value="<?= $configuracoes->config_facebook ?>" id="facebook" class="form-control" placeholder="link facebook" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter">Twitter</label>
                                            <input name="config_twitter" value="<?= $configuracoes->config_twitter ?>" id="twitter" class="form-control" placeholder="link google plus" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="instagram">Instagram</label>
                                            <input name="config_instagram" value="<?= $configuracoes->config_instagram ?>" id="istagram" class="form-control" placeholder="link istagram" type="text"/>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="google_plus">Google Plus</label>
                                            <input name="config_google_plus" value="<?= $configuracoes->config_google_plus ?>" id="google_plus" class="form-control" placeholder="link google plus" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="youtube">Youtube</label>
                                            <input name="config_youtube" value="<?= $configuracoes->config_youtube ?>" id="youtube" class="form-control" placeholder="link youtube" type="text"/>
                                        </div>
                                        <div class="form-group">
                                            <label for="linkedin">Linkedin</label>
                                            <input name="config_linkedin" value="<?= $configuracoes->config_linkedin ?>" id="linkedin" class="form-control" placeholder="link linkedin" type="text"/>
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div><!-- col-lg-12 -->

                                </form>
                            </div><!-- #RedesSociais -->

                            <div id="CodAnalytics" class="tab-pane">
                                <form class="row" action="<?= base_url('control_admin') . '/configuracoes/editaConfiguracoes' ?>" method="post">
                                    <div class="form-group">
                                        <label for="cod_analytics">Código do Analytics</label>
                                        <textarea name="config_cod_analytics" class="form-control" rows="7" id="cod_analytics" placeholder="Insira aqui o código do Google Analytics"><?= $configuracoes->config_cod_analytics ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="config_report_id">ID Analytics Report</label>
                                        <input type="text" name="config_report_id" class="form-control" id="report_id" value="<?= $configuracoes->config_report_id ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="config_client_email_report">Client Email User Analytics</label>
                                        <input type="email" name="config_client_email_report" class="form-control" id="client_email_report" value="<?= $configuracoes->config_client_email_report ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div>
                                </form>
                            </div><!-- #CodAnalytics -->

                            <div id="MapaGoogle" class="tab-pane">
                                <form class="row" action="<?= base_url('control_admin') . '/configuracoes/editaConfiguracoes' ?>" method="post">
                                    <div class="form-group">
                                        <label for="cod_mapa_google">Código do Mapa do Google</label>
                                        <textarea name="config_cod_mapa_google" class="form-control" rows="4" id="cod_mapa_google" placeholder="Insira aqui o código do Mapa do Google"><?= $configuracoes->config_cod_mapa_google ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Salvar</button>
                                    </div>
                                </form>
                            </div><!-- #CodAnalytics -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($this->User->usuario_nivel_acesso >= 3): ?>
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><i class="fa fa-gears"></i> Configurações Web Site</h5>
                </div>
                <div class="ibox-content">
                    <form action="<?= base_url('control_admin') . '/configuracoes/editaConfiguracoes' ?>" method="post">
                        <div class="form-group">
                            <label for="manutencao">Site em Manutenção</label>
                            <select name="config_manutencao" id="manutencao" class="form-control">
                                <option value="0" <?= ($configuracoes->config_manutencao == 0) ? 'selected' : '' ?>>Não</option>
                                <option value="1" <?= ($configuracoes->config_manutencao == 1) ? 'selected' : '' ?>>Sim</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tema_site">Tema do Site</label>
                            <select name="config_tema_site" id="tema_site" class="form-control">
                                <?php foreach (MY_Controller::getTemplates() as $linha): ?>
                                    <option value="<?= $linha ?>" <?= ($configuracoes->config_tema_site === $linha) ? 'selected' : '' ?>><?= $linha ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="seo">Sitemap e Meta Tags (SEO)</label>
                            <select name="config_seo" id="manutencao" class="form-control">
                                <option value="Ativo" <?= ($configuracoes->config_seo == "Ativo") ? 'selected' : '' ?>>Ativo</option>
                                <option value="Inativo" <?= ($configuracoes->config_seo == "Inativo") ? 'selected' : '' ?>>Inativo</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="pag_lista_artigos">Listar Artigos em:</label>
                            <input name="config_pag_lista_artigos" value="<?= $configuracoes->config_pag_lista_artigos ?>" id="pag_lista_artigos" class="form-control" placeholder="Informe a url da página que irá listar os artigos" type="text">
                        </div>
                        <div class="form-group">
                            <label for="num_pag_paginacao">Quant. Artigos por Paginação</label>
                            <input name="config_num_pag_paginacao" value="<?= $configuracoes->config_num_pag_paginacao ?>" id="num_pag_paginacao" class="form-control" type="number" min="1">
                        </div>
                        <div class="form-group">
                            <label for="smtp_host">Host - SMTP</label>
                            <input name="config_smtp_host" value="<?= $configuracoes->config_smtp_host ?>" id="smtp_host" class="form-control" placeholder="Host smtp" type="text">
                        </div>
                        <div class="form-group">
                            <label for="smtp_porta">Porta - SMTP</label>
                            <input name="config_smtp_porta" value="<?= $configuracoes->config_smtp_porta ?>" id="smtp_porta" class="form-control" placeholder="Porta - SMTP" type="text">
                        </div>
                        <div class="form-group">
                            <label for="smtp_usuario">Usuário - SMTP</label>
                            <input name="config_smtp_usuario" value="<?= $configuracoes->config_smtp_usuario ?>" id="smtp_usuario" class="form-control" placeholder="Usuário - SMTP" type="text">
                        </div>
                        <div class="form-group">
                            <label for="smtp_senha">Senha - SMTP</label>
                            <input name="config_smtp_senha" value="<?= $configuracoes->config_smtp_senha ?>" id="smtp_senha" class="form-control" placeholder="Senha - SMTP" type="password">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div><!-- col-lg-12 -->
                    </form>
                </div>
            </div>
        </div>
    <?php endif ?>
</div><!-- row -->