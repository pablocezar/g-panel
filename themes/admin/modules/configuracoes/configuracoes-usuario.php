<!-- Cropper -->
<link href="<?= $this->PathTheme ?>assets/css/plugins/cropper/cropper.min.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>assets/js/plugins/cropper/cropper.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-cropper.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i> <span id="active_menu">Configurações Usuário</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Configurações Usuário</strong></li>
        </ol>
    </div>
</div>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Editar Usuário</h5>
    </div>
    <div class="ibox-content">
        <form id="formUsuario" action="<?= base_url('control_admin/usuarios/editaUsuario/') . $usuario->usuario_id ?>" method="post" enctype="multipart/form-data">

            <div class="col-md-4">
                <div class="form-group">
                    <label title="Upload de Foto" for="usuario_foto" class="btn btn-primary">
                        <input accept="image/*" id="usuario_foto" class="hide" type="file">
                        Upload de Foto
                    </label>
                    <input name="usuario_foto" id="cropp_image"  type="hidden">
                    <div class="image-crop">
                        <img class="img-responsive" src="<?= ($usuario->usuario_foto != '') ? base_url($usuario->usuario_foto) : $this->PathTheme.'assets/images/user-default.png' ?>">
                    </div>
                </div>
            </div><!-- col-md-4 -->

            <div class="col-md-8">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input name="usuario_nome" id="usuario_nome" value="<?= isset($usuario) ? $usuario->usuario_nome : '' ?>" class="form-control" placeholder="Nome do usuário" type="text" required/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="usuario_email" id="usuario_email" value="<?= isset($usuario) ? $usuario->usuario_email : '' ?>" class="form-control" placeholder="usuario@exemplo.com" type="email" required/>
                </div>
                <div class="form-group">
                    <label for="tipo_menu">Menu</label>
                    <select class="form-control" name="usuario_tipo_menu" id="tipo_menu">
                        <option value="mini-navbar" <?= (isset($usuario) && $usuario->usuario_tipo_menu == 'mini-navbar') ? 'selected' : '' ?>>Recolhido</option>
                        <option value="" <?= (isset($usuario) && $usuario->usuario_tipo_menu == '') ? 'selected' : '' ?>>Expandido</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="senha">Senha</label>
                    <input name="usuario_senha" id="usuario_senha" value="" class="form-control" placeholder="Senha" type="password" <?= (isset($usuario) ? '' : 'required') ?>/>
                </div>
                <div class="form-group">
                    <label for="confima_senha">Confirme a Senha</label>
                    <input id="confima_senha" value="" class="form-control" placeholder="Confirme a Senha" type="password" <?= (isset($usuario) ? '' : 'required') ?>/>
                    <span style="color: #ff0033; display: none" id="msgValidaSenha">Senhas não Conferem!</span>
                </div>
                <div class="form-group">
                    <button type="submit" id="btnUsuarios" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                </div>
            </div><!-- col-md-8 -->
            <div style="clear: both"></div>

        </form>
    </div>
</div>