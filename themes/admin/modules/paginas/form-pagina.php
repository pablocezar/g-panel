<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>
<!-- TinyMCE -->
<script src="<?= $this->PathTheme ?>plugins/tinymce/tinymce.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-tynimce.js"></script>

<script src="<?= $this->PathTheme ?>js/paginas.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-file-o"></i> <span id="active_menu"><?= (isset($pagina) ? 'Editar Página' : 'Nova Página') ?></span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li><a href="<?= base_url('admin/paginas') ?>" title="Páginas">Páginas</a></li>
            <li class="active"><strong><?= (isset($pagina) ? 'Editar Página' : 'Nova Página') ?></strong></li>
        </ol>
    </div>
    <div class="col-lg-5">
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <?php if (isset($pagina)): ?>
            <a href="<?= base_url('admin/formPagina') ?>" class="btn btn-primary pull-right" title="Criar Nova Página"><i class="fa fa-pencil-square-o"></i> Nova Página</a>
        <?php endif; ?>
        <a href="<?= base_url('admin/paginas') ?>" class="btn btn-info pull-right" title="Listar Páginas" style="margin-right: 5px"><i class="fa fa-list"></i> Listar Páginas</a>
    </div>
</div>

<div class="row">
    <form id="form_pagina" action="<?= (!isset($pagina)) ? base_url('control_admin/paginas/cadastraPagina') : base_url('control_admin/paginas/editaPagina') . '/' . $pagina->pagina_id ?>" method="post">
        <div class="col-lg-9">
            <div class="form-group">
                <input type="text" id="pagina_tipo" value="pagina" name="pagina_tipo" class="hidden"/>
                <label>Título</label>
                <input type="text" value="<?= isset($pagina) ? $pagina->pagina_titulo : '' ?>" id="pagina_titulo" name="pagina_titulo" class="form-control j_pagina_titulo" placeholder="Titulo da Página" required/>
                <span style="color:#ff0000; font-weight: bold; display: none" id="msg_pg_duplicada">Atenção esta página já existe!</span>
            </div>
            <div class="form-group">
                <label for="descricao">Sub Título / Descrição <small>(SEO)</small></label>
                <textarea name="pagina_descricao" id="pagina_descricao" class="form-control" rows="2" cols="80" placeholder="Resuma o contéudo abordado nesta página"><?= isset($pagina) ? $pagina->pagina_descricao : '' ?></textarea>
            </div>
            <div class="form-group">
                <div class="box box-default">
                    <div class="box-header">
                    </div><!-- /.box-header -->
                    <div class="box-body pad">
                        <textarea id="editable" name="pagina_conteudo" rows="10" cols="80"><?= isset($pagina) ? $pagina->pagina_conteudo : '' ?></textarea>
                    </div>
                </div><!-- /.box -->
            </div>

        </div><!-- /col-lg-9 -->

        <div class="col-lg-3">
            <div class="form-group">
                <label>Link</label>
                <input type="text" value="<?= isset($pagina) ? $pagina->pagina_slug : '' ?>" name="pagina_slug" id="checkSlug" class="form-control j_pagina_slug <?= (!isset($pagina) ? 'j_blocked' : '')?>" placeholder="Link" <?= (isset($pagina) ? 'disabled' : '') ?>/>
            </div>
            <div class="selecionar_imagem_pagina">
                <div class="form-group">
                    <label>Imagem Destacada</label>
                    <a href="#" title="Imagem destacada" data-toggle="modal" data-target="#file_manager">
                        <img id="preview" src="<?= isset($pagina) && $pagina->pagina_imagem_destacada != '' ? base_url().'tim.php?src='. $pagina->pagina_imagem_destacada.'&w=360&h=200' : $this->PathTheme . 'assets/images/img-padrao.jpg' ?>" style="max-height:290px" class="img-responsive">
                        <input id="imagem" value="<?= isset($pagina) ? $pagina->pagina_imagem_destacada : '' ?>" name="pagina_imagem_destacada" type="hidden">
                    </a>
                </div>
            </div>
            <div class="form-group">
                <label>Selecione uma Galeria</label>
                <select name="pagina_id_galeria" id="id_galeria" class="form-control">
                    <option value="0">(Sem Galeria)</option>
                    <?php foreach ($todas_galerias as $galeria): ?>
                        <option value="<?= $galeria->galeria_id ?>" <?= (isset($pagina) && $pagina->pagina_id_galeria == $galeria->galeria_id) ? 'selected' : '' ?>><?= $galeria->galeria_titulo ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>
                    <input name="pagina_destaque_home" id="pagina_destaque_home" type="checkbox" <?= (isset($pagina) && $pagina->pagina_destaque_home == 'on') ? 'checked' : '' ?>>
                    Exibir Página na Home
                </label>
            </div>
             <div class="form-group">
                <label>Data de Criação</label>
                <input name="pagina_data_criacao" value="<?= isset($pagina) ? date('d/m/Y H:i', strtotime($pagina->pagina_data_criacao)) : '' ?>" id="pagina_data_criacao" type="text" class="form-control" disabled/>
            </div>
            <div class="form-group">
                <input type="hidden" name="pagina_data_alteracao" value="<?= date('Y-m-d H:i:s') ?>"/>
                <?php if (!isset($pagina)): ?>
                    <button id="btnCheck" type="submit" class="btn btn-primary">Salvar</button>
                <?php else: ?>
                    <button id="btn_edit_pagina" type="submit" class="btn btn-primary">Atualizar</button>
                <?php endif ?>
            </div>
        </div><!-- /col-lg-3 -->
    </form>
</div>

<div class="row"></div>