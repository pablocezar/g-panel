<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-file-o"></i> <span id="active_menu">Todas Páginas</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin')?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Todas Páginas</strong></li>
        </ol>
    </div>
    <div class="col-lg-5">
        <div style="padding-top: 40px"></div>
<!--        <form id="ajaxArtigos" class="form-inline" method="post">
            <input type="text" name="titulo" id="searchArtigos" class="form-control" placeholder="Pesquisar"/>
            <button class="btn btn-warning" id="j_ajaxArtigos"><i class="fa fa-search"></i> Pesquisar</button>
            <img src="<?= $this->PathTheme ?>/assets/images/ajax_loader.gif" alt="img-ajax" class="img-ajax-loader" style="width: 40px"/>
        </form>-->
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url() ?>admin/formPagina" title="Nova Página" class="btn btn-primary pull-right"><i class="fa fa-pencil-square-o"></i> Nova Página</a>
    </div>
</div>

<div class="row">
    <div class="col-sm-6"></div><!-- col-md-6 -->
    <div class="col-sm-6"><div class="pull-right"><?= $paginas['links'];?></div></div><!-- col-md-6 -->
</div><!-- row -->
<div class="row">
    <?php foreach ($paginas['paginas']->result() as $linha): ?>
        <div class="col-md-3 col-sm-6 box-list-pages">
            <div class="ibox-content text-center">
                <h2><?= character_limiter($linha->pagina_titulo, 10); ?></h2>
                <div class="m-b-sm">
                    <img class="img-dest-pagina" alt="image" src="<?= ($linha->pagina_imagem_destacada == '') ? $this->PathTheme.'assets/images/img-padrao.jpg' :base_url('') . $linha->pagina_imagem_destacada ?>">
                </div>
                <p class="font-bold"><?= character_limiter($linha->pagina_descricao, 55) ?></p>
                <div class="text-center">
                    <a href="<?= base_url().$linha->pagina_slug ?>" target="_blank" class="btn btn-xs btn-primary">
                        <i class="fa fa-share"></i>
                        Ver
                    </a>
                    <a href="<?= base_url() . 'admin/formPagina/' . $linha->pagina_id ?>" class="btn btn-xs btn-warning">
                        <i class="fa fa-pencil"></i>
                        Editar
                    </a>
                    <a onclick="modalExcluirPagina(<?= $linha->pagina_id ?>)" class="btn btn-xs btn-danger">
                        <i class="fa fa-trash"></i>
                        Excluir
                    </a>
                </div>
            </div>
            <div style="padding: 10px 0"></div>
        </div>
    <?php endforeach; ?>
</div><!-- /row -->