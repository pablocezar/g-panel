<!-- Select2 -->
<link href="<?= $this->PathTheme ?>plugins/select2/select2.css" rel="stylesheet">
<link href="<?= $this->PathTheme ?>plugins/select2/theme.css" rel="stylesheet">
<script src="<?= $this->PathTheme ?>plugins/select2/select2.min.js"></script>

<script src="<?= $this->PathTheme ?>js/popups.js"></script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-envelope-o"></i> <span id="active_menu">Popup's</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Popups</strong></li>
        </ol>
    </div>
    <div class="col-lg-5">

    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url() ?>admin/popups" title="Novo Popup" class="btn btn-primary pull-right"><i class="fa fa-circle"></i> Novo Popup</a>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Popups ( Janelas de notificações )</h5>
            </div>

            <div class="ibox-content">
                <form action="<?= (isset($popup)) ? base_url('control_admin/popups/update/') . $popup->popup_id : base_url('control_admin/popups/cad') ?>" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <a href="#" title="Imagem destacada" data-toggle="modal" data-target="#file_manager">
                                    <img id="preview" src="<?= isset($popup) && $popup->popup_imagem_destacada != '' ? base_url() . 'tim.php?src=' . $popup->popup_imagem_destacada . '&w=460&h=260' : $this->PathTheme . 'assets/images/img-padrao.jpg' ?>" style="max-height:290px" class="img-responsive">
                                    <input id="imagem" value="<?= isset($popup) ? $popup->popup_imagem_destacada : '' ?>" name="popup_imagem_destacada" type="hidden">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="popup_titulo">Titulo</label>
                                <input class="form-control" value="<?= (isset($popup)) ? $popup->popup_titulo : '' ?>" name="popup_titulo" id="regiao_nome" placeholder="Titulo" type="text">
                            </div>
                            <div class="form-group">
                                <label for="popup_link">Link</label>
                                <input class="form-control" value="<?= (isset($popup)) ? $popup->popup_link : '' ?>" name="popup_link" id="regiao_nome" placeholder="Link" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="popup_id_paginas">Exibir em:</label>
                        <?php
                        if (isset($popup)):
                            $arrPopupPag = explode(',', $popup->popup_id_paginas);
                        endif;

//                        var_dump($todos_popups);
                        //remove as paginas que já tem popups da lista para que não seja cadastrados mais de 1 popup por página!
                        $SelectPages = $todas_paginas;

                        foreach ($SelectPages as $key => $value):
                            foreach ($todos_popups as $linha):
                                $popup_id_paginas = explode(',', $linha->popup_id_paginas);
                                if (in_array($value->pagina_id, $popup_id_paginas)):
                                    unset($SelectPages[$key]);
                                endif;
                            endforeach;
                        endforeach;
                        ?>
                        <select name="popup_id_paginas[]" class="select2" multiple="multiple" required>
                            <?php if (isset($popup)): ?>
                                <option value="0" <?= (isset($popup) && in_array(0, $arrPopupPag) ? 'selected' : '') ?>>Home Page</option>
                                <?php foreach ($todas_paginas as $linha): ?>
                                    <option value="<?= $linha->pagina_id ?>" <?= (isset($arrPopupPag) && in_array($linha->pagina_id, $arrPopupPag) ? 'selected' : '') ?>><?= $linha->pagina_titulo ?></option>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <option value="0" <?= (isset($popup) && in_array(0, $arrPopupPag) ? 'selected' : '') ?>>Home Page</option>
                                <?php foreach ($SelectPages as $linha): ?>
                                    <option value="<?= $linha->pagina_id ?>" <?= (isset($arrPopupPag) && in_array($linha->pagina_id, $arrPopupPag) ? 'selected' : '') ?>><?= $linha->pagina_titulo ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="popup_link">Conteúdo</label>
                        <textarea class="form-control" id="popup_link" name="popup_conteudo" rows="10" cols="40"><?= (isset($popup)) ? $popup->popup_conteudo : '' ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="popup_status">Status</label>
                        <select class="form-control" name="popup_status">
                            <option value="0" <?= (isset($popup) && $popup->popup_status != 0) ? '' : 'selected' ?>>Inativo</option>
                            <option value="1" <?= (isset($popup) && $popup->popup_status != 1) ? '' : 'selected' ?>>Ativo</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div><!-- col-md-6 -->

    <div class="col-md-6">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Popups Cadastrados</h5>
            </div>

            <div class="ibox-content">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Imagem</th>
                            <th>Titulo</th>
                            <th>Status</th>
                            <th>Açoes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($todos_popups as $linha): ?>
                            <tr>
                                <td><img src="<?= Thumbnail(140, 80, $linha->popup_imagem_destacada) ?>" alt="<?= $linha->popup_titulo ?>" title="<?= $linha->popup_titulo ?>"/></td>
                                <td><?= $linha->popup_titulo ?></td>
                                <td><?= ($linha->popup_status == 1) ? '<span class="label label-primary">Ativo</span>' : '<span class="label label-warning">Inativo</span>' ?></td>
                                <td>
                                    <a href="<?= base_url('admin/popups/') . $linha->popup_id ?>" title="Editar" data-toggle="tooltip" class="btn btn-warning btn-sm"><span class="fa fa-pencil"></span></a>
                                    <button type="button" onclick="modalExcluirPopup(<?= $linha->popup_id ?>)" title="Excluir" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div><!-- col-md-6 -->

</div><!-- row -->