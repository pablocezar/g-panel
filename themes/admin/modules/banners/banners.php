<script src="<?= $this->PathTheme ?>js/banners.js"></script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-image"></i> Gerenciar Banners</h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong><span id="active_menu">Banners</span></strong></li>
        </ol>
    </div>
</div>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Gerenciar Banners</h5>
    </div>
    <div class="ibox-content">
        <div class="row">
            <form action="<?= (!isset($banner)) ? base_url('control_admin/banners/cadBanner') : base_url('control_admin/banners/editBanner').'/'.$banner->banner_id ?>" method="post">

                <div class="col-md-6">
                    <div class="form-group">
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#file_manager">
                            <span class="glyphicon glyphicon-picture"></span> Imagem
                        </a>
                    </div>
                    <div class="form-group">
                        <img id="preview" src="<?= isset($banner) && $banner->banner_imagem != '' ? base_url() . $banner->banner_imagem : $this->PathTheme . 'assets/images/img-padrao.jpg' ?>" style="max-height:290px" class="img-responsive">
                        <input id="imagem" value="<?= isset($banner) ? $banner->banner_imagem : '' ?>" name="banner_imagem" type="hidden">
                    </div>
                </div><!-- col-md-6 -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="banner_titulo">Título</label>
                        <input value="<?= (isset($banner)) ? $banner->banner_titulo : '' ?>" name="banner_titulo" id="banner_titulo" class="form-control" placeholder="Título"/>
                    </div>
                    <div class="form-group">
                        <label for="banner_link">Link</label>
                        <input value="<?= (isset($banner)) ? $banner->banner_link : '' ?>" name="banner_link" id="banner_link" class="form-control" placeholder="Link"/>
                    </div>
                    <div class="form-group">
                        <label for="banner_descricao">Descrição</label>
                        <textarea name="banner_descricao" id="banner_descricao" rows="5" class="form-control" placeholder="Descrição"><?= (isset($banner)) ? $banner->banner_descricao : '' ?></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </div><!-- col-md-6 -->

            </form>
        </div><!-- row -->
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Gerenciar Banners</h5>
            </div>
            <div class="ibox-content" style="display: block;">
                <?php foreach ($banners as $linha): ?>
                    <div class="col-md-6" style="position: relative; margin-bottom: 10px;">
                        <img class="img-responsive" src="<?= base_url() . $linha->banner_imagem ?>" />
                        <div style="position: absolute; top: 3%; left: 5%;">
                            <a href="<?=base_url('admin/banners').'/'.$linha->banner_id?>" class="btn btn-primary btn-circle btn-lg" type="button"><i class="fa fa-pencil"></i></a>
                            <button onclick="modalExcluirBanner('<?= $linha->banner_id ?>')" class="btn btn-danger btn-circle btn-lg" type="button"><i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                <?php endforeach ?>
                <div style="clear: both"></div>
            </div>
        </div>
    </div>
</div>