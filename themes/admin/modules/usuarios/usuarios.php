<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-users"></i> <span id="active_menu">Usuários</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin')?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Usuários</strong></li>
        </ol>
    </div>
</div>

<div class="col-md-7">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Usuários Cadastrados</h5>
        </div>
        <div class="ibox-content table-responsive">
            <form action="" method="post">
                <!--<button type="submit" title="Excluir" class="btn btn-danger"><span class="fa fa-trash"></span> Excluir Marcados</button>-->
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Foto</th>
                            <th>Nome</th>
                            <th>Nivel</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($usuarios as $linha): ?>
                            <tr class="gradeX">
                                <td>
                    <center><img class="img-circle" width="50" height="50" src="<?= ($linha->usuario_foto != '') ? base_url().$linha->usuario_foto : base_url('themes/admin/assets/images/user-default.png') ?>" title="<?= $linha->usuario_nome ?>" alt="<?= $linha->usuario_nome ?>"/></center>
                        </td>
                        <td><?= $linha->usuario_nome ?></td>
                        <td>
                            <?php
                            if($linha->usuario_nivel_acesso == 1):
                                echo 'Editor';
                            elseif($linha->usuario_nivel_acesso == 2):
                                echo 'Administrador';
                            elseif($linha->usuario_nivel_acesso == 3):
                                echo 'Master';
                            endif;
                            ?>
                        </td>
                        <td>
                            <a href="<?= base_url('admin/usuarios/') . $linha->usuario_id ?>" title="Editar" data-toggle="tooltip" class="btn btn-warning btn-sm"><span class="fa fa-pencil"></span></a>
                            <button type="button" onclick="modalExcluirUsuario(<?= $linha->usuario_id ?>)" title="Excluir" class="btn btn-danger btn-sm"><span class="fa fa-trash"></span></button>
                        </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Foto</th>
                            <th>Nome</th>
                            <th>Nivel</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
            </form>
        </div>
    </div>
</div><!-- col-md-8 -->

<div class="col-md-5">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5><?= (isset($usuario)) ? 'Editar Usuário' : 'Cadastrar Usuário' ?></h5>
        </div>
        <div class="ibox-content">
            <form id="formUsuario" action="<?= (!isset($usuario)) ? base_url('control_admin/usuarios/cadastraUsuario') : base_url('control_admin/usuarios/editaUsuario/') . $usuario->usuario_id ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input name="usuario_nome" id="usuario_nome" value="<?= isset($usuario) ? $usuario->usuario_nome : '' ?>" class="form-control" placeholder="Nome do usuário" type="text" required/>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input name="usuario_email" id="usuario_email" value="<?= isset($usuario) ? $usuario->usuario_email : '' ?>" class="form-control" placeholder="usuario@exemplo.com" type="email" required/>
                </div>
                <div class="form-group">
                    <label for="nivel_acesso">Nivel de Acesso</label>
                    <select name="usuario_nivel_acesso" class="form-control" required>
                            <option value="1" <?= (isset($usuario)  && $usuario->usuario_nivel_acesso == 1) ? 'selected' : ''?>>Editor</option>
                        <option value="2" <?= (isset($usuario)  && $usuario->usuario_nivel_acesso == 2) ? 'selected' : ''?>>Administrador</option>
                        <option value="3" <?= (isset($usuario)  && $usuario->usuario_nivel_acesso == 3) ? 'selected' : ''?>>Master</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="senha">Senha</label>
                    <input name="usuario_senha" id="usuario_senha" value="" class="form-control" placeholder="Senha" type="password" <?= (isset($usuario) ? '' : 'required') ?>/>
                </div>
                <div class="form-group">
                    <label for="confima_senha">Confirme a Senha</label>
                    <input id="confima_senha" value="" class="form-control" placeholder="Confirme a Senha" type="password" <?= (isset($usuario) ? '' : 'required') ?>/>
                    <span style="color: #ff0033; display: none" id="msgValidaSenha">Senhas não Conferem!</span>
                </div>
                <div class="form-group">
                    <button type="submit" id="btnUsuarios" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div><!-- col-md-4 -->
<div class="row"></div>
