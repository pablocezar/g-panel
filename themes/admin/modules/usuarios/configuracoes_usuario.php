<div class="row">
    <div class="col-md-6">
        <div class="box box-default">
            <div class="box-header">
                <h3 class="box-title">Editar Informações de <?= $usuario_logado->usuario_nome ?></h3>
            </div><!-- /.box-header -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs pull-right">
                    <li class="pull-left header"><i class="fa fa-th"></i> Editar</li>
                    <li class=""><a href="#tab_tema_admin" data-toggle="tab">Tema Admin</a></li>
                    <li><a href="#tab_senha" data-toggle="tab">Senha</a></li>
                    <li class="active"><a href="#tab_perfil" data-toggle="tab">Dados de Perfil</a></li>
                    <li class="dropdown">
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_perfil">
                        <form action="<?= base_url('control_admin/usuarios/editar_dados_usuario_logado') ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group editar-imagem-usuario">
                                <input id="usuario_foto" name="usuario_foto" type="file">
                                <br>
                                <?php if ($usuario_logado->usuario_foto == ""): ?>
                                    <img id="prev_logo" src="<?= base_url() ?>themes/admin/dist/img/user-default.png" class="img-circle" alt="foto usuário" title="foto" />
                                <?php else: ?>
                                    <img id="prev_logo" src="<?= base_url('themes/admin/uploads') . '/' . $usuario_logado->usuario_foto ?>" class="img-circle" alt="foto usuário" title="foto" />
                                <?php endif; ?>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label>Nome</label>
                                <input value="<?= $usuario_logado->usuario_nome ?>" name="usuario_nome" id="usuario_nome" type="text" class="form-control" placeholder="Nome do usuário" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input value="<?= $usuario_logado->usuario_email ?>" name="usuario_email" id="usuario_email" type="email" class="form-control" placeholder="usuario@email.com" required>
                            </div>
                            <button type="submit" class="btn btn-success">Salvar</button>
                        </form>
                    </div><!-- /.tab_perfil -->
                    <div class="tab-pane" id="tab_senha">
                        <form action="<?= base_url('control_admin/usuarios/alterar_senha_usuario_logado') . '/' . $usuario_logado->usuario_id ?>" method="post">
                            <div class="form-group">
                                <label>Senha</label> <small style="color: #f00">mínimo de 6 caracteres</small>
                                <input value="" name="usuario_senha" pattern=".{6,}" id="usuario_senha" type="password" class="form-control" placeholder="Senha" required>
                            </div>
                            <div class="form-group">
                                <label>Confirme a senha</label>
                                <input value="" name="confirma_senha" pattern=".{6,}" id="confirma_senha" type="password" class="form-control" placeholder="Confirme a senha" required>
                            </div>
                            <button type="submit" class="btn btn-success">Salvar</button>
                        </form>
                    </div><!-- /.tab_senha -->
                    <div class="tab-pane" id="tab_tema_admin">
                        <form action="<?= base_url('control_admin/usuarios/alterar_tema_admin') ?>" method="post">
                            <div class="form-group">
                                <label>Selecione o Tema</label>
                                <select name="usuario_tema_admin" id="usuario_tema_admin" class="form-control">
                                    <option value="<?= $usuario_logado->usuario_tema_admin?>" selected>Escolha um Tema</option>
                                    <option value="skin-blue">Blue</option>
                                    <option value="skin-blue-light">Blue Light</option>
                                    <option value="skin-black">Black</option>
                                    <option value="skin-black-light">Black Light</option>
                                    <option value="skin-purple">Purple</option>
                                    <option value="skin-purple-light">Purple Light</option>
                                    <option value="skin-green">Green</option>
                                    <option value="skin-green-light">Green Light</option>
                                    <option value="skin-red">Red</option>
                                    <option value="skin-red-light">Red Light</option>
                                    <option value="skin-yellow">Yelow</option>
                                    <option value="skin-yellow-light">Yellow Light</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Menu</label>
                                <select name="estilo_sidebar" id="usuario_estilo_admin" class="form-control">
                                    <option value="">Expandido</option>
                                    <option value="sidebar-collapse">Recolhido</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Salvar</button>
                        </form>
                    </div><!-- /.tab_tema_admin -->
                </div><!-- /.tab-content -->
            </div>
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->

<div class="row">
    <div class="col-md-6">
        <!-- Custom Tabs -->
        <!-- nav-tabs-custom -->
    </div><!-- /.col -->


</div><!-- /.row -->

<!--Modal Deleta Post-->
<div class="modal" id="confirma_deletar_usuario">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Deletar Usuário</h4>
            </div>
            <div class="modal-body">
                <p>Deseja realmente deletar esta usuário?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Não</button>
                <a id="deletar_usuario" href="#"  class="btn btn-primary">Sim</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>