<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-picture-o"></i> <span id="active_menu">Nova Galeria</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin')?>" title="Dashboard">Dashboard</a></li>
            <li><a href="<?= base_url('admin/galerias')?>" title="Páginas">Galerias</a></li>
            <li class="active"><strong>Nova Galeria</strong></li>
        </ol>
    </div>
</div>

<form action="<?= base_url('control_admin/galerias/criarGaleria') ?>" method="post">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <a href="#" class="btn btn-info btn-galeria btn-block" data-toggle="modal" data-target="#file_manager">
                    <span class="glyphicon glyphicon-folder-open"></span>
                </a>
                <input id="imagem" type="hidden" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input name="galeria_titulo" id="galeria_titulo" type="text" class="form-control" placeholder="Titulo da galeria" required>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <button class="btn btn-primary" type="submit"><span class="fa fa-check"></span> Salvar</button>
            </div>
        </div>
    </div>
    <div class="row" id="j_prenche_galerias">
        <div class="container" style="text-align: center">
            <hr>
            <h1 style="color:#ccc; font-size: 4.5em">Selecione a pasta! <i class="fa fa-folder"></i></h1>
        </div>
    </div>
</form>

