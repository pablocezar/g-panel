<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-3">
        <h2><i class="fa fa-picture-o"></i> <span id="active_menu">Galerias</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Galerias</strong></li>
        </ol>
    </div>
    <div class="col-lg-5">
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url() ?>admin/novaGaleria" title="Nova Galeria" class="btn btn-primary pull-right"><i class="fa fa-picture-o"></i> Nova Galeria</a>
    </div>
</div>
<div class="row">
    <?php foreach ($todas_galerias as $linha): ?>
        <div class="col-md-3 col-sm-6 box-list-pages">
            <div class="ibox-content text-center">
                <h2><?= character_limiter($linha->galeria_titulo, 10) ?></h2>
                <div class="m-b-sm">
                    <div id="carousel-<?= $linha->galeria_id ?>" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php $i = 1; ?>
                            <?php foreach ($imagens_galerias as $img): ?>
                                <?php if ($img->imagem_id_galeria == $linha->galeria_id):  ?>
                                <div class="item <?= ($i == 1) ? 'active' : '' ?>">
                                    <img class="img-galeria" alt="image" src="<?= Thumbnail(300, 200, 'medias/uploads/'.$img->imagem_imagem)?>">
                                </div>
                                <?php $i++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-<?= $linha->galeria_id ?>" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Próximo</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-<?= $linha->galeria_id ?>" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Anterior</span>
                        </a>
                    </div>
                </div>
                <p class="font-bold">Criada em <?= date('d/m/Y', strtotime($linha->galeria_data_criacao))?></p>
                <?php
                foreach ($imagens_galerias as $imgGaleria):
                    if($imgGaleria->imagem_id_galeria == $linha->galeria_id):
                        $numImages[] = $imgGaleria;
                    endif;
                endforeach;
                ?>
                <span class="label label-info"><?= count($numImages)?> Imagens</span><hr>
                <?php unset($numImages);?>
                <div class="text-center">
                    <a href="<?= base_url('admin/editarGaleria/').$linha->galeria_id?>" class="btn btn-xs btn-warning">
                        <i class="fa fa-pencil"></i>
                        Editar
                    </a>
                    <a onclick="modalExcluirGaleria(<?= $linha->galeria_id ?>)" class="btn btn-xs btn-danger">
                        <i class="fa fa-trash"></i>
                        Excluir
                    </a>
                </div>
            </div>
            <div style="padding: 10px 0"></div>
        </div>
    <?php endforeach; ?>
</div>
