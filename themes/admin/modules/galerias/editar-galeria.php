<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-4">
        <h2><i class="fa fa-picture-o"></i> <span id="active_menu">Editar Galeria</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li><a href="<?= base_url('admin/galerias') ?>" title="Páginas">Galerias</a></li>
            <li class="active"><strong>Editar Galeria</strong></li>
        </ol>
    </div>
    <div class="col-lg-4">
    </div>
    <div class="col-lg-4">
        <div style="padding-top: 40px"></div>
        <a href="<?= base_url() ?>admin/novaGaleria" title="Nova Galeria" class="btn btn-primary pull-right"><i class="fa fa-picture-o"></i> Nova Galeria</a>
    </div>
</div>

<form action="<?= base_url('control_admin/galerias/editarGaleria/'.$galeria->galeria_id) ?>" method="post">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <a href="#" class="btn btn-info btn-galeria btn-block" data-toggle="modal" data-target="#file_manager">
                    <span class="glyphicon glyphicon-folder-open"></span>
                </a>
                <input id="imagem" type="hidden" class="form-control">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input name="galeria_titulo" id="galeria_titulo" value="<?= $galeria->galeria_titulo ?>" type="text" class="form-control" placeholder="Titulo da galeria" required>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <button class="btn btn-primary" type="submit"><span class="fa fa-check"></span> Salvar</button>
            </div>
        </div>
    </div>
    <div class="row" id="j_prenche_galerias">
        <?php $i = 0; ?>
        <?php foreach ($imagens_galeria as $linha):?>
        <div class="col-md-4 col-sm-6 box-list-images-galeria">
            <a onclick="modalRemoveImagemGaleria(<?= $linha->imagem_id ?>)" title="Remover imagem da galeria" class="btn btn-danger btn-delete-img-galeria"><i class="fa fa-trash-o"></i></a>
            <div class="ibox-content text-center">
                <div class="m-b-sm">
                    <img class="img-galeria" alt="image" data-url="<?= base_url('medias/uploads/').$linha->imagem_imagem?>" src="<?= base_url('medias/uploads/').$linha->imagem_imagem?>">
                    <input name="imagem[<?= $i ?>][imagem_id]" value="<?= $linha->imagem_id ?>" type="hidden">
                </div>
                <div class="form-group">
                    <input class="form-control" name="imagem[<?= $i ?>][imagem_titulo]" value="<?= $linha->imagem_titulo ?>" placeholder="Titulo" type="text">
                </div>
                <div class="form-group">
                    <input class="form-control" name="imagem[<?= $i ?>][imagem_link]"  value="<?= $linha->imagem_link ?>" placeholder="Link" type="text">
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="imagem[<?= $i ?>][imagem_descricao]" placeholder="Descrição"><?= $linha->imagem_descricao ?></textarea>
                </div>
            </div>
            <div style="padding: 10px 0"></div>
        </div>
        <?php $i ++; ?>
        <?php endforeach;?>
    </div>
</form>
<div class="row" style="padding: 20px 0"></div>

