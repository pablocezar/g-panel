<!-- DataTables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?= $this->PathTheme ?>js/config-datatable.js"></script>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-envelope-o"></i> <span id="active_menu">Emails</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin') ?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Emails</strong></li>
        </ol>
    </div>
</div>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Emails recebidos através do formulário do site</h5> <span class="label label-primary">GNS</span>
    </div>
    <div class="ibox-content table-responsive">
        <table id="datatable" class="table table-bordered table-hover dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Assunto</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($todos_emails as $linha): ?>
                    <tr>
                        <td><?= $linha->id; ?></td>
                        <td><?= date("d/m/Y H:i", strtotime($linha->data)) ?></td>
                        <td><?= $linha->nome; ?> <?= ($linha->aberto == 0) ? '<span class="label label-primary">Novo</span>' : '' ?></td>
                        <td><?= $linha->email; ?></td>
                        <td><?= $linha->assunto; ?></td>
                        <td width="90px">
                            <a onclick="visualizar_email('<?= $linha->id ?>')" href="#" title="Vizualizar" data-toggle="tooltip" class="btn btn-info"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a onclick="modalExcluirEmail('<?= $linha->id; ?>')" data-toggle="tooltip" href="#" title="Deletar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Assunto</th>
                    <th>Ações</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-envelope"></i> Email</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-mail">
                        <thead>
                            <tr>
                                <th colspan="4"><small id="EmNome">Nome</small></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="1"><small id="EmTelefone">Telefone</small></td>
                                <td colspan="2"><small id="EmEmail">Email</small></td>
                            </tr>
                            <tr>
                                <td colspan="4"><small><b>Nos Achou Através de:</b> </small><small id="EmNosAchou"></small></td>
                            </tr>
                            <tr>
                                <td colspan="4"><small id="EmMensagem">Mensagem</small></td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="DownAnexo"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>