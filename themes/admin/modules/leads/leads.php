<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-envelope-o"></i> <span id="active_menu">Leads</span></h2>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('admin')?>" title="Dashboard">Dashboard</a></li>
            <li class="active"><strong>Leads</strong></li>
        </ol>
    </div>
</div>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Leads cadastrados pelo formulário do site</h5>
    </div>
    <div class="ibox-content">
        <table id="datatable" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($todos_leads as $linha): ?>
                    <tr>
                        <td><?= date("d/m/Y H:i", strtotime($linha->data_cadastro)) ?></td>
                        <td><?= $linha->nome; ?></td>
                        <td><?= $linha->email; ?></td>
                        <td>
                            <a onclick="modalExcluirLead('<?= $linha->id; ?>')" data-toggle="tooltip" href="#" title="Deletar" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Ações</th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-envelope"></i> Email</h4>
      </div>
      <div class="modal-body">
          <table class="table table-mail">
              <thead>
                  <tr>
                      <th colspan="4"><small id="EmNome">Nome</small></th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td colspan="1"><small id="EmTelefone">Telefone</small></td>
                      <td colspan="2"><small id="EmEmail">Email</small></td>
                  </tr>
                  <tr>
                      <td colspan="4"><small><b>Nos Achou Através de:</b> </small><small id="EmNosAchou"></small></td>
                  </tr>
                  <tr>
                      <td colspan="4"><small id="EmMensagem">Mensagem</small></td>
                  </tr>
              </tbody>
          </table>
          <div id="DownAnexo"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>