<!--Modal Deleta Gerenciador de Arquivos-->
<div class="modal" id="file_manager">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button id="fechar_modal" type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Gerenciador de Arquivos</h4>
            </div>
            <iframe src="<?= $this->PathTheme?>plugins/filemanager/dialog.php?type=1&field_id=imagem" style="width: 100%; height: 500px; border: none">;
            </iframe>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>