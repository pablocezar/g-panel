<div class="footer">
    <div class="pull-right">
        Versão <strong>2.2</strong>
    </div>
    <div>
        <strong>Copyright</strong> Genesys Tech &copy; 2015-<?= date('Y') ?>
    </div>
</div>
</div>
</div>

<div class="modal fade" id="modalDialogo" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmar Ação</h4>
            </div>
            <div class="modal-body">
                <h3>Tem certeza que gostaria de realizar esta ação?</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <a href="#" class="btn btn-primary" id="link_modal_dialog">Sim, continuar!</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    var base_url = '<?= base_url() ?>';
</script>


<!-- Menu -->
<script src="<?= $this->PathTheme ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<!-- Toastr -->
<script src="<?= $this->PathTheme ?>assets/js/plugins/toastr/toastr.min.js"></script>
<!-- Ajax -->
<script src="<?= $this->PathTheme ?>js/inspinia.js"></script>
<script src="<?= $this->PathTheme ?>ajax.js"></script>

<?php if ($this->session->flashdata('sucesso')): ?>
    <script>
    $(document).ready(function() {
        setTimeout(function() {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            toastr.success('Sucesso!', '<?= $this->session->flashdata('sucesso'); ?>');

        }, 100);
    });
    </script>
<?php endif; ?>
<script src="<?= $this->PathTheme ?>pbl.js"></script>
</body>
</html>