<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
    </div>
    <ul class="nav navbar-top-links navbar-right">
        <li>
            <span class="m-r-sm text-muted welcome-message">Bem Vindo ao G Painel <?= $this->ConfigSysten->config_nome_empresa ?></span>
        </li>
        <li>
            <a href="#">
                <button type="button" class="btn btn-primary" data-container="body" data-toggle="popover" data-placement="bottom" data-content="No momento há <?= count($this->OnlineVisits) ?> <?= (count($this->OnlineVisits) > 1 ? 'visitantes' : 'visitante') ?> navegando no site." data-original-title="" title="" aria-describedby="">
                    <b><?= count($this->OnlineVisits)?></b>
                    <?= (count($this->OnlineVisits) > 1 ? 'Visitantes' : 'Visitante') ?> Online
                </button>
            </a>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope"></i>  <span class="label label-warning"><?= count($this->Emails) ?></span>
            </a>
            <ul class="dropdown-menu dropdown-messages">
                <?php foreach ($this->Emails as $linha): ?>
                    <li>
                        <div class="dropdown-messages-box">
                            <a href="profile.html" class="pull-left">
                                <i class="fa fa-envelope" style="font-size: 2em"></i>
                            </a>
                            <div class="media-body">
                                <?php if ($linha->anexo != NULL): ?>
                                    <small class="pull-right"><i class="fa fa-paperclip"></i> Anexo</small>
                                <?php endif ?>
                                <strong><?= $linha->nome ?></strong> 
                                <p> <?= $linha->assunto ?></p>
                                <strong><?= $linha->email ?></strong>. <br>
                                <small class="text-muted">Recebido em <?= date('d/m/Y H:i', strtotime($linha->data)); ?></small>
                            </div>
                        </div>
                    </li>
                    <li class="divider"></li>
                <?php endforeach; ?>
                <li>
                    <div class="text-center link-block">
                        <a href="<?= base_url('admin/emails') ?>">
                            <i class="fa fa-envelope"></i> <strong>Ler Todos os Emails</strong>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                <i class="fa fa-bell"></i>  <span class="label label-primary"><?= count($proxPostagens) ?></span>
            </a>
            <ul class="dropdown-menu dropdown-alerts">
                <?php if (count($proxPostagens) > 0): ?>
                    <center><h5>Próximas Postagens</h5></center>
                    <?php foreach ($proxPostagens as $linha): ?>
                        <li>
                            <a href="<?= base_url('admin/FormArtigo') . '/' . $linha->pagina_id ?>">
                                <div>
                                    <i class="fa fa-clock-o fa-fw"></i> <?= character_limiter($linha->pagina_titulo, 10) ?>
                                    <span class="pull-right text-muted small"><span class="label label-primary"><?= date('d/m/Y', strtotime($linha->pagina_postagem_programada)) ?></span></span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                    <?php endforeach; ?>
                <?php else: ?>
                    <center><h5>Nenhuma Postagem para realizar nos próximos 3 dias!</h5></center>
                    <li class="divider"></li>
                <?php endif; ?>
                <li>
                    <div class="text-center link-block">
                        <a href="<?= base_url('admin/artigosAgendados') ?>">
                            <strong>Mostrar Todos Artigos Agendados</strong>
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
        <li>
            <a href="<?= base_url('login/logout') ?>">
                <i class="fa fa-sign-out"></i> Sair
            </a>
        </li>
    </ul>

</nav>