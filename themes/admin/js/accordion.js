$(function () {


    $(".select2").select2();

    if ($('.sortableAcc').length > 0) {
        $('.sortableAcc').nestedSortable({
            handle: 'div',
            items: 'li',
            toleranceElement: '> div',
            maxLevels: 1,
            stop: function () {
                ajaxOrderLineAcc()
            }
        });
    }

});

function ajaxOrderLineAcc() {
    var data_to_send = $("ol.sortableAcc").nestedSortable("serialize");
    $.ajax({
        type: "POST",
        dataType: "text",
        url: base_url + "control_admin/accordions/order",
        data: data_to_send,
        beforeSend: function () {
            $(".loading").css({display: 'flex'});
            $(".loading").fadeIn('slow');
        },
        success: function () {
            $(".loading").fadeOut('slow');
        },
        error: function (res) {
            console.clear();
            console.log(res);
            alert('erro');
        }
    });
}

function getAjaxAccID(id) {
    var url = base_url + "control_admin/accordions/getAjaxByID/" + id;
    $("#form_edit_accordion_line").attr('action', base_url + "control_admin/accordions/edit/" + id);
    $.getJSON(url, function (data) {
        $(".j_accordion_titulo").val(data.accordion_titulo);
        tinyMCE.activeEditor.setContent(data.accordion_conteudo);
    });
    $(".modal-editar-accordion").modal('show');
    return false;

}