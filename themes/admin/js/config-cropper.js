$(function(){
    //Cropp image
    var $imageCropp = $(".image-crop > img");
    $($imageCropp).cropper({
        aspectRatio: 1.168,
//        width: 825,
        height: 464.0625
    });

    $("#formUsuario").submit(function() {
//        window.open($image.cropper("getDataURL"));
        $("#cropp_image").val($image.cropper("getDataURL"));
    });
    
    var $image = $(".image-crop > img");
    $($image).cropper({
        aspectRatio: 1.618,
        preview: ".img-preview",
        done: function(data) {
            // Output the result data for cropping image.
        }
    });

    var $inputImage = $("#usuario_foto");
    if (window.FileReader) {
        $inputImage.change(function() {
            var fileReader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {
                fileReader.readAsDataURL(file);
                fileReader.onload = function() {
                    $inputImage.val("");
                    $image.cropper("reset", true).cropper("replace", this.result);
                };
            } else {
                showMessage("Please choose an image file.");
            }
        });
    } else {
        $inputImage.addClass("hide");
    }
});