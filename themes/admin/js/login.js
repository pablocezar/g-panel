$(function () {
    $("#formLogin").submit(function () {
        var url = $("#formLogin").attr('action');
        $.ajax({
            url: url,
            type: 'POST',
            data: $("#formLogin").serialize(),
            dataType: 'Json',
            beforeSend: function () {
                $(".btn_logar").html("<i class=\"fa fa-spinner fa-spin fa-1x fa-fw\"></i> Verificando...");
            },
            success: function (resposta) {
                $(document).pblNotification({
                    type: resposta[0],
                    message: resposta[1],
                    timeShow: 5000
                });

                $(".btn_logar").html("<i class=\"fa fa-lock\"></i> Login");
                if (resposta[2]) {
                    setTimeout(function () {
                        window.location = base_url + "admin";
                    }, 5000);
                }
            },
            error: function (resposta) {
                $(document).pblNotification({
                    type: "danger",
                    message: "Falha ao logar, tente mais tarde!",
                    timeShow: 5000
                });
            }
        });
        return false;
    });
});