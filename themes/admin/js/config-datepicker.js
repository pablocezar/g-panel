$.datetimepicker.setLocale('pt-BR');
$(function () {
    jQuery('#datetimepicker').datetimepicker({
        format: 'd/m/Y H:i'
    });
    jQuery('.datepickerDashboard').datetimepicker({
        format: 'd/m/Y',
        timepicker:false
    });
});