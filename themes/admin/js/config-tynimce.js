$(function () {
    tinymce.init({
        selector: "textarea#editable",
        language: "pt_BR",
        height: 400,
        theme: 'modern',
        relative_urls: false,
        remove_script_host: false,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | backcolor | template',
        templates: [
            {title: '2 Colunas', description: 'Apresenta o conteúdo da página em duas colunas', content: '<div class="container"><div class="col-6"><h3>Exemplo coluna 1</h3><img src="' + base_url + 'themes/admin/assets/images/img-padrao.jpg" title="Imagem" alt="Imagem"/></div><div class="col-6"><h3>Exemplo coluna 2</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis, tortor a laoreet gravida, ligula sem laoreet erat, ac dictum eros mauris non sapien. Praesent eu diam mauris. Vestibulum accumsan lacus ut neque congue, sed sollicitudin nisl egestas. Ut feugiat tristique diam, at posuere tortor malesuada non. Maecenas accumsan urna dolor, in dictum eros ultrices nec.</p></div></div>'},
            {title: '3 Colunas', description: 'Apresenta o conteúdo da página em três colunas', content: '<div class="container"><div class="col-4"><h3>Exemplo coluna 1</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis, tortor a laoreet gravida, ligula sem laoreet erat, ac dictum eros mauris non sapien. Praesent eu diam mauris. Vestibulum accumsan lacus ut neque congue, sed sollicitudin nisl egestas. Ut feugiat tristique diam, at posuere tortor malesuada non. Maecenas accumsan urna dolor, in dictum eros ultrices nec.</p></div><div class="col-4"><h3>Exemplo coluna 2</h3><img src="' + base_url + 'themes/admin/assets/images/img-padrao.jpg" title="Imagem" alt="Imagem"/></div><div class="col-4"><h3>Exemplo coluna 3</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis, tortor a laoreet gravida, ligula sem laoreet erat, ac dictum eros mauris non sapien. Praesent eu diam mauris. Vestibulum accumsan lacus ut neque congue, sed sollicitudin nisl egestas. Ut feugiat tristique diam, at posuere tortor malesuada non. Maecenas accumsan urna dolor, in dictum eros ultrices nec.</p></div></div>'},
            {title: '4 Colunas', description: 'Apresenta o conteúdo da página em quatro colunas', content: '<div class="container"><div class="col-4"><h3>Exemplo coluna 1</h3><img src="' + base_url + 'themes/admin/assets/images/img-padrao.jpg" title="Imagem" alt="Imagem"/></div><div class="col-4"><h3>Exemplo coluna 2</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis, tortor a laoreet gravida, ligula sem laoreet erat, ac dictum eros mauris non sapien. Praesent eu diam mauris. Vestibulum accumsan lacus ut neque congue, sed sollicitudin nisl egestas. Ut feugiat tristique diam, at posuere tortor malesuada non. Maecenas accumsan urna dolor, in dictum eros ultrices nec.</p></div><div class="col-4"><h3>Exemplo coluna 3</h3><img src="' + base_url + 'themes/admin/assets/images/img-padrao.jpg" title="Imagem" alt="Imagem"/></div><div class="col-4"><h3>Exemplo coluna 4</h3><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris iaculis, tortor a laoreet gravida, ligula sem laoreet erat, ac dictum eros mauris non sapien. Praesent eu diam mauris. Vestibulum accumsan lacus ut neque congue, sed sollicitudin nisl egestas. Ut feugiat tristique diam, at posuere tortor malesuada non. Maecenas accumsan urna dolor, in dictum eros ultrices nec.</p></div></div>'},
        ],
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ],
        formats: {
            alignleft: {selector: 'img', styles: {float: 'left', margin: '0 10px 0 0'}},
            alignright: {selector: 'img', styles: {float: 'right', margin: '0 0 0 10px'}}
        },
        image_advtab: true,
        image_title: true,
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Lightbox', value: 'lightbox'}
        ],
        external_filemanager_path: base_url + "themes/admin/plugins/filemanager/dialog.php?type=2",
        filemanager_title: "Gerenciador de Arquivos",
        external_plugins: {"filemanager": base_url + "themes/admin/plugins/filemanager/plugin.min.js"}
    });
});