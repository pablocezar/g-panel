$(function () {

    $('.sortableP').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
        maxLevels: 2
    });
    
    $('.sortableR').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
        maxLevels: 1
    });

    $(".select2").select2();

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green'
    });

    $("form").hide();
    $("#link_pagina").show();

    $('#radio_cat').on('ifClicked', function () {
        $("#link_pagina").hide();
        $("#link_url").hide();

        $("#link_categoria").show();
    });

    $('#radio_link').on('ifClicked', function () {
        $("#link_pagina").hide();
        $("#link_categoria").hide();

        $("#link_url").show();
    });

    $('#radio_pagina').on('ifClicked', function () {
        $("#link_categoria").hide();
        $("#link_url").hide();

        $("#link_pagina").show();
    });

    $("#select_pagina").change(function () {
        $("#txt_titulo_pagina").val($("#select_pagina :selected").text());
    });

    $("#select_cat").change(function () {
        $("#txt_titulo_cat").val($("#select_cat :selected").text());
    });

});