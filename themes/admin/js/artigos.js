$(function() {

    //Colorpicker
    if ($('.j_color_picker').length > 0) {

        $('.j_color_picker').colorpicker();
        var divStyle = $('#catColor_amostra')[0].style;
        $('.j_color_picker').colorpicker({
            color: divStyle.backgroundColor
        }).on('changeColor', function(ev) {
            divStyle.backgroundColor = ev.color.toHex();
        });

    } else {
        
        $(".select2").select2();
        
    }

});