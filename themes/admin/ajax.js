//Alimenta Tabela de Caegorias
//$("#resultAjaxCategorias").html();

//Retorna pesquisa de artigos no admin
$("#j_ajaxArtigos").click(function () {
    var search = $("#searchArtigos").val();
    if ($("#searchArtigos").val() === "") {
        $(".j_resultArtigos").html('');
        $(".box-list-pages").show();
    } else {
        getAjaxArtigos(search);
    }
    return false;
});

//Verifica a se havera duplicidade de urls
$(".j_pagina_titulo").focusout(function () {
    checkUrl('paginas/ajaxCheckPagina');
});
$(".j_pagina_slug").keyup(function () {
    checkUrl('paginas/ajaxCheckPagina');
});
$(".j_categoria_titulo").keyup(function () {
    checkUrl('categorias/ajaxCheckCategoria');
});

function checkUrl(check) {
    var slug = $("#checkSlug").val();
    var url = base_url + 'control_admin/' + check + '/' + slug;
    var btnCheck = $("#btnCheck");
    $.getJSON(url, function (data) {
        if (data.urlDuplicada && btnCheck.length > 0) {
            $("#msg_pg_duplicada").fadeIn('slow');
            $("#btnCheck").attr('disabled', '');
        } else {
            $("#msg_pg_duplicada").fadeOut('slow');
            $("#btnCheck").attr('disabled', false);
        }
    });
}

//Cadastra Categorias
$("#form_categorias").submit(function () {
    var url = $(this).attr('action');
    var form = $(this);
    formAjax(form, url);
    return false;
});

function getResultsAjaxCategorias() {
    var url = base_url + 'control_admin/categorias/ajaxReturnCategorias';
    $.getJSON(url, function (data) {
        var tableContent = '';
        $.each(data, function (key, value) {
            tableContent += "<tr>";
            tableContent += "<td style='background:" + value.categoria_cor + "; color:#fff'>" + value.categoria_cor + "</td>";
            tableContent += "<td><img class='img-responsive' src='" + base_url + 'tim.php?src=' + value.categoria_imagem_destacada + '&w=400&h=230' + "' /></td>";
            tableContent += "<td>" + value.categoria_titulo + "</td>";
            tableContent += "<td>" + value.categoria_descricao + "</td>";
            tableContent += "<td>";
            tableContent += '<a href="' + base_url + 'admin/categoriasArtigos/' + value.categoria_id + '" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> </a> ';
            tableContent += '<a href="#" onclick="modalExcluirCategoria(' + value.categoria_id + ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> </a>';
            tableContent += "</td>";
            tableContent += "</tr>";
        });
        $("#resultAjaxCategorias").html(tableContent);
    });
}
;
getResultsAjaxCategorias();

function getSelectAjaxCategorias() {
    var url = base_url + 'control_admin/categorias/ajaxReturnCategorias';
    $.getJSON(url, function (data) {
        var tableContent = '';
        $.each(data, function (key, value) {
            tableContent += "<tr>";
            tableContent += "<td style='background:" + value.cor + "; color:#fff'>" + value.cor + "</td>";
            tableContent += "<td>" + value.titulo + "</td>";
            tableContent += "<td>";
            tableContent += '<a href="' + base_url + 'admin/categoriasArtigos/' + value.id + '" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i> </a> ';
            tableContent += '<a href="#" onclick="modalExcluirCategoria(' + value.id + ')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> </a>';
            tableContent += "</td>";
            tableContent += "</tr>";
        });
        $("#resultAjaxCategorias").html(tableContent);
    });
}
;


function getAjaxArtigos(search) {
    var url = base_url + 'control_admin/paginas/ajaxGetArtigo/' + search;
    var data = $("#ajaxArtigos").serialize();
    $.ajax({
        url: url,
        type: 'Post',
        dataType: 'Json',
        data: data,
        beforeSend: function () {
            $(".img-ajax-loader").fadeIn('slow');
        },
        success: function (result) {
            var ContentSerach = "";
            $.each(result, function (key, value) {
                ContentSerach += "<div class=\"col-md-3 col-sm-6\">";
                ContentSerach += "<div class=\"ibox-content text-center\">";
                ContentSerach += "<h2>" + value.pagina_titulo.substr(0, 15) + '...' + "</h2>";
                ContentSerach += "<div class=\"m-b-sm\">";
                ContentSerach += "<img class=\"img-dest-pagina\" alt=\"image\" src=\"" + base_url + value.pagina_imagem_destacada + "\">";
                ContentSerach += "</div>";
                ContentSerach += "<p class=\"font-bold\">" + value.pagina_descricao.substr(0, 55) + '...' + "</p>";
                ContentSerach += "<div class=\"text-center\">";
                ContentSerach += "<a href=" + base_url + value.pagina_slug + " target=\"_blank\" class=\"btn btn-xs btn-primary\"> <i class=\"fa fa-share\"></i> Ver </a> ";
                ContentSerach += "<a href=" + base_url + "admin/formArtigo/" + value.pagina_id + " class=\"btn btn-xs btn-warning\"> <i class=\"fa fa-pencil\"></i> Editar </a> ";
                ContentSerach += "<a onclick=\"modalExcluirPagina(" + value.pagina_id + ")\" class=\"btn btn-xs btn-danger\"> <i class=\"fa fa-trash\"></i> Excluir </a> ";
                ContentSerach += "</div>";
                ContentSerach += "</div>";
                ContentSerach += "<div style=\"padding: 10px 0\"></div>";
                ContentSerach += "</div>";
                ContentSerach += "</div>";
            });
            $(".box-list-pages").hide();
            $(".j_resultArtigos").html(ContentSerach);
            $(".img-ajax-loader").fadeOut('slow');
        },
        error: function () {
            alert("nada encontrado!");
        }
    });
}
;

//Filtro Analytics
$("#filtro_analytics").submit(function () {
    var url = base_url + 'admin/getAjaxGA';
    var data = $(this).serialize();
    $.ajax({
        url: url,
        type: 'Post',
        dataType: 'Json',
        data: data,
        beforeSend: function () {
            $(".img-ajax-loader").fadeIn('slow');
        },
        success: function (data) {
            var table = '';
            $.each(data.paginas, function (key, value) {
                table += "<tr>";
                table += "<td>" + value.getpageTitle + "</td>";
                table += "<td>" + value.getpageviews + "</td>";
                table += "<td>" + value.getorganicSearches + "</td>";
                var v = (value.getbounceRate <= 50) ? 'up' : 'down';
                table += "<td class='text-navy'><i class='fa fa-level-" + v + "'></i> " + value.getbounceRate + "% </td>";
                table += "</tr>";
                $("#j_ajax_result_pages").html(table);
            });

            $("#getVisits").html(data.trafegos.visits);
            $("#getPageviews").html(data.trafegos.pageviews);
            $("#getnewUsers").html(data.trafegos.newUsers);
            $("#getorganicSearches").html(data.trafegos.organicSearches);
            $(".img-ajax-loader").fadeOut('slow');
        },
        error: function () {
            alert("erro");
        }
    });

    return false;
});

//Ordena Página em menu
$("#btn_edit_pagina").on('click', function () {
    var data_to_send = $("#menu_sortable").sortable("serialize");
    $.ajax({
        type: "POST",
        dataType: "text",
        url: base_url + "control_admin/paginas/ordenaPagina",
        data: data_to_send,
        success: function () {
            $("#form_pagina").submit();
        }
    });
});

//Ordena Menu Principal
$("#btn_edit_menu_principal").on('click', function () {
    var data_to_send = $("#menu_sortable").nestedSortable("serialize");
    $.ajax({
        type: "POST",
        dataType: "text",
        url: base_url + "control_admin/menus/ordenaMenu",
        data: data_to_send,
        success: function () {
            $(document).ready(function () {
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.success('Sucesso!', 'Menu Ordenado com sucesso!');
                }, 100);
            });
        }
    });
});


//Ordena Menu Principal
$("#btn_edit_menu_rodape").on('click', function () {
    var data_to_send = $("#menu_sortable_rodape").nestedSortable("serialize");
    $.ajax({
        type: "POST",
        dataType: "text",
        url: base_url + "control_admin/menus/ordenaMenu",
        data: data_to_send,
        success: function () {
            $(document).ready(function () {
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 4000
                    };
                    toastr.success('Sucesso!', 'Menu Ordenado com sucesso!');
                }, 100);
            });
        }
    });
});


$(".btnRemoveItemMenu").on('click', function () {
    var remove = $(this).closest('li');
    $.ajax({
        type: "POST",
        dataType: "text",
        url: $(this).attr("href"),
        success: function () {
            remove.fadeOut('slow');
        }
    });
    return false;
});

/**
 * Função de cadastro de formulário via ajax
 * Basta informar o form e a url a ser execultada.
 */
function formAjax(form, url) {
    var data = form.serialize();
    $.ajax({
        url: url,
        type: 'Post',
        dataType: 'HTML',
        data: data,
        beforeSend: function () {
            $(".img-ajax-loader").fadeIn('slow');
        },
        success: function () {
            $(".img-ajax-loader").fadeOut('slow');
            form[0].reset()
            getResultsAjaxCategorias();
            msgSuccess();

        },
        error: function () {
            $(".img-ajax-loader").fadeOut('slow');
            alert("erro");
        }
    });
}

function msgSuccess() {
    setTimeout(function () {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };
        toastr.success('Sucesso!', 'Informações salvas com sucesso');
    }, 1300);
}

function lerEmail(id) {
    $.ajax({
        url: base_url + "control_admin/emails/marcaComoLido/" + id,
        type: 'Post',
        dataType: 'HTML',
        success: function () {

        },
        error: function () {
            alert("erro ao selecionar email");
        }
    });
}


//***********************************//
//********** Ajax Galeria **********//
//*********************************//

function Ajaxgaleria(caminho) {

    $("#j_prenche_galerias input").length > 0 ? updateGallry(caminho) : createGallery(caminho);

    return false;
}

function createGallery(caminho) {
    var pasta = caminho.replace(base_url + 'medias/uploads/', '');
    var url = base_url + 'control_admin/galerias/ajaxPathGaleria/' + pasta;
    $.getJSON(url, function (data) {
        var result = "";
        $.each(data, function (key, value) {
            var imagem = value.replace(base_url + 'medias/uploads/', '');
            result += '<div class="col-md-4 col-sm-6 box-list-pages">';
            result += '<div class="ibox-content text-center">';
            result += '<div class="m-b-sm">';
            result += '<img class="img-galeria" alt="image" src="' + value + '">';
            result += '<input name="imagem[' + key + '][imagem_imagem]" value="' + imagem + '" type="hidden">';
            result += '</div>';
            result += '<div class="form-group">';
            result += '<input class="form-control" name="imagem[' + key + '][imagem_titulo]" placeholder="Titulo" type="text">';
            result += '</div>';
            result += '<div class="form-group">';
            result += '<input class="form-control" name="imagem[' + key + '][imagem_link]" placeholder="Link" type="text">';
            result += '</div>';
            result += '<div class="form-group">';
            result += '<textarea class="form-control" name="imagem[' + key + '][imagem_descricao]" placeholder="Descrição"></textarea>';
            result += '</div>';
            result += '</div>';
            result += '<div style="padding: 10px 0"></div>';
            result += '</div>';
        });
        $("#j_prenche_galerias").html(result);
    });
}

function updateGallry() {
//    var pasta = caminho.replace(base_url + 'medias/uploads/', '');
//    var url = base_url + 'control_admin/galerias/ajaxPathGaleria/' + pasta;

    var result = "";
    var novaImagem = $("#imagem").val();
    var numImage = $(".img-galeria").length;

    result += '<div class="col-md-4 col-sm-6 box-list-pages">';
    result += '<div class="ibox-content text-center">';
    result += '<div class="m-b-sm">';
    result += '<img class="img-galeria" alt="image" src="' + novaImagem + '">';
    result += '<input name="imagem[' + numImage + '][imagem_imagem]" value="' + novaImagem.replace(base_url + 'medias/uploads/', '') + '" type="hidden">';
    result += '</div>';
    result += '<div class="form-group">';
    result += '<input class="form-control" name="imagem[' + numImage + '][imagem_titulo]" placeholder="Titulo" type="text">';
    result += '</div>';
    result += '<div class="form-group">';
    result += '<input class="form-control" name="imagem[' + numImage + '][imagem_link]" placeholder="Link" type="text">';
    result += '</div>';
    result += '<div class="form-group">';
    result += '<textarea class="form-control" name="imagem[' + numImage + '][imagem_descricao]" placeholder="Descrição"></textarea>';
    result += '</div>';
    result += '</div>';
    result += '<div style="padding: 20px 0"></div>';
    result += '</div>';
    $("#j_prenche_galerias").append(result);
    console.log(novaImagem);
}