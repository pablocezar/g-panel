<!DOCTYPE html>
<html lang="pt-BR">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>G Panel - <?= $this->ConfigSysten->config_nome_empresa ?></title>

         <link rel="shortcut icon" href="<?= $this->PathTheme ?>assets/images/favicon.png"/>
        <link href="<?= $this->PathTheme ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= $this->PathTheme ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

        <!-- Toastr style -->
        <link href="<?= $this->PathTheme ?>assets/css/plugins/toastr/toastr.min.css" rel="stylesheet">

        <link href="<?= $this->PathTheme ?>assets/css/animate.css" rel="stylesheet">
        <link href="<?= $this->PathTheme ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= $this->PathTheme ?>pbl.css" rel="stylesheet">
        
        <!-- Scripts Default -->
        <script src="<?= $this->PathTheme ?>assets/js/jquery-2.1.1.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="<?= $this->PathTheme ?>assets/js/bootstrap.min.js"></script>
    </head>

    <body class="<?= $this->User->usuario_tipo_menu ?>">
        <div class="loading">
            <span><i class="fa fa-gear fa-spin"></i></span>
        </div>
        <div id="wrapper">
            <?php include('nav-left.php') ?>

            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php if($this->ConfigSysten->config_manutencao === '1'):?>
                <div class="alert row alert-warning" role="alert" style="background: #F8AC59; color: #fff; border-radius: 0">
                    <b><i class="fa fa-exclamation-circle"></i> Atenção!</b> 
                    O site encontra-se em modo manutenção, então apenas você podera ver as alterações feitas, para torna-lo público desabilite o modo manutenção em configurações!
                </div>
                <?php endif;?>
                <div class="row border-bottom">
                    <?php include('nav-top.php') ?>
                </div>
                

