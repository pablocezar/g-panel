<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" width="60" height="60" src="<?= base_url() . $this->SessionData['usuario_foto'] ?>" />
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?= $this->SessionData['usuario_nome'] ?></strong>
                            </span> 
                            <span class="text-muted text-xs block">
                                <?php
                                if ($this->User->usuario_nivel_acesso == 1):
                                    echo 'Editor';
                                elseif ($this->User->usuario_nivel_acesso == 2):
                                    echo 'Administrador';
                                elseif ($this->User->usuario_nivel_acesso == 3):
                                    echo 'Master User';
                                endif;
                                ?>
                                <b class="caret"></b>
                            </span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="<?= base_url('admin/configUsuario') . '/' . $this->SessionData['usuario_id'] ?>">Perfil</a></li>
                        <li class="divider"></li>
                        <li><a href="<?= base_url('login/logout') ?>">Sair</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    GNS
                </div>
            </li>
            <li class="tooltip-demo">
                <a href="<?= base_url('admin') ?>" data-active-menu="Dashboard">
                    <i class="fa fa-home"></i> <span class="nav-label" data-active-menu="Dashboard">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?= base_url('admin') . '/configuracoes' ?>" data-active-menu="Configurações"><i class="fa fa-gears"></i> <span class="nav-label">Configurações</span></a>
            </li>
            <li>
                <a href="<?= base_url('admin') . '/menus' ?>" data-active-menu="Menus"><i class="fa fa-list-alt"></i> <span class="nav-label">Menus</span></a>
            </li>
            <li class="j_drop">
                <a href="#"><i class="fa fa-files-o"></i> <span class="nav-label">Páginas</span> <span class="label label-warning"><?= $this->TotalPaginas ?></span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?= base_url('admin') . '/formPagina' ?>" data-active-menu="Nova Página">Nova Página</a></li>
                    <li><a href="<?= base_url('admin') . '/paginas' ?>" data-active-menu="Todas Páginas">Todas Páginas</a></li>
                </ul>
            </li>
            <li class="j_drop">
                <a href="#"><i class="fa fa-pencil"></i> <span class="nav-label">Artigos</span> <span class="label label-warning"><?= $this->TotalArtigos ?></span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?= base_url('admin') . '/categoriasArtigos' ?>" data-active-menu="Categorias">Categorias</a></li>
                    <li><a href="<?= base_url('admin') . '/formArtigo' ?>" data-active-menu="Novo Artigo">Novo Artigo</a></li>
                    <li><a href="<?= base_url('admin') . '/artigos' ?>" data-active-menu="Todos Artigos">Todos Artigos</a></li>
                    <li><a href="<?= base_url('admin') . '/artigosAgendados' ?>" data-active-menu="Artigos Agendados">Artigos Agendados</a></li>
                </ul>
            </li>
            <li>
                <a href="<?= base_url('admin') . '/banners' ?>" data-active-menu="Banners"><i class="fa fa-desktop"></i> <span class="nav-label">Banners</span></a>
            </li>
            <li class="j_drop">
                <a href="#"><i class="fa fa-picture-o"></i> <span class="nav-label">Galerias</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?= base_url('admin') . '/novaGaleria' ?>" data-active-menu="Nova Galeria">Nova Galeria</a></li>
                    <li><a href="<?= base_url('admin') . '/galerias' ?>" data-active-menu="Galerias">Todas Galerias</a></li>
                </ul>
            </li>
            <li>
                <a href="<?= base_url('admin/regioes_atendimento') ?>" data-active-menu="Regiões de Atendimento"><i class="fa fa-map-marker"></i> <span class="nav-label">Regiões de Atendimento</span></a>
            </li>
            <li>
                <a href="<?= base_url('admin') . '/accordions' ?>" data-active-menu="Accordions"><i class="fa fa-align-justify"></i> <span class="nav-label">Accordions</span></a>
            </li>
            <li>
                <a href="<?= base_url('admin/popups') ?>" data-active-menu="Popup's"><i class="fa fa-circle"></i> <span class="nav-label">Popup's</span></a>
            </li>
            <li>
                <a href="<?= base_url('admin/leads') ?>" data-active-menu="Leads"><i class="fa fa-users"></i> <span class="nav-label">Leads</span></a>
            </li>
            <?php if ($this->User->usuario_nivel_acesso >= 3): ?>
                <li>
                    <a href="<?= base_url('admin/usuarios') ?>" data-active-menu="Usuários"><i class="fa fa-lock"></i> <span class="nav-label">Usuários</span></a>
                </li>
            <?php endif ?>
            <li>
                <a href="<?= base_url() ?>" target="_blank"><i class="fa fa-share"></i> <span class="nav-label">Ver Site</span></a>
            </li>
        </ul>

    </div>
</nav>