/**
 * <b>PT_notification</b>
 * Exibe mensagens de ações ou notificações simples no site.
 * @param {int} timeShow
 * @param {string} type
 * @param {string} message
 * @returns {notification}
 */
(function ($) {
    $.fn.pblNotification = function (options) {
        
        var settings = $.extend({
            type: 'success',
            message: 'Olá Mundo!',
            timeShow: 3000,
            speed: 500,
            favIcon: '',
            position: 'top-right'
        }, options);
        
        var n = "<div class='pt-notification " + settings.type +' '+ settings.position + "'>"+ settings.favIcon + settings.message + "</div>";

        if ($('.pt-notification').length <= 0) {
            $('body').append(n);
            $('.pt-notification').prepend('<div class="pt-notification-time"></div>');
        }

        $('.pt-notification').animate({
            right: '2%'
        }, {
            complete: function () {
                $('.pt-notification-time').animate({
                    left: '100%'
                }, settings.timeShow, function () {
                    $('.pt-notification').animate({right: '-100%'}, settings.speed, function () {
                        $('.pt-notification').remove();
                    });
                });
            }
        });
        
    };
    
})(jQuery);
