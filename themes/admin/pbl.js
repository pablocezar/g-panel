$(function() {
    // MetsiMenu
    $('#side-menu').metisMenu();
    // minimalize menu
    $('.navbar-minimalize:not(.binded)').addClass("binded").click(function() {
        $("body").toggleClass("mini-navbar");
        SmoothlyMenu();
    });
    

    //Habilita Toltips
//    $('[data-toggle="tooltip"]').tooltip();

    //Ativa menu
    $.each($("#side-menu li"), function() {
        if ($("#active_menu").length > 0 && $("#active_menu").html() === $(this).children('ul li a').attr('data-active-menu')) {
            $(this).parent('ul').parent('.j_drop').addClass("active").children("ul").collapse("toggle");
            $(this).addClass('active');
        }
    });

    $("#confima_senha").keyup(function() {
        var senha = $("#usuario_senha").val();
        var confirmaSenha = $(this).val();
        if (confirmaSenha !== senha) {
            $("#msgValidaSenha").fadeIn('slow');
            $("#btnUsuarios").attr('disabled', 'disabled');
        } else {
            $("#msgValidaSenha").fadeOut('slow');
            $("#btnUsuarios").attr('disabled', false);
        }
    });

});

//CEP AutoComplete
$("#config_cep").focusout(function() {
//Consulta o webservice viacep.com.br/
    $.getJSON("//viacep.com.br/ws/" + $(this).val() + "/json/?callback=?", function(dados) {
        if (!("erro" in dados)) {
            //Atualiza os campos com os valores da consulta.
            $("#config_logradouro").val(dados.logradouro);
            $("#config_bairro").val(dados.bairro);
            $("#config_localidade").val(dados.localidade);
            $("#config_uf").val(dados.uf);
        } //end if.
        else {
            //CEP pesquisado não foi encontrado.
            limpa_formulário_cep();
            alert("CEP não encontrado.");
        }
    });

});


//Criação dos links das páginas
$("#pagina_titulo").keyup(function() {
    var slug = retira_acentos($("#pagina_titulo").val()).toLowerCase().replace(/ /g, "-");
    $("#checkSlug").val(slug);
});
$("#pagina_titulo").focusout(function() {
    var slug = retira_acentos($("#pagina_titulo").val()).toLowerCase().replace(/ /g, "-");
    $("#checkSlug").val(slug);
});
$("#categoria_titulo").keyup(function() {
    var slug = retira_acentos($("#categoria_titulo").val()).toLowerCase().replace(/ /g, "-");
    $("#checkSlug").val(slug);
});

//*********************************************************************//
//************************** Funções *********************************//
function responsive_filemanager_callback(field_id) {
    $("#fechar_modal").trigger('click');
    var imagem = $("#imagem").val();
    $("#preview").attr('src', imagem);
    Ajaxgaleria(imagem);
}

function retira_acentos(palavra) {
    com_acento = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ';
    sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    nova = '';
    for (i = 0; i < palavra.length; i++) {
        if (com_acento.search(palavra.substr(i, 1)) >= 0) {
            nova += sem_acento.substr(com_acento.search(palavra.substr(i, 1)), 1);
        }
        else {
            nova += palavra.substr(i, 1);
        }
    }
    return nova;
}

//Janelas de Dialogo Modal
function modalExcluirPagina(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/paginas/deletaPagina/' + id);
    $("#modalDialogo").modal('show');
    return false;
}
function modalExcluirCategoria(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/categorias/deletaCategoria/' + id);
    $("#modalDialogo").modal('show');
    return false;
}
function modalExcluirUsuario(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/usuarios/deletaUsuario/' + id);
    $("#modalDialogo").modal('show');
    return false;
}
function modalExcluirEmail(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/emails/deletaEmail/' + id);
    $("#modalDialogo").modal('show');
    return false;
}

function modalExcluirLead(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/leads/deletaLead/' + id);
    $("#modalDialogo").modal('show');
    return false;
}
function modalExcluirGaleria(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/galerias/deletarGaleria/' + id);
    $("#modalDialogo").modal('show');
    return false;
}
function modalRemoveImagemGaleria(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/galerias/RemoveImagemGaleria/' + id);
    $("#modalDialogo").modal('show');
    return false;
}

function modalExcluirRegioesAtend(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/regioesAtendimento/del/' + id);
    $("#modalDialogo").modal('show');
    return false;
}

function modalExcluirAccordions(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/accordions/del/' + id);
    $("#modalDialogo").modal('show');
    return false;
}

function modalExcluirPopup(id) {
    $("#link_modal_dialog").attr('href', base_url + 'control_admin/popups/del/' + id);
    $("#modalDialogo").modal('show');
    return false;
}

function visualizar_email(id) {
    lerEmail(id);
    var url = base_url + "control_admin/emails/getEmailJson/" + id;
    $.getJSON(url, function(data) {
        console.clear();
        console.log(data.anexo);
        $("#EmNome").html(data.nome);
        $("#EmTelefone").html(data.telefone);
        $("#EmEmail").html(data.email);
        $("#EmNosAchou").html(data.como_nos_achou);
        $("#EmMensagem").html(data.mensagem);
        if (data.anexo != "") {
            $("#DownAnexo").html("<a class='btn btn-info btn-sm' href='" + base_url + "emails_anexo/" + data.anexo + "' target='_blank' title='Vizualizar Anexo'>Vizualizar Anexo<a/>");
        } else {
            $("#DownAnexo").html('');
        }
        ;
    });
    $("#modalEmail").modal('show');
    return false;
}