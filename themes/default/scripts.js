$(function () {

    $('#fade').list_ticker({
        speed: 6000,
        effect: 'fade'
    });

    //Mantém a integridade responsiva e estética das tabelas adicionadas pela Área administrativa.
    if ($('table').length > 0) {
        $('table').wrapAll("<div class='table-responsive'>");
        $('table').addClass('table table-hover table-bordered');
    }

    //****Garante todos os titles em imagens desde que seja informado um alt****//
    //****Também adiciona a classe img-responsive, garantido o layout responsivo****//
    $(".content-page img").each(function () {
        $(this).attr("title", $(this).attr("alt"));
        $(this).addClass('img-responsive');
    }),
            //****Ativa item de Menu****//
            $(".navbar-nav li a").each(function () {
        var url = window.location.href;
        if ($(this).attr("href") === url) {
            $(this).parent('li').addClass("active");
        }
        ;
    });

    $(".dropdown").hover(function () {
        $(this).children(".dropdown-menu").fadeToggle(300);
    });

    //****Recolhe menu Categorias****//
    $(".menu-categorias li").click(function () {
//    $(".dropdown-menu-cat").click(function () {
        $(this).find('i').toggleClass('fa-angle-down');
        $(this).children(".sub-menu-categorias").toggle("slow");
    });

    $(".fancybox").fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic'
    });

//    $.cookie("visit", 1, {expires: 10});
    if ($(".popup").length > 0) {
        $(".popup").fancybox().trigger('click');
    }
    $('.popup-footer a').click(function () {
        window.location.href = $(this).attr('href');
        return false;
    });

    if ($(".lightbox").length > 0) {
        $.each($(".lightbox"), function () {
            var urlLink = $(this).attr('src');
            var imgTitle = $(this).attr("title");
            $(this).wrap("<a href='" + urlLink + "' class='thumbnail fancybox' title='" + imgTitle + "'>");
        });
    }


    $("#share").jsSocials({
//            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        shareIn: "popup",
        shares: ["twitter", {share: "facebook", label: "Compartilhar"}, "googleplus", "whatsapp"]

    });

    //Menu fixo topo
    $(window).scroll(function () {
        if ($(this).scrollTop() > $('.header-site').outerHeight()) {
            $('body').css('padding-top', $('.navbar-default').outerHeight());
            $('.navbar-default').addClass('menu-fixo');
            $('.j_backtop').fadeIn('slow');
        } else {
            $('body').css('padding-top', '0');
            $('.navbar-default').removeClass('menu-fixo');
            $('.j_backtop').fadeOut('slow');
        }
    });

    $(".j_backtop").hide();
    $(".j_backtop").click(function () {
        $("html, body").animate({scrollTop: 0}, 1e3)
    });

    //**** Form Contato ****//
    $("#ajax_form").submit(function () {
        $.ajax({
            url: "control_admin/emails/formEmail",
            type: 'POST',
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $(".j_img_loader").fadeIn('slow');
            },
            success: function () {
                $(".msg-email-success").fadeIn('slow');
                $(".j_img_loader").fadeOut('slow');
                $("input").val('');
                $("textarea").val('');
                $("select").val('');
//                $("#file_name").html('');
            },
            error: function () {
                $(".j_img_loader").fadeOut('slow');
                $(".msg-email-error").fadeIn('slow');
            }
        });
        return false;
    });

    $(".load-page").fadeOut('slow');
});