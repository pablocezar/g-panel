<section>
    <header class="header-page" style="background: url(<?= $pagina->pagina_imagem_destacada ?>);">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2 class="text-center"><?= $pagina->pagina_titulo ?></h2>
            <p class="tagline text-center"><?= $pagina->pagina_descricao ?></p>
            <?= $bread_crumb ?>
        </div>
    </header>
    <div class="container content-page">
        <div class="col-md-8 col_left">
            <article class="artigo">
                <div class="img-dest-artigo">
                    <?php if ($pagina->pagina_id_categoria != 3): ?>
                        <a class="btn btn-success btn-post-call-action" href="<?= base_url('contato') ?>" title="Solicite Proposta"><i class="fa fa-envelope"></i> Solicite Proposta</a>
                    <?php endif ?>
                    <img class="img-responsive" src="<?= Thumbnail(900, 480, $pagina->pagina_imagem_destacada) ?>" alt="<?= $pagina->pagina_titulo ?>" title="<?= $pagina->pagina_titulo ?>"/>
                </div>
                <div class="row">
                    <?= (count($galeria) > 0) ? $galeria : '' ?>
                </div>
                <div style="clear: both"></div>
                <div id="share"></div>
                <?= $pagina->pagina_conteudo ?>
                <a class="btn btn-success" href="<?= base_url('contato') ?>" title="Solicite Proposta"><i class="fa fa-envelope"></i> Solicite Proposta </a>

                <div class="hidden">
                    <div itemprop="review" itemscope itemtype="http://schema.org/Review">
                        <div itemprop="author" itemscope itemtype="http://schema.org/Person">
                            <span itemprop="name">Avaliação de Clientes</span>
                        </div>
                        <span itemprop='itemReviewed'><?= $pagina->pagina_titulo ?></span>
                        <meta itemprop="datePublished" content="2017-05-13T14:30:02+00:00">
                        <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                            <span itemprop="ratingValue">5</span>
                        </div>
                    </div>  
                </div>
            </article>


            <h4><?= ($pagina->categoria_id == 1) ? 'Mais Carros em Estoque' : 'Relacionados' ?></h4><hr>
            <div class="row">
                <?php
                $where = "FIND_IN_SET('" . $pagina->pagina_id_categoria . "', pagina_id_categoria)";
                $relacionados = $this->db->order_by('pagina_data_criacao', "RANDOM")->where($where)->where('pagina_id !=', $pagina->pagina_id)->where('pagina_postagem_programada', NULL)->limit(6)->get('paginas')->result();
                ?>
                <?php foreach ($relacionados as $linha): ?>
                    <div class="col-md-4 col-sm-4 artigos-relacionados">
                        <article>
                            <a title="<?= $linha->pagina_titulo ?>" href="<?= base_url() . $linha->pagina_slug ?>"><img class="img-responsive img-relacionados" src="<?= Thumbnail(600, 380, $linha->pagina_imagem_destacada) ?>" alt="<?= $linha->pagina_titulo ?>" title="<?= $linha->pagina_titulo ?>"/></a>
                            <header class="title-default">
                                <h5><a title="<?= $linha->pagina_titulo ?>" href="<?= base_url() . '/' . $linha->pagina_slug ?>"><?= character_limiter($linha->pagina_titulo, 25) ?></a></h5>
                            </header><!-- title_default --> 
                            <p class="tagline"><small><?= character_limiter(strip_tags($linha->pagina_descricao), 35) ?></small></p>
                        </article>
                    </div>
                <?php endforeach; ?>
            </div>
        </div><!-- col_left -->

        <div class="col-md-4 col-sm-12 col_right">
            <?php include 'sidebar.php'; ?>
        </div><!-- col_right -->
    </div><!-- container -->

</section>