<ul id="fade" class="slide-home">
    <?php foreach ($banners as $linha): ?>
        <li style="background-image: url(<?= base_url() . $linha->banner_imagem ?>)">
            <div class="container">
                <p class="title"><?= $linha->banner_titulo ?></p>
                <p class="desc"><?= $linha->banner_descricao ?></p>
                <?php if($linha->banner_link != ''): ?>
                <a class="btn btn-success btn-slide-home" href="<?= $linha->banner_link ?>" title="<?= $linha->banner_descricao ?>">Nosso Certificado</a>
                <?php else: ?>
                <a class="btn btn-success btn-slide-home" href="<?= base_url('contato') ?>" title="Entre em Contato">Entre em Contato</a>
                <?php endif; ?>
            </div>
        </li>
    <?php endforeach; ?>
</ul>

<section class="container-fluid section-produtos">
    <div class="container">
        <header>
            <h2 class="title-default"><?= $config->config_nome_empresa ?> Suspendisse eu ligula</h2>
            <p class="tagline">Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est.</p>
        </header>

        <?php foreach ($paginas_destaque as $linha): ?>
            <article class="col-md-4 box-produtos">
                <header>
                    <h4 class="title-secont"><?= $linha->pagina_titulo ?></h4>
                </header>
                <div class="box-produtos-content">
                    <a class="masc" href="<?= base_url() . $linha->pagina_slug ?>" title="<?= $linha->pagina_titulo ?>">
                        <i class="fa fa-link"></i> Saiba mais
                    </a>
                    <img class="img-responsive" src="<?= Thumbnail(600, 350, $linha->pagina_imagem_destacada) ?>" alt="<?= $linha->pagina_titulo ?>"/>
                </div>
                <div style="text-align: center; padding: 10px;"><p><?= $linha->pagina_descricao ?></p></div>
            </article><!-- col-md-4 -->
        <?php endforeach; ?>

    </div>
</section>

<section class="section-sobre-home">
    <div class="masc-black"></div>
    <div class="container">
        <header class="text-center">
            <h2>Pellentesque egestas <?= $config->config_nome_empresa ?></h2>
        </header>

        <article>
            <header>
                <h3 class="title-secont">Duis vel nibh at velit scelerisque suscipit.</h3>
            </header>
            <p>Duis vel nibh at velit scelerisque suscipit. Suspendisse enim turpis, dictum sed, iaculis a, condimentum nec, nisi.</p>
            <div  class="text-center">
                <a class="btn btn-warning" href="<?= base_url('cr-exercito-brr-blindagens.pdf')?>" target="_blank" title="Nullam accumsan lorem in">Nullam accumsan lorem in</a>
            </div>
        </article>
    </div>
</section>


<section class="container-fluid section-eventos">
    <div class="container">
        <header>
            <h2 class="title-default">Lorem ipsum dolor sit amet</h2>
            <p class="tagline">consectetur adipiscing elit. Etiam non diam euismod!</p>
        </header>
        <?php
        $estoque = $this->db->order_by('pagina_data_criacao', "DESC")->where('pagina_id_categoria', 1)->limit(4)->get('paginas')->result();
        ?>
        <?php if (count($estoque) == 0): ?>
            <b>No momento há nada para apresentar!</b>
        <?php else: ?>
            <div class="row">
                <?php foreach ($estoque as $linha): ?>
                    <article class="col-md-3 box-produtos">
                        <div class="box-produtos-content">
                            <a class="masc" href="<?= base_url() . $linha->pagina_slug ?>" title="<?= $linha->pagina_titulo ?>">
                                <i class="fa fa-link"></i> Saiba mais
                            </a>
                            <img class="img-responsive" src="<?= Thumbnail(600, 350, $linha->pagina_imagem_destacada) ?>" alt="<?= $linha->pagina_titulo ?>"/>
                        </div>
                        <header>
                            <h3 class="title-default font-reduz"><?= $linha->pagina_titulo ?></h3>
                        </header>
                    </article><!-- col-md-4 -->
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

    </div>
</section>