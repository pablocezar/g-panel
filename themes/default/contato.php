<section class="section-contato">
    <header class="header-page">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2><?= $pagina->pagina_titulo ?></h2>
            <?= $bread_crumb ?>
        </div>
    </header>
    <div class="container">
        <div class="col-md-6">
            <h3>Preecha o formulário abaixo:</h3>
            <form id="ajax_form" action="" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-icons">
                            <label for="nome">Nome</label>
                            <i class="fa fa-user"></i>
                            <input name="nome" id="nome" class="form-control" placeholder="Nome" type="text" required/>
                        </div>
                    </div><!-- col-md-6 -->
                    <div class="col-md-6">
                        <div class="form-group form-icons">
                            <label for="email">Email</label>
                            <i class="fa fa-envelope"></i>
                            <input name="email" id="email" class="form-control" placeholder="Email" type="email" required/>
                        </div>
                    </div><!-- col-md-6 -->
                </div><!-- row -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group form-icons">
                            <label for="telefone">Telefone</label>
                            <i class="fa fa-phone"></i>
                            <input name="telefone" id="telefone" class="form-control" placeholder="(00) 0000-000" type="tel" required/>
                        </div>
                    </div><!-- col-md-6 -->
                    <div class="col-md-6">
                        <div class="form-group form-icons">
                            <label for="como_nos_achou">Como nos Achou?</label>
                            <i class="fa fa-question"></i>
                            <select name="como_nos_achou" id="como_nos_achou" class="form-control" required>
                                <option value="">Selecione</option>
                                <option value="Google">Google</option>
                                <option value="Indicação">Indicação</option>
                                <option value="Facebook">Facebook</option>
                                <option value="Outros Meios">Outros Meios</option>
                            </select>
                        </div>
                    </div><!-- col-md-6 -->
                </div><!-- row -->

                <div class="form-group form-icons">
                    <label for="assunto">Assunto</label>
                    <i class="fa fa-check"></i>
                    <input name="assunto" id="assunto" class="form-control" placeholder="Assunto" type="text" required/>
                </div>
                <div class="form-group form-icons">
                    <label for="mensagem">Mensagem</label>
                    <i class="fa fa-comment"></i>
                    <textarea name="mensagem" rows="8" id="mensagem" class="form-control" placeholder="Mensagem"></textarea>
                </div>
                <!--                <div class="form-group">
                                    <label for="anexo" class="btn btn-default"><i class="fa fa-paperclip"></i> Anexo</label>
                                    <span id="file_name"></span>
                                    <input name="anexo" id="anexo" class="hidden" type="file">
                                    <hr>
                                </div>-->
                <div class="form-group">
                    <button type="submit" name="formEmail" class="btn btn-success"><i class="fa fa-paper-plane"></i> Enviar</button>
                    <img class="oculto j_img_loader" width="35" src="<?= $theme ?>/images/loader.gif" alt="loader" title="Enviando email..."/>
                    <div class="msg-email-success"><i class="glyphicon glyphicon-thumbs-up"></i> Seu email foi enviado com sucesso! Obrigado.</div>
                    <div class="msg-email-error"><i class="glyphicon glyphicon-thumbs-down"></i> Erro ao enviar email, por favor tente novamente mais tarde!</div>
                </div>
            </form>
        </div><!-- col-md-6 -->
        <div class="col-md-6">
            <h3>Informações de Contato</h3>
            <p><?= $pagina->pagina_conteudo ?></p>
            <?php if ($config->config_telefone != ''): ?>
                <p><i class="fa fa-phone"></i> <a title="Clique e ligue" rel="nofollow" href="tel:+55<?= str_replace(array('(', ')', ' ', '-'), '', $config->config_telefone) ?>"><?= $config->config_telefone ?></a></p>
            <?php endif ?>
            <?php if ($config->config_telefone_2 != ''): ?>
                <p><i class="fa fa-phone"></i> <a title="Clique e ligue" rel="nofollow" href="tel:+55<?= str_replace(array('(', ')', ' ', '-'), '', $config->config_telefone_2) ?>"><?= $config->config_telefone_2 ?></a></p>
            <?php endif ?>
            <p><i class="fa fa-envelope"></i> <a title="Entrar em contato" rel="nofollow" href="mailto:<?= $config->config_email ?>"><?= $config->config_email ?></a></p>
            <?php if ($config->config_email_2 != ''): ?>
            <p><small>Para assuntos administrativos:</small></p>
            <p><i class="fa fa-envelope"></i> <a title="Entrar em contato" rel="nofollow" href="mailto:<?= $config->config_email_2 ?>"><?= $config->config_email_2 ?></a></p>
            <?php endif ?>
            <hr>
            <p><?= $config->config_logradouro ?>, <?= $config->config_numero ?>, <?= $config->config_complemento ?> - <?= $config->config_localidade ?>, <?= $config->config_uf ?></p>
            <p>CEP: <?= $config->config_cep ?></p>
            <p><?= ($config->config_horario_atendimento != '') ? '<i class="fa fa-clock-o"></i> ' . $config->config_horario_atendimento : '' ?></p>
        </div><!-- col-md-6 -->
    </div>
    
    <div class="mapa">
        <?= $config->config_cod_mapa_google ?>
    </div>
</section>