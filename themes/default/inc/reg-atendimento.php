<div>
    <!-- Nav tabs -->
    <nav>
        <h1 class="hidden">Regiões</h1>
        <ul class="nav nav-tabs" role="tablist">
            <?php $i = 0 ?>
            <?php foreach ($regioes_atend as $linha): ?>
                <li role="presentation" class="<?= ($i == 0 ? 'active' : '') ?>"><a href="#<?= $linha->regiao_id ?>" aria-controls="<?= $linha->regiao_id ?>" role="tab" data-toggle="tab"><?= $linha->regiao_nome ?></a></li>
            <?php $i++; ?>
            <?php endforeach; ?>
        </ul>
    </nav>

    <!-- Tab panes -->
    <div class="tab-content multiplicador">

        <?php $i = 0 ?>
        <?php foreach ($regioes_atend as $linha): ?>

            <div role="tabpanel" class="tab-pane <?= ($i == 0 ? 'active' : '') ?>" id="<?= $linha->regiao_id ?>">
                <ul>
                    <?php
                    $localidades = explode(',', $linha->regiao_localidades);
                    ?>
                    <?php foreach ($localidades as $local): ?>
                        <li><strong><?= $local ?></strong></li>
                    <?php endforeach; ?>
                </ul>
            </div>

            <?php $i++; ?>
        <?php endforeach; ?>

    </div>
</div><br>