<section>
    <header class="header-page">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2><?= $pagina->pagina_titulo ?></h2>
            <p class="tagline" style="color:#fff">Pesquisa interna.</p>
        </div>
    </header>
    <div class="container-fluid">
        <div class="container content-page">
            <p>Você pesquisou por <b><?= $this->FormPost['search'] ?></b> e sua consulta retornou <b><?= count($search); ?></b> resuldados</p>
            <hr>
            <?php if (count($search) > 0): ?>

                <?php foreach ($search as $linha): ?>
                    <article class="col-md-3 box-produtos">
                        <div class="box-produtos-content">
                            <a class="masc" href="<?= base_url() . $linha->pagina_slug ?>" title="<?= $linha->pagina_titulo ?>"style="color: #fff">
                                <i class="fa fa-link"></i> Saiba mais
                            </a>
                            <img class="img-responsive" src="<?= Thumbnail(600, 350, $linha->pagina_imagem_destacada) ?>" alt="<?= $linha->pagina_titulo ?>"/>
                        </div>
                        <header>
                            <h3 class="title-default font-reduz"><?= $linha->pagina_titulo ?></h3>
                        </header>
                    </article><!-- col-md-4 -->
                <?php endforeach; ?>

            <?php else: ?>
                <div class="text-center">
                    <h3>Gostaria de entrar em contato conosco?</h3>
                    <a href="<?= base_url('contato') ?>" title="Entrar em Contato" class="btn btn-success">Entrar em Contato</a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>