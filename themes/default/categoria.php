<section>
    <header class="header-page" style="background: url(<?= $pagina->pagina_imagem_destacada ?>);">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2><?= $categoria->categoria_titulo ?></h2>
            <?= $bread_crumb ?>
        </div>
    </header>
    <div class="container">
        <div class="col-md-8 col-left">
            <div class="desc-categorias">
                <p><?= $categoria->categoria_descricao ?></p>
                <hr>
            </div>
            <div class="row">
                <?php foreach ($artigos_paginados['paginas']->result() as $linha): ?>
                    <article class="col-md-6 box-produtos">
                        <div class="box-produtos-content">
                            <a class="masc" href="<?= base_url() . $linha->pagina_slug ?>" title="<?= $linha->pagina_titulo ?>">
                                <i class="fa fa-link"></i> Saiba mais
                            </a>
                            <img class="img-responsive" src="<?= Thumbnail(600, 350, $linha->pagina_imagem_destacada) ?>" alt="<?= $linha->pagina_titulo ?>"/>
                        </div>
                        <header>
                            <h3 class="title-default font-reduz"><?= $linha->pagina_titulo ?></h3>
                        </header>
                    </article><!-- col-md-4 -->
                <?php endforeach; ?>
            </div>
            <div style="clear: both"></div>
            <?= $artigos_paginados["links"] ?>
        </div><!-- col_left -->

        <div class="col-md-4 col-right">
            <?php include 'sidebar.php'; ?>
        </div><!-- col_right -->
    </div><!-- container -->

</section>