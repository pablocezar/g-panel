<article>
    <header class="header-page">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2><?= $pagina->pagina_titulo ?></h2>
            <p class="tagline" style="color:#fff">A página que você esta procurando não existe!</p>
        </div>
    </header>
    <div class="container-fluid">
        <div class="container content-page">
            <center>
                <h1>Opss... Nada encontrado</h1>
                <p>Parece que o que você procura não existe aqui ou você digitou algo errado!</p>
                <div class="col-md-2"></div><!-- col-md-4 -->
                <div class="col-md-8">
                    <form class="form-inline form-404" action="<?= base_url('search')?>" method="post">
                        <div class="form-group">
                            <input name="search" class="form-control" placeholder="Digite aqui o que você procura" type="text"/>
                            <button class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div><!-- col-md-4 -->
                <div class="col-md-2"></div><!-- col-md-4 -->
            </center>
        </div>
    </div>
</article>