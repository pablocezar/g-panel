<article>
    <header class="header-page" style="background: url(<?= $pagina->pagina_imagem_destacada ?>);">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2 class="text-center"><?= $pagina->pagina_titulo ?></h2>
            <p class="tagline text-center"><?= $pagina->pagina_descricao ?></p>
            <?= $bread_crumb ?>
        </div>
    </header>
    <div class="container-fluid">
        <div class="container content-page">
            <?= $pagina->pagina_conteudo ?>
            <?= (count($galeria) > 0) ? $galeria : '' ?>
        </div>
    </div>
</article>