<?php include 'compacta-html.php'; ?>
<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#" itemscope itemtype="https://schema.org/WebSite">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?= (!isset($pagina->pagina_slug)) ? $title : $title . ' - ' . $config->config_nome_empresa ?></title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?= $theme ?>/images/favicon.png">
        <?= $metasSeo ?>

        <!-- Bootstrap -->
        <link href="<?= $theme ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Fancybox -->
        <link href="<?= $theme ?>/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">

        <!-- Social -->
        <link rel="stylesheet" href="<?= $theme ?>/plugins/jssocials/jssocials.css" />
        <link rel="stylesheet" href="<?= $theme ?>/plugins/jssocials/jssocials-theme-minima.css" />

        <!-- Style -->
        <link href="<?= $theme ?>/style.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <?php
        if (!preg_match('/localhost/', base_url())):
            echo $config->config_cod_analytics;
        endif;
        ?>
    </head>
    <body>

        <div class="load-page"></div>

        <header class="container-fluid header-site">
            <div class="container">
                <div class="btn-tel-mobile"><?= LinkPhone($config->config_telefone, '<i class="fa fa-phone"></i>') ?></div>
                <div itemscope itemtype="http://schema.org/Organization">
                    <span class="hidden" itemprop="name"><?= $config->config_nome_empresa ?></span>
                    <h1 class="hidden"><?= $pagina->pagina_titulo ?></h1>
                </div><!-- schema -->
                <nav class="navbar navbar-default">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu_principal" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div itemscope itemtype="http://schema.org/Organization">
                                <a class="navbar-brand" href="<?= base_url() ?>" rel="home" itemprop="url">
                                    <?php if ($config->config_logo != ''): ?>
                                        <img class="img-responsive logo" itemprop="logo" src="<?= base_url($config->config_logo) ?>" alt="logo" title="<?= $config->config_nome_empresa ?>"/>
                                    <?php else: ?>
                                        <img class="img-responsive logo" itemprop="logo" src="<?= $theme.'/images/logo.png' ?>" alt="logo" title="<?= $config->config_nome_empresa ?>"/>
                                    <?php endif; ?>
                                </a>
                            </div><!-- schema -->
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="menu_principal">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a href="<?= base_url() ?>" title="<?= $config->config_nome_empresa ?>">Home</a></li>
                                <?= $menu_principal ?>
                                <li class="btn btn-warning btn-tel"><?= LinkPhone($config->config_telefone, '<i class="fa fa-phone"></i>') ?></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </div><!-- container -->
        </header>

        <main>