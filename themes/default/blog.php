<section>
    <header class="header-page">
        <div class="filter-white"></div><!-- filter-white -->
        <div class="container">
            <h2><?= $pagina->pagina_titulo ?></h2>
            <?= $bread_crumb ?>
        </div>
    </header>
    <div class="container">
        <div class="col-md-8 col_left">
            <?php foreach ($artigos_paginados['paginas']->result() as $linha): ?>
                <article class="artigo">
                    <a title="<?= $linha->pagina_titulo ?>" href="<?= base_url($linha->pagina_slug) ?>">
                        <img class="img-responsive img-dest-artigo" src="<?= base_url() . $linha->pagina_imagem_destacada ?>" alt="<?= $linha->pagina_titulo ?>" title="<?= $linha->pagina_titulo ?>" />
                    </a>
                    <header class="title-default">
                        <h2><a title="<?= $linha->pagina_titulo ?>" href="<?= base_url($linha->pagina_slug) ?>"><?= $linha->pagina_titulo ?></a></h2>
                    </header><!-- title_default --> 
                    <div class="resume-post">
                        <?= word_limiter(strip_tags($linha->pagina_conteudo), 70); ?>
                    </div>
                    <br>
                    <a href="<?= base_url($linha->pagina_slug) ?>" class="btn btn-default"><i class="fa fa-angle-right"></i> Leia Mais</a>
                    <hr>
                </article>
            <?php endforeach; ?>
            <?= $artigos_paginados["links"] ?>
        </div><!-- col_left -->

        <div class="col-md-4 col_right">
            <?php include 'sidebar.php'; ?>
        </div><!-- col_right -->
    </div><!-- container -->

</section>