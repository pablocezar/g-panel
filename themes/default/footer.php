</main>
<div class="j_backtop"><i class="fa fa-angle-up"></i></div>
<footer class="footer">
    <div class="container">

        <div class="col-md-4">
            <h4 class="title-default"><i class="fa fa-info"></i> Informaçõe de Contato</h4>
            <?php if ($config->config_telefone != ''): ?>
                <p><i class="fa fa-phone"></i> <a title="Clique e ligue" rel="nofollow" href="tel:+55<?= str_replace(array('(', ')', ' ', '-'), '', $config->config_telefone) ?>"><?= $config->config_telefone ?></a></p>
            <?php endif ?>
            <?php if ($config->config_telefone_2 != ''): ?>
                <p><i class="fa fa-phone"></i> <a title="Clique e ligue" rel="nofollow" href="tel:+55<?= str_replace(array('(', ')', ' ', '-'), '', $config->config_telefone_2) ?>"><?= $config->config_telefone_2 ?></a></p>
            <?php endif ?>
            <p><i class="fa fa-envelope-o"></i> <a title="Entrar em contato" rel="nofollow" href="mailto:<?= $config->config_email ?>"><?= $config->config_email ?></a></p>
            <?php if ($config->config_email_2 != ''): ?>
            <p><small>Para assuntos administrativos:</small></p>
            <p><i class="fa fa-envelope-o"></i> <a title="Entrar em contato" rel="nofollow" href="mailto:<?= $config->config_email_2 ?>"><?= $config->config_email_2 ?></a></p>
            <?php endif ?>
            <p><?= ($config->config_horario_atendimento != '') ? '<i class="fa fa-clock-o"></i> ' . $config->config_horario_atendimento : '' ?></p>
            <hr>
            <p><?= $config->config_logradouro ?>, <?= $config->config_numero ?></p>
            <p><?= $config->config_complemento ?></p>
            <p><?= $config->config_localidade ?>, <?= $config->config_uf ?></p>
            <p>CEP: <?= $config->config_cep ?></p>
        </div>

        <div class="col-md-4">
            <h4 class="title-default"><i class="fa fa-angle-right"></i> Menu</h4>
            <ul class="menu-footer">
                <?php foreach ($menu_rodape as $linha): ?>
                    <li><a href="<?= base_url() . $linha->menu_link ?>" title="<?= $linha->menu_titulo ?>"><i class="fa fa-angle-double-right"></i> <?= $linha->menu_titulo ?></a></li>
                <?php endforeach ?>
            </ul>
        </div>

        <div class="col-md-4">
            <h4 class="title-default"><i class="fa fa-share-alt"></i> Redes Sociais</h4>
            <?= ($config->config_facebook != '') ? '<a href="' . $config->config_facebook . '" target="_blank" class="btn btn-social btn-facebook"><i class="fa fa-facebook"></i></a>' : '' ?>
            <?= ($config->config_google_plus != '') ? '<a href="' . $config->config_google_plus . '" target="_blank" class="btn btn-social btn-google-plus"><i class="fa fa-google-plus"></i></a>' : '' ?>
            <?= ($config->config_twitter != '') ? '<a href="' . $config->config_twitter . '" target="_blank" class="btn btn-social btn-twitter"><i class="fa fa-twitter"></i></a>' : '' ?>
            <?= ($config->config_instagram != '') ? '<a href="' . $config->config_instagram . '" target="_blank" class="btn btn-social btn-instagram"><i class="fa fa-instagram"></i></a>' : '' ?>
            <?= ($config->config_youtube != '') ? '<a href="' . $config->config_youtube . '" target="_blank" class="btn btn-social btn-youtube"><i class="fa fa-youtube"></i></a>' : '' ?>
            <?= ($config->config_linkedin != '') ? '<a href="' . $config->config_linkedin . '" target="_blank" class="btn btn-social btn-linkedin"><i class="fa fa-linkedin"></i></a>' : '' ?>
        </div>

    </div>
</footer>
<div class="line-footer text-center">
    Copyright <?= date('Y') . ' ' . $config->config_nome_empresa ?> Todos os Direitos Reservados.
    <a href="https://www.genesystech.com.br" title="Criação e Otimização de Sites" target="_blank">
        <img src="<?= $theme ?>/images/ass.png" alt="Criação e Otimização de Sites" title="Criação e Otimização de Sites" />
    </a>
</div>
<?php if($popup != NULL):?>
<div class="popup">
    <div class="popup-header"><h3><?= $popup->popup_titulo ?></h3></div>
    <div class="popup-content">
        <img src="<?= Thumbnail(500, 300, $popup->popup_imagem_destacada) ?>" alt="<?= $popup->popup_titulo ?>" title="<?= $popup->popup_titulo ?>"/>
        <p><?= $popup->popup_conteudo ?></p>
    </div>
    <div class="popup-footer">
        <a class="btn btn-success btn-block" href="<?= $popup->popup_link ?>" title="Mais informações">Mais informações</a>
    </div>
</div>
<?php endif;?>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?= $theme ?>/bootstrap/js/bootstrap.min.js"></script>
<script>var base_url = '<?= base_url() ?>';</script>
<!-- Scripts -->
<script src="<?= $theme ?>/js/ticker.js"></script>
<script src="<?= $theme ?>/plugins/jssocials/jssocials.min.js"></script>
<!-- FancyBox -->
<script src="<?= $theme ?>/plugins/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?= $theme ?>/js/jquery.cookie.js"></script>

<script src="<?= $theme ?>/scripts.js"></script>

</body>
</html>