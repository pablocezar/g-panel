<aside>
    <header>
        <h3 class="hidden"><i class="fa fa-coffee"></i> Informações</h3>
    </header>


    <h3 class="title-secont"><i class="fa fa-facebook-official"></i> Facebook</h3>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-page" data-href="https://www.facebook.com/BRR-Blindagens-647061635645404/" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/BRR-Blindagens-647061635645404/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/BRR-Blindagens-647061635645404/">BRR Blindagens</a></blockquote></div>
    
    <h3 class="title-secont"><i class="fa fa-list"></i> Categorias</h3>
    <nav>
        <ul class="menu-categorias">
            <?php foreach ($categorias as $linha): ?>
                <?php
                $paginasBtCat = $this->db->where('pagina_id_categoria', $linha->categoria_id)->where('pagina_postagem_programada', NULL)->get('paginas')->result();
                ?>
                <li>
                    <span class="dropdown-menu-cat"><i class="fa fa-angle-right"></i></span>
                    <a href="<?= base_url('categoria/' . $linha->categoria_slug) ?>" title="<?= $linha->categoria_titulo ?>"><?= $linha->categoria_titulo ?></a>
                    <ul class="sub-menu-categorias">
                        <?php foreach ($paginasBtCat as $pg): ?>
                            <li>
                                <a href="<?= base_url() . $pg->pagina_slug ?>" title="<?= $pg->pagina_titulo ?>"> - <?= $pg->pagina_titulo ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        </ul>
    </nav>
</aside>